package com.azinova.emaid_crew.ui.activity.forgotpassword

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.azinova.emaid_crew.R

class ForgotPasswordActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
    }
}