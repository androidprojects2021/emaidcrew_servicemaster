package com.azinova.emaid_crew.ui.fragment.ui.edit_profile

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.azinova.emaid_crew.model.response.edit_profile.ResponseEditProfile
import com.azinova.emaid_crew.network.ApiClient
import com.azinova.emaid_crew.utils.SharedPref
import kotlinx.coroutines.launch

class EditProfileViewModel:ViewModel() {
    private val prefs = SharedPref

    private val _editprofileStatus = MutableLiveData<ResponseEditProfile>()
    val editprofileStatus: LiveData<ResponseEditProfile>
        get() = _editprofileStatus


     fun setProfileEdit(userId: Int, userType: String?, currentDate: String, token: String?, name: String, phone: String, email: String) {
      viewModelScope.launch {
          try {

              val response = ApiClient.getRetrofit().getEditProfile(userId.toString(),userType.toString(),currentDate, token.toString(),name,phone,email)


              if (response.status.equals("success")) {
                  _editprofileStatus.value = response

                  prefs.user_name = name
                  prefs.phone = phone
                  prefs.email = email
              }else{
                  _editprofileStatus.value = response

              }
          } catch (e: Exception) {
              Log.e("Throwe", "setProfileEdit: ", )
              ResponseEditProfile(message = "Something went wrong", status = "error")

          }
      }


    }

}