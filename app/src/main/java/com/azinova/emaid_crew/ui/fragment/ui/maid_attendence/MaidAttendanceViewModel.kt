package com.azinova.emaid_crew.ui.fragment.ui.maid_attendence

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.azinova.emaid_crew.database.repository.BookingsRepository
import com.azinova.emaid_crew.model.response.maidattendance.ResponseMaidAttendance
import com.azinova.emaid_crew.model.response.maidstatus.ResponseMaidStatusChange
import com.azinova.emaid_crew.network.ApiClient
import kotlinx.coroutines.launch

class MaidAttendanceViewModel : ViewModel() {

    private val _maidAttendencelistStatus = MutableLiveData<ResponseMaidAttendance>()
    val maidAttendencelistStatus: LiveData<ResponseMaidAttendance>
        get() = _maidAttendencelistStatus

    private val _maidStatusChange = MutableLiveData<ResponseMaidStatusChange>()
    val maidStatusChange: LiveData<ResponseMaidStatusChange>
        get() = _maidStatusChange


    fun getmaidAttendence(userId: String, currentdate: String, userType: String, token: String?) {
        viewModelScope.launch {
            try {

                val response = ApiClient.getRetrofit().getMaidAttendancelist(userId,userType,currentdate,token!!)
                if (response.status.equals("success")) {
                    _maidAttendencelistStatus.value = response

                } else {
                    _maidAttendencelistStatus.value = response
                }
            } catch (e: Exception) {
                _maidAttendencelistStatus.value = ResponseMaidAttendance(message = "Something went wrong", status = "Error")

            }
        }
    }

    suspend fun getMaidStatusChange(requireContext: Context, userId: Int, userType: String?, currentDate: String, token: String?, status: Int, maidId: String?) {
        try {
            val response = ApiClient.getRetrofit().getMaidStatusChange(
                    userId.toString(),
                    userType.toString(),
                    currentDate,
                    token.toString(),
                    status.toString(),
                    maidId.toString()

            )
            if (response.status.equals("success")) {

                updateMaidStatusChange(
                        requireContext, response.maidId.toString(),
                        response.maidDetails?.maidAttandence.toString()
                )
                _maidStatusChange.value = response

            } else {
                _maidStatusChange.value = response
            }

        } catch (e: Exception) {
            _maidStatusChange.value = ResponseMaidStatusChange(message = "Something went wrong", status = "error")
        }


    }

    private suspend fun updateMaidStatusChange(
            context: Context,
            maidId: String,
            maidAttandence: String
    ) {
        BookingsRepository.updateMaidStatusChange(context, maidId, maidAttandence)
    }

}