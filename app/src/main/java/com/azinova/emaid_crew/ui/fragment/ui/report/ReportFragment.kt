package com.azinova.emaid_crew.ui.fragment.ui.report

import android.app.DatePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.azinova.emaid_crew.R
import com.azinova.emaid_crew.commons.Loader
import com.azinova.emaid_crew.database.EmaidDatabase
import com.azinova.emaid_crew.ui.activity.home.MainActivity
import com.azinova.emaid_crew.ui.activity.login.LoginActivity
import com.azinova.emaid_crew.utils.SharedPref
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_report.*
import java.text.SimpleDateFormat
import java.util.*


class ReportFragment : Fragment() {
    private lateinit var reportViewModel: ReportViewModel

    private val prefs = SharedPref
    var database: EmaidDatabase? = null

    var newdate: String = " "

    private lateinit var parent: MainActivity


    lateinit var datePickerDialog: DatePickerDialog
    var year = 0
    var month = 0
    var dayOfMonth = 0
    lateinit var calendar: Calendar

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_report, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        reportViewModel = ViewModelProvider(this).get(ReportViewModel::class.java)

        parent = activity as MainActivity

        val sdf = SimpleDateFormat("dd/MM/yyyy")
        newdate = sdf.format(Date())

        setUpReport(newdate)
        observedIn()
        onClick()
    }

    private fun setUpReport(newdate: String) {

        txt_selecteddate .text = newdate

        var spf = SimpleDateFormat("dd/MM/yyyy")
        val newDatee = spf.parse(this.newdate)
        spf = SimpleDateFormat("yyyy/MM/dd")
        val date:String = spf.format(newDatee)


        Loader.showLoader(requireContext())
        if (requireContext().isConnectedToNetwork()) {
            getReportDetails(prefs.user_id.toString(), date, prefs.user_type.toString(), prefs.token)
        } else {
            Loader.hideLoader()
            Snackbar.make(outerreport, "Please connect to internet", Snackbar.LENGTH_LONG).show()
        }

    }

    private fun getReportDetails(user_id: String, date: String, user_type: String, token: String?) {
        reportViewModel.getReportDetails(user_id,date,user_type,token)
    }

    private fun onClick() {
        img_datereport.setOnClickListener {

            calendar = Calendar.getInstance()
            year = calendar.get(Calendar.YEAR)
            month = calendar.get(Calendar.MONTH)
            dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)

            datePickerDialog = DatePickerDialog(
                    requireContext(),
                    { datePicker, year, month, day ->

                        var formatmonth: String = month.toString() 
                        var formatDayOfMonth: String = "" + day

                        if (month < 10) {

                            formatmonth = "0" + (month+1);
                        }
                        if (day < 10) {

                            formatDayOfMonth = "0" + day;
                        }


                        newdate = formatDayOfMonth + "/" + formatmonth + "/" + year
                        txt_selecteddate.text = newdate
                        setUpReport(newdate)

                    }, year, month, dayOfMonth
            )

            datePickerDialog.setButton(
                    DialogInterface.BUTTON_NEGATIVE,
                    "Cancel",
                    DialogInterface.OnClickListener { dialog, which ->
                        if (which == DialogInterface.BUTTON_NEGATIVE) {

                            datePickerDialog.dismiss()

                        }
                    })
            datePickerDialog.show()
        }
    }

    private fun observedIn() {
        reportViewModel.reportStatus.observe(viewLifecycleOwner, androidx.lifecycle.Observer { reportStatus ->


            if (reportStatus.status.equals("success")) {
                Loader.hideLoader()
                txt_no_of_bookings.text = reportStatus.reportDetails?.bookingCount.toString()
                txt_no_of_hours.text = reportStatus.reportDetails?.bookingHours.toString()
                txt_billingamount_value.text = reportStatus.reportDetails?.totalBilled.toString()+" AED"
                txt_totalcollected_value.text = reportStatus.reportDetails?.totalCollected.toString()+" AED"
                txt_cashinhand_value.text = reportStatus.reportDetails?.cashinhand.toString()+" AED"

            } else if (reportStatus.status.equals("token_error")) {
                Loader.hideLoader()
                Toast.makeText(requireContext(), reportStatus.message, Toast.LENGTH_SHORT)
                        .show()


                clearLogin()
                parent.stopServiceTracker()
                database?.clearAllTables()

                val intent = Intent(requireContext(), LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                activity?.finish()

            } else {
                Loader.hideLoader()
                Toast.makeText(requireContext(), reportStatus.message, Toast.LENGTH_SHORT)
                        .show()

            }



        })
    }
    private fun clearLogin() {
        // prefs.clearSession(requireContext())
        prefs.user_details = ""
        prefs.user_id = 0
        prefs.user_name = ""
        prefs.user_type = ""
        prefs.token = ""
        prefs.image = ""
        prefs.phone = ""
        prefs.email = ""
        prefs.Latitude = ""
        prefs.Longitude = ""
    }

    override fun onPause() {
        super.onPause()
        try {
            Loader.hideLoader()
        } catch (e: Exception) {
        }
    }

    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
                this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }

}