package com.azinova.emaid_crew.ui.fragment.ui.notification

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.azinova.emaid_crew.model.response.notification.ResponseNotification
import com.azinova.emaid_crew.network.ApiClient
import kotlinx.coroutines.launch

class NotificationViewModel:ViewModel() {

    private val _notificationlistStatus = MutableLiveData<ResponseNotification>()
    val notificationlistStatus: LiveData<ResponseNotification>
        get() = _notificationlistStatus


    fun getNotificationList(userId: String, currentdate: String, userType: String, token: String?) {
        viewModelScope.launch {
            try {

                val response = ApiClient.getRetrofit().getnotificationList(userId,userType,currentdate,token!!)
                if (response.status.equals("success")) {
                    _notificationlistStatus.value = response

                } else {
                    _notificationlistStatus.value = response
                }
            } catch (e: Exception) {
                _notificationlistStatus.value = ResponseNotification(message = "Something went wrong", status = "Error")

            }
        }
    }


}