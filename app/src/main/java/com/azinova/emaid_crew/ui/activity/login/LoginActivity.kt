package com.azinova.emaid_crew.ui.activity.login

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.LocationManager
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.azinova.emaid_crew.R
import com.azinova.emaid_crew.commons.Loader
import com.azinova.emaid_crew.ui.activity.home.MainActivity
import com.azinova.emaid_crew.utils.PermissionUtils
import com.azinova.emaid_crew.utils.SharedPref
import com.azinova.emaid_crew.utils.notification.BaseActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.messaging.FirebaseMessaging
import java.text.SimpleDateFormat
import java.util.*


class LoginActivity : BaseActivity() {
    private val prefs = SharedPref

    private lateinit var loginviewModel: LoginActivityViewModel

    lateinit var context: Context

    var usertype: Int = 0

    lateinit var crew_click: TextView
    lateinit var driver_click: TextView

    lateinit var loginButton: Button
    lateinit var username: TextInputEditText
    lateinit var userpassword: TextInputEditText

    lateinit var outerlogin: ConstraintLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        loginviewModel = ViewModelProvider(this).get(LoginActivityViewModel::class.java)

        context = this@LoginActivity



        initView()
        clickListen()
        observeLoggedIn()


    }


    override fun onResume() {
        super.onResume()
        if (prefs.firebasetoken.isNullOrEmpty()) {
            FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("TAG  ", "Fetching FCM registration token failed", task.exception)
                    return@OnCompleteListener
                }

                val token = task.result
                prefs.firebasetoken = token

                Log.e("newToken", "newToken  " + token)

            })

        }
    }

    private fun observeLoggedIn() {
        loginviewModel.loginStatus.observe(this, Observer { loginStatus ->

            if (loginStatus.equals("success")) {
                Loader.hideLoader()
                val intent = Intent(applicationContext, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)

            } else {
                Loader.hideLoader()
                Toast.makeText(this, loginStatus, Toast.LENGTH_SHORT).show()
                username.text = Editable.Factory.getInstance()
                        .newEditable("")
                userpassword.text = Editable.Factory.getInstance()
                        .newEditable("")
                username.requestFocus()

                prefs.user_details = ""
                prefs.user_id = 0
                prefs.user_name = ""
                prefs.user_type = ""
                prefs.token = ""
                prefs.image = ""
                prefs.phone = ""
                prefs.email = ""
                prefs.Latitude = ""
                prefs.Longitude = ""
            }

        })


    }

    private fun initView() {
        outerlogin = findViewById(R.id.outerlogin)

        crew_click = findViewById(R.id.crew_click)
        driver_click = findViewById(R.id.driver_click)
        username = findViewById(R.id.txt_username)
        userpassword = findViewById(R.id.txt_password)
        loginButton = findViewById(R.id.loginButton)


    }


    private fun clickListen() {
        /* linearLayout_signup.setOnClickListener {
             val intent = Intent(applicationContext, SignUpActivity::class.java)
             startActivity(intent)
         }*/

        crew_click.setOnClickListener {
            usertype = 0

            crew_click.setBackgroundResource(R.drawable.bg_purple_left_curve)
            crew_click.setTextColor(Color.WHITE)

            driver_click.setBackgroundResource(R.drawable.bg_square_rightcurve)
            driver_click.setTextColor(getResources().getColor(R.color.colorPrimary))
        }
        driver_click.setOnClickListener {
            usertype = 1

            crew_click.setBackgroundResource(R.drawable.bg_square_leftcurve)
            crew_click.setTextColor(getResources().getColor(R.color.colorPrimary))

            driver_click.setBackgroundResource(R.drawable.bg_purple_right_curve)
            driver_click.setTextColor(Color.WHITE)

        }
        @SuppressLint("SimpleDateFormat")
        val sdf = SimpleDateFormat("yyyy/MM/dd")
        val currentDate = sdf.format(Date())

        loginButton.setOnClickListener {
            /*if (TextUtils.isEmpty(username.text)) {
                Snackbar.make(outerlogin, "Please enter username", Snackbar.LENGTH_SHORT).show()
            } else if (TextUtils.isEmpty(userpassword.text)) {
                Snackbar.make(outerlogin, "Please enter password", Snackbar.LENGTH_SHORT).show()
            } else {
*/
            Log.d("Login Firebase Token", "clickListen: " + prefs.firebasetoken)

            if (isValidLogin()) {
                Loader.showLoader(this)
                if (this.isConnectedToNetwork()) {
                    getDataLogin(
                            username.text.toString(),
                            userpassword.text.toString(),
                            usertype.toString(),
                            currentDate
                    )
                } else {
                    Loader.hideLoader()
                    Snackbar.make(outerlogin, "Please connect to internet", Snackbar.LENGTH_LONG)
                            .show()
                }
            }
            // }

        }
    }

    private fun isValidLogin(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                        applicationContext,
                        Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(
                        applicationContext,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this, arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
            ), 101
            )
            return false
        }
        val locationManager = applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Toast.makeText(this, "Turn on GPS", Toast.LENGTH_SHORT).show()
            PermissionUtils.showGPSNotEnabledDialog(this)

            return false
        }
        if (TextUtils.isEmpty(username.text)) {
            Snackbar.make(outerlogin, "Please enter username", Snackbar.LENGTH_SHORT).show()
            return false
        } else if (TextUtils.isEmpty(userpassword.text)) {
            Snackbar.make(outerlogin, "Please enter password", Snackbar.LENGTH_SHORT).show()
            return false
        }

        return true
    }

    private fun getDataLogin(
            username: String,
            password: String,
            type: String,
            currentDate: String
    ) {
        loginviewModel.doLogin(context, username, password, type, currentDate, prefs.firebasetoken)

    }

    override fun onPause() {
        super.onPause()
        try {
            Loader.hideLoader()
        } catch (e: Exception) {
        }
    }


    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
                this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }

}