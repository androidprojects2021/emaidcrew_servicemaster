package com.azinova.emaid_crew.ui.activity.detail

import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.os.bundleOf
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.azinova.emaid_crew.R
import com.azinova.emaid_crew.utils.PermissionUtils
import com.azinova.emaid_crew.utils.notification.BaseActivity
import kotlinx.android.synthetic.main.activity_booking_details.*

class BookingDetailsActivity : BaseActivity() {

    private lateinit var navController: NavController
    var actionbar: ActionBar? = null
     lateinit var customer_id:String
     lateinit var booking_id:String
     lateinit var booking_date:String
     lateinit var service_status:String

    companion object {
        private var REQUEST_LOCATION_CODE = 101
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_details)
        setSupportActionBar(findViewById(R.id.toolbar))

        actionbar = supportActionBar
        actionbar?.setDisplayHomeAsUpEnabled(true)
        actionbar?.setDisplayShowHomeEnabled(true)

        val bundle :Bundle ?=intent.extras
        if (bundle!=null) {
            customer_id = bundle.getString("Customer_id").toString()
            booking_id = bundle.getString("Booking_id").toString()
            booking_date = bundle.getString("Schedule_date").toString()
            service_status = bundle.getString("Service_status").toString()

        }

        onLocation()

        navController = findNavController(R.id.fragment_details)
        Log.d("Schedule_date", "onCreate: "+booking_date)
        val args = bundleOf("Booking_id" to booking_id,"Schedule_date" to booking_date,"Service_status" to service_status)


        navController.setGraph(R.navigation.nav_graph, args)
        onDestination()


    }

    private fun onDestination() {
        findNavController(R.id.fragment_details).addOnDestinationChangedListener { controller, destination, arguments ->

            when (destination.id) {
                R.id.navigation_bookingdetails -> {
                    actionbar?.title = "Booking Details"
                    img_call.visibility = View.VISIBLE
                    layout_direction.visibility = View.VISIBLE
                }
                R.id.navigation_paymentdetails -> {
                    actionbar?.title = "Payment Details"
                    img_call.visibility = View.GONE
                    layout_direction.visibility = View.GONE
                }
                else -> {
                    actionbar?.title = " "
                    img_call.visibility = View.GONE
                    layout_direction.visibility = View.GONE
                }
            }
        }

    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
    private fun onLocation() {
        when {
            PermissionUtils.isAccessFineLocationGranted(this) -> {
                when {
                    PermissionUtils.isLocationEnabled(this) -> {

                    }
                    else -> {
                        PermissionUtils.showGPSNotEnabledDialog(this)
                    }
                }
            }
            else -> {
                PermissionUtils.requestAccessFineLocationPermission(
                    this,
                    BookingDetailsActivity.REQUEST_LOCATION_CODE
                )


            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            BookingDetailsActivity.REQUEST_LOCATION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    when {
                        PermissionUtils.isLocationEnabled(this) -> {
                        }
                        else -> {

                            PermissionUtils.showGPSNotEnabledDialog(this)
                        }
                    }
                } else {
                    Toast.makeText(
                        this,
                        "Location Permission not granted",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }
    override fun onResume() {
        super.onResume()
        onLocation()
    }




}