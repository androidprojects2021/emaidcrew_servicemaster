package com.azinova.emaid_crew.ui.fragment.ui.booking_details

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.azinova.emaid_crew.R
import com.azinova.emaid_crew.commons.Loader
import com.azinova.emaid_crew.database.EmaidDatabase
import com.azinova.emaid_crew.database.tables.BookingsTable
import com.azinova.emaid_crew.database.tables.MaidTable
import com.azinova.emaid_crew.database.tables.TimeCountdownTable
import com.azinova.emaid_crew.model.response.maiddetails.MaidDetails
import com.azinova.emaid_crew.model.response.maiddetails.ResponseMaidBookingDetailsList
import com.azinova.emaid_crew.model.response.transfer.TransferListItem
import com.azinova.emaid_crew.ui.activity.detail.BookingDetailsActivity
import com.azinova.emaid_crew.ui.activity.login.LoginActivity
import com.azinova.emaid_crew.ui.fragment.ui.booking_details.adapter.MaidOnBoardAdapter
import com.azinova.emaid_crew.ui.fragment.ui.booking_details.adapter.TimeScheduleAdapter
import com.azinova.emaid_crew.ui.fragment.ui.booking_details.adapter.TransferAdapter
import com.azinova.emaid_crew.ui.fragment.ui.jobs.JobsViewModel
import com.azinova.emaid_crew.utils.SharedPref
import com.azinova.emaid_crew.utils.TimerConstants
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_booking_details.*
import kotlinx.android.synthetic.main.fragment_booking_details.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*


class BookingDetailsFragment : Fragment() {
    private val prefs = SharedPref
    var database: EmaidDatabase? = null

    private lateinit var parent: BookingDetailsActivity

    private lateinit var bookingssViewModel: BookingDetailsViewModel
    private lateinit var jobsViewModel: JobsViewModel //new change

    lateinit var maidOnBoardAdapter: MaidOnBoardAdapter
    lateinit var timeScheduleAdapter: TimeScheduleAdapter
    lateinit var transferAdapter: TransferAdapter

    private lateinit var rv_maidlistdetails: RecyclerView
    private lateinit var rvTimeSchedule: RecyclerView
    private lateinit var rv_transferSchedule: RecyclerView

    lateinit var btn_stop: Button

    lateinit var booking_id: String
    lateinit var booking_date: String
    lateinit var service_status: String

    var selected_driver: String = ""
    var selected_zone: String = ""

    val REQ_CODE_CALL = 100

    var schedulesFromDb: ArrayList<BookingsTable>? = ArrayList()

    var transferlist: List<TransferListItem?>? = listOf()

    var maidarraylist: List<MaidTable>? = listOf()
    var customerDetailsFromDb: List<BookingsTable>? = listOf()
    var countdatalist: List<TimeCountdownTable>? = null


    private val mInterval = TimerConstants.TIMER_INTERVAL
    private var mHandler: Handler? = null
    private var timeInSeconds = 0L

    private var startButtonClicked = false
    private var stopButtonClicked = false
    private var transferButtonClicked = false
    private var maidButtonClicked = false

    var starttime: String = " "

    var maidActive = 0


    private val _countdownTimer = MutableLiveData<String>()
    val countdownTimer: LiveData<String>
        get() = _countdownTimer

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_booking_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bookingssViewModel = ViewModelProvider(this).get(BookingDetailsViewModel::class.java)
        jobsViewModel = ViewModelProvider(this).get(JobsViewModel::class.java)//new change
        parent = activity as BookingDetailsActivity

/*
        initStopWatch()
*/

        bookingssViewModel.reset()

        booking_date = arguments?.getString("Schedule_date").toString()
        Log.d("Schedule_date", "onViewCreated: " + booking_date)

        booking_id = arguments?.getString("Booking_id").toString()
        txt_bookingidvalue.text = booking_id

        service_status = arguments?.getString("Service_status").toString()


        rv_maidlistdetails = view.findViewById(R.id.rv_maidlistdetails)
        btn_stop = view.findViewById(R.id.btn_stop)

        when (prefs.user_type) {
            "0" -> btn_transfer.visibility = View.GONE
            "1" -> btn_transfer.visibility = View.VISIBLE
        }


        //onViewSet()
        onCustomerViewset()
        observeList()
        onClick()


    }

    private fun onCustomerViewset() {
        Log.d("Maid Attandence 1", "onCustomerViewset: " + maidActive)

        //--------------- Booking updates new---------


        //MAID
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val maiddataArray = bookingssViewModel.NewMailListFromDB(requireContext(), booking_id)
                Log.d("Details", "onViewSet: " + maiddataArray)


                if (maiddataArray != null) {
                    if (maiddataArray?.size != 0) {
                        maiddataArray?.let { maidlist ->

                            maidarraylist = ArrayList()
                            (maidarraylist as ArrayList<MaidTable?>).clear()
                            maidarraylist = maidlist

                            Log.d("Maid Attandence 2", "onCustomerViewset: " + maidActive)

                            if (maidarraylist!!.isNotEmpty()) {
                                maidViewSet(maidarraylist!!)
                            }


                        }
                    }
                }

            } catch (e: Exception) {
            }
        }


        // CUSTOMER
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val bookingdetailsArray = bookingssViewModel.NewCustomerDetailsFromDB(requireContext(), booking_id)
                Log.d("Booking Details", "onViewSet: " + bookingdetailsArray)
                if (bookingdetailsArray != null) {
                    if (bookingdetailsArray.size != 0) {
                        bookingdetailsArray.let { detailslist ->

                            customerDetailsFromDb = ArrayList()
                            (customerDetailsFromDb as ArrayList<BookingsTable>).clear()
                            customerDetailsFromDb = detailslist

                            Log.d("Maid Attandence 3", "onCustomerViewset: " + maidActive)

                            if (customerDetailsFromDb!!.isNotEmpty()) {
                                detailsViewSet(customerDetailsFromDb!!)
                            }


                        }
                    }
                }
            } catch (e: Exception) {
            }
        }

//-----------------------------


        /* if (!service_status.isNullOrEmpty()) {
             timeInSeconds = 0L
             timerdataSet(service_status)
         }*/
    }

    private fun detailsViewSet(customerdetails: List<BookingsTable>) {
        val sdf = SimpleDateFormat("dd-MM-yyyy")
        val currentdate = sdf.format(Date())

        when (customerdetails[0].service_status) {
            0 -> {
                txt_status.text = "Pending"
                txt_status.setTextColor(getResources().getColor(R.color.color_pending_dark))
                img_status.setImageResource(R.drawable.ic_pending)

                Log.d("Maid Attandence 4", "onCustomerViewset: " + maidActive)

                if (maidActive == 1) {
                    startButtonClicked = false
                    btn_start.setBackgroundResource(R.drawable.bg_green)
                } else if (maidActive == 0) {
                    startButtonClicked = true
                    btn_start.setBackgroundResource(R.drawable.bg_button_off)
                }
                Log.d("maidActive", "onViewSet: maid " + maidActive)
                stopButtonClicked = false
                btn_stop.setBackgroundResource(R.drawable.bg_red)
                transferButtonClicked = false
                btn_transfer.setBackgroundResource(R.drawable.bg_purple)

                txt_starttime.visibility = View.GONE
                txt_stoptime.visibility = View.GONE
            }
            1 -> {
                txt_status.text = "Ongoing"
                txt_status.setTextColor(getResources().getColor(R.color.color_ongoing_dark))
                img_status.setImageResource(R.drawable.ic_ongoing_icon)

                startButtonClicked = true
                btn_start.setBackgroundResource(R.drawable.bg_button_off)
                stopButtonClicked = false
                btn_stop.setBackgroundResource(R.drawable.bg_red)
                transferButtonClicked = false
                btn_transfer.setBackgroundResource(R.drawable.bg_purple)

                txt_starttime.visibility = View.VISIBLE
                txt_stoptime.visibility = View.GONE

                var spf = SimpleDateFormat("hh:mm:ss a")
                val new = spf.parse(customerdetails[0].starttime)
                spf = SimpleDateFormat("hh:mm a")
                val starttime:String = spf.format(new)

                txt_starttime.text = starttime
            }
            2 -> {
                txt_status.text = "Finished"
                txt_status.setTextColor(getResources().getColor(R.color.color_finished_dark))
                img_status.setImageResource(R.drawable.ic_finished)

                startButtonClicked = true
                btn_start.setBackgroundResource(R.drawable.bg_button_off)
                stopButtonClicked = true
                btn_stop.setBackgroundResource(R.drawable.bg_button_off)
                transferButtonClicked = true
                btn_transfer.setBackgroundResource(R.drawable.bg_button_off)

                txt_starttime.visibility = View.VISIBLE
                txt_stoptime.visibility = View.VISIBLE


                var sspf = SimpleDateFormat("hh:mm:ss a")
                val snew = sspf.parse(customerdetails[0].starttime)
                sspf = SimpleDateFormat("hh:mm a")
                val starttime:String = sspf.format(snew)

                var spf = SimpleDateFormat("hh:mm:ss a")
                val new = spf.parse(customerdetails[0].endtime)
                spf = SimpleDateFormat("hh:mm a")
                val endtime:String = spf.format(new)

                txt_starttime.text = starttime
                txt_stoptime.text = endtime
            }
            3 -> {
                txt_status.text = "Cancelled"
                txt_status.setTextColor(getResources().getColor(R.color.color_cancelled_dark))
                img_status.setImageResource(R.drawable.ic_cancelled)

                startButtonClicked = true
                btn_start.setBackgroundResource(R.drawable.bg_button_off)
                stopButtonClicked = true
                btn_stop.setBackgroundResource(R.drawable.bg_button_off)
                transferButtonClicked = true
                btn_transfer.setBackgroundResource(R.drawable.bg_button_off)

                txt_starttime.visibility = View.GONE
                txt_stoptime.visibility = View.GONE
            }
            else -> {
                startButtonClicked = true
                btn_start.setBackgroundResource(R.drawable.bg_button_off)
                stopButtonClicked = true
                btn_stop.setBackgroundResource(R.drawable.bg_button_off)
                transferButtonClicked = true
                btn_transfer.setBackgroundResource(R.drawable.bg_button_off)

                txt_starttime.visibility = View.GONE
                txt_stoptime.visibility = View.GONE
            }
        }


        txt_customername.text = customerdetails[0].customer_name
        txt_customernumber.text = customerdetails[0].customerMobile
        txt_area.text = customerdetails[0].area
        txt_customeraddress.text = customerdetails[0].customerAddress
        txt_notes.text = customerdetails[0].customernotes
        txt_extranotes.text = customerdetails[0].bookingNote
//        txt_extranotes.text = "dihcskhdskjdmlskdcugsdckmslkdhckshcdnkadhc,hadlchbjsgcahbaliusgcksjcigakchlahckajcikhakchiahdkkcjiahcihadchlaihdcoljandickjadohcliahdcoahdoichssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss"

        when (customerdetails[0].keyStatus) {
            "0" -> txt_key.text = "No"
            "1" -> txt_key.text = "Yes"
        }


        val _24HourTimeshiftStart = customerdetails[0].shift_start
        val _24HourSDFS = SimpleDateFormat("HH:mm")
        val _12HourSDFS = SimpleDateFormat("hh:mm a")
        val shiftStart = _24HourSDFS.parse(_24HourTimeshiftStart)

        val _24HourTimeshiftEnd = customerdetails[0].shift_end
        val _24HourSDF = SimpleDateFormat("HH:mm")
        val _12HourSDF = SimpleDateFormat("hh:mm a")
        val shiftEnd = _24HourSDF.parse(_24HourTimeshiftEnd)

        txt_servicetime.text =
                _12HourSDFS.format(shiftStart) + " - " + _12HourSDF.format(shiftEnd)
        txt_housecleaning.text = customerdetails[0].servicetype
        txt_cleaningmaterial.text = customerdetails[0].tools
        txt_extra.text = customerdetails[0].extraService

        when (customerdetails[0].paymentStatus) {
            0 -> payment_status.text = "Not Paid"
            1 -> payment_status.text = "Paid"
        }


        txt_serviceamount_value.text = "AED " + customerdetails[0].serviceFee
        txt_paidamount_value.text =
                "AED " + customerdetails[0].paidAmount.toString()
        txt_balance_value.text = "AED " + customerdetails[0].balance.toString()

        schedulesFromDb = customerdetails as ArrayList<BookingsTable>?

        if (currentdate.toString() != customerDetailsFromDb?.get(0)?.schedule_date) {

            Log.e("Booking date", "onViewSet: date " + "True")

            startButtonClicked = true
            btn_start.setBackgroundResource(R.drawable.bg_button_off)
            stopButtonClicked = true
            btn_stop.setBackgroundResource(R.drawable.bg_button_off)
            transferButtonClicked = true
            btn_transfer.setBackgroundResource(R.drawable.bg_button_off)

        }


    }

    private fun maidViewSet(maidlist: List<MaidTable>) {

        for (item in maidlist) {
            maidActive = 0
            when (item.maid_attandence) {
                "1" -> maidActive = 1
            }
        }
        Log.d("Maid Observer", "onViewSet:----------- " + maidActive)

        /* if (maidActive == 1) {
             startButtonClicked = false
             btn_start.setBackgroundResource(R.drawable.bg_green)

         } else if (maidActive == 0) {
             startButtonClicked = true
             btn_start.setBackgroundResource(R.drawable.bg_button_off)
         }*/


        maidOnBoardAdapter = MaidOnBoardAdapter(
                requireContext(),
                maidarraylist
        ) { maidtable: MaidTable?, i: Int, list: List<MaidTable?>? ->
            MaidClick(maidtable)

        }
        val lm = LinearLayoutManager(requireContext())
        lm.orientation = LinearLayoutManager.HORIZONTAL
        rv_maidlistdetails.setLayoutManager(lm)
        rv_maidlistdetails.setAdapter(maidOnBoardAdapter)
    }


    private fun observeList() {


        bookingssViewModel.TransferlistStatus.observe(
                viewLifecycleOwner,
                Observer { TransferlistStatus ->
                    if (TransferlistStatus != null) {
                        if (TransferlistStatus?.status.equals("success")) {
                            Loader.hideLoader()

                            if (TransferlistStatus?.transferList?.size != 0) {
                                transferlist = TransferlistStatus?.transferList
                                transferdailog()
                            } else {
                                Snackbar.make(
                                        outerdetails,
                                        "Transfer List is empty",
                                        Snackbar.LENGTH_LONG
                                )
                                        .show()
                            }
                        } else if (TransferlistStatus.status.equals("token_error")) {
                            Loader.hideLoader()
                            Toast.makeText(requireContext(), TransferlistStatus.message, Toast.LENGTH_SHORT)
                                    .show()


                            clearLogin()
                            //parent.stopServiceTracker()
                            database?.clearAllTables()

                            val intent = Intent(requireContext(), LoginActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(intent)
                            activity?.finish()

                        } else {
                            Loader.hideLoader()
                            Toast.makeText(
                                    requireContext(),
                                    TransferlistStatus?.message,
                                    Toast.LENGTH_SHORT
                            )
                                    .show()
                        }
                    }
                })


        bookingssViewModel.transferStatus.observe(viewLifecycleOwner, Observer { transferStatus ->
            if (transferStatus != null) {

                if (transferStatus?.status.equals("success")) {
                    Loader.hideLoader()
                    Toast.makeText(requireContext(), transferStatus?.message, Toast.LENGTH_SHORT)
                            .show()

                    callCurrentDate()//new change
                    activity?.onBackPressed()
                } else if (transferStatus.status.equals("token_error")) {
                    Loader.hideLoader()
                    Toast.makeText(requireContext(), transferStatus.message, Toast.LENGTH_SHORT)
                            .show()


                    clearLogin()
                    //parent.stopServiceTracker()
                    database?.clearAllTables()

                    val intent = Intent(requireContext(), LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    activity?.finish()

                } else {
                    Loader.hideLoader()
                    Toast.makeText(requireContext(), transferStatus?.message, Toast.LENGTH_SHORT)
                            .show()
                }
            }
        })

        bookingssViewModel.startServiceStatus.observe(
                viewLifecycleOwner,
                Observer { startServiceStatus ->
                    if (startServiceStatus != null) {

                        if (startServiceStatus?.status.equals("success")) {
                            Loader.hideLoader()
                            Toast.makeText(
                                    requireContext(),
                                    startServiceStatus?.message,
                                    Toast.LENGTH_SHORT
                            )
                                    .show()
                            activity?.onBackPressed()
                        } else if (startServiceStatus.status.equals("token_error")) {
                            Loader.hideLoader()
                            Toast.makeText(requireContext(), startServiceStatus.message, Toast.LENGTH_SHORT)
                                    .show()


                            clearLogin()
                            // parent.stopServiceTracker()
                            database?.clearAllTables()

                            val intent = Intent(requireContext(), LoginActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(intent)
                            activity?.finish()

                        } else {
                            Loader.hideLoader()
                            Toast.makeText(
                                    requireContext(),
                                    startServiceStatus?.message,
                                    Toast.LENGTH_SHORT
                            )
                                    .show()
                        }
                    }
                })


        bookingssViewModel.maidBookingsStatus.observe(
                viewLifecycleOwner,
                Observer { maidBookingsStatus ->
                    if (maidBookingsStatus != null) {

                        if (maidBookingsStatus?.status.equals("success")) {
                            Loader.hideLoader()

                            if (maidBookingsStatus != null) {

                                Log.d("SERVICE STATUS", "observeList: " + txt_status.text.toString())
                                maidDetailsDialog(maidBookingsStatus, txt_status.text.toString())
                            }

                        } else if (maidBookingsStatus.status.equals("token_error")) {
                            Loader.hideLoader()


                            clearLogin()
                            //parent.stopServiceTracker()
                            database?.clearAllTables()

                            val intent = Intent(requireContext(), LoginActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(intent)
                            // activity?.finish()
                            Toast.makeText(requireContext(), maidBookingsStatus.message, Toast.LENGTH_SHORT)
                                    .show()

                        } else {
                            Loader.hideLoader()
                            Toast.makeText(
                                    requireContext(),
                                    maidBookingsStatus?.message,
                                    Toast.LENGTH_SHORT
                            )
                                    .show()
                        }
                    }
                })

        bookingssViewModel.maidStatusChange.observe(
                viewLifecycleOwner,
                Observer { maidStatusChange ->
                    if (maidStatusChange != null) {

                        if (maidStatusChange?.status.equals("success")) {
                            Loader.hideLoader()
                            when (maidStatusChange.maidDetails?.maidAttandence) {
                                "1" -> maidActive = 1
                                "0" -> maidActive = 0
                                "2" -> maidActive = 0
                            }
                            onCustomerViewset()
                            Toast.makeText(
                                    requireContext(),
                                    maidStatusChange?.message,
                                    Toast.LENGTH_SHORT
                            )
                                    .show()

                        } else if (maidStatusChange.status.equals("token_error")) {
                            Loader.hideLoader()
                            Toast.makeText(requireContext(), maidStatusChange.message, Toast.LENGTH_SHORT)
                                    .show()


                            clearLogin()
                            //parent.stopServiceTracker()
                            database?.clearAllTables()

                            val intent = Intent(requireContext(), LoginActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(intent)
                            activity?.finish()

                        } else {
                            Loader.hideLoader()
                            Toast.makeText(
                                    requireContext(),
                                    maidStatusChange?.message,
                                    Toast.LENGTH_SHORT
                            )
                                    .show()
                        }
                    }
                })

       /* countdownTimer.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                Log.d("Timer -- 2", "updateStopWatchView: " + it)
                txt_timershow.text = it
            }
        })*/
    }

    //new change
    private fun callCurrentDate() {
        val sdf = SimpleDateFormat("dd-MM-yyyy")
        //val sdf = "25-06-2021"
        var currentdate = sdf.format(Date())

        // DatesFromDb
        GlobalScope.launch(Dispatchers.Main){
            try {

                val dateArray = jobsViewModel.getBookingDates(requireContext())

                val datelist: List<String>? = dateArray
                val selected = datelist?.filter { it.equals(currentdate) }
                Log.e("TAG", "callDateData: " + selected)

                if (selected?.isEmpty()!!) {

                    insertDate(currentdate)

                }else{
                    activity?.onBackPressed()
                }
            }catch (e: Exception){
                Log.e("callDateData", "Exception: ${e.message}" )
                activity?.onBackPressed()
            }
        }
    }

    //new change
    private fun insertDate(currentdate: String) {
        GlobalScope.launch {
            jobsViewModel.insertDate(currentdate)
        }
    }

    private fun maidDetailsDialog(
            maidBookingsStatus: ResponseMaidBookingDetailsList?,
            service_status: String
    ) {

        val sdf = SimpleDateFormat("dd-MM-yyyy")
        val currentdate = sdf.format(Date())

        val prefs = SharedPref

        var maidstatus = 0

        val builder = AlertDialog.Builder(requireContext())
        val dialogview: View = LayoutInflater.from(requireContext()).inflate(
                R.layout.dialog_maiddetails,
                null
        )
        builder.setView(dialogview)

        rvTimeSchedule = dialogview.findViewById(R.id.rv_timeSchedule)
        val l_noSchedule = dialogview.findViewById<LinearLayout>(R.id.l_noSchedule)
        val txt_maidname = dialogview.findViewById<TextView>(R.id.txt_maidname)
        val btn_maidstatus_changer = dialogview.findViewById<TextView>(R.id.btn_maidstatus_changer)
        val txt_remainingtime_value =
                dialogview.findViewById<TextView>(R.id.txt_remainingtime_value)
        val img_maid = dialogview.findViewById<ImageView>(R.id.img_maid)

        val alertDialog: AlertDialog = builder.create()



        when (prefs.user_type) {
            "0" -> btn_maidstatus_changer.visibility = View.GONE
            "1" -> {

                btn_maidstatus_changer.visibility = View.VISIBLE

                if (booking_date != null) {
                    if (currentdate.toString() != booking_date) {
                        btn_maidstatus_changer.visibility = View.GONE
                    } else {
                        btn_maidstatus_changer.visibility = View.VISIBLE

                    }
                }
            }
        }







        if (maidBookingsStatus?.maidDetails?.maidImage!!.isNotEmpty()) {

            Glide.with(requireContext())
                    .load(maidBookingsStatus.maidDetails.maidImage)
                    .placeholder(R.drawable.nomaidimage)
                    .error(R.drawable.nomaidimage)
                    .into(img_maid)

        } else {
            img_maid.setImageResource(R.drawable.nomaidimage)
        }


        txt_maidname.text = maidBookingsStatus?.maidDetails?.maidName

       /* if (service_status == "Ongoing") {
            countdownTimer.observe(viewLifecycleOwner, Observer {
                Log.d("Timer -- 3", "updateStopWatchView: " + it)
                txt_remainingtime_value.text = it
            })
        } else {
            txt_remainingtime_value.text = ""
        }*/


        Log.d("TAG", "maidDetailsDialog 1: " + maidBookingsStatus?.maidDetails?.maidAttendance)
        when (maidBookingsStatus?.maidDetails?.maidAttendance) {
            "0" -> {
                maidstatus = 1
                btn_maidstatus_changer.text = "Maid In  "
                btn_maidstatus_changer.setBackgroundResource(R.drawable.bg_green)
            }
            "1" -> {
                maidstatus = 2
                btn_maidstatus_changer.text = "Maid Out"
                btn_maidstatus_changer.setBackgroundResource(R.drawable.bg_red)
            }
            "2" -> {
                maidstatus = 1
                btn_maidstatus_changer.text = "Maid In  "
                btn_maidstatus_changer.setBackgroundResource(R.drawable.bg_green)
            }
        }

        Log.d("TAG", "maidDetailsDialog 2: " + maidstatus)



        if (maidBookingsStatus?.maidDetails?.nextSchedules?.size != 0) {
            rvTimeSchedule.visibility = View.VISIBLE
            l_noSchedule.visibility = View.GONE

            timeScheduleAdapter =
                    TimeScheduleAdapter(maidBookingsStatus?.maidDetails?.nextSchedules)
            val layoutManager = LinearLayoutManager(requireContext())
            rvTimeSchedule.layoutManager = layoutManager
            rvTimeSchedule.adapter = timeScheduleAdapter
        } else {
            rvTimeSchedule.visibility = View.GONE
            l_noSchedule.visibility = View.VISIBLE
        }


        val img_close = dialogview.findViewById(R.id.img_close) as ImageView
        img_close.setOnClickListener {
            alertDialog.dismiss()
        }

        btn_maidstatus_changer.setOnClickListener {
            maidStatuschange(maidBookingsStatus?.maidDetails, maidstatus)
            alertDialog.dismiss()
        }

        alertDialog.show()
    }


    private fun maidStatuschange(maidDetails: MaidDetails?, status: Int) {

        if (requireContext().isConnectedToNetwork()) {

            @SuppressLint("SimpleDateFormat")
            val sdf1 = SimpleDateFormat("yyyy/MM/dd")
            val currentDate = sdf1.format(Date())

            Loader.showLoader(requireContext())
            GlobalScope.launch(Dispatchers.Main) {
                bookingssViewModel.getMaidStatusChange(
                        requireContext(),
                        prefs.user_id,
                        prefs.user_type,
                        currentDate,
                        prefs.token,
                        status,
                        maidDetails?.maidId
                )
            }


        } else {
            Snackbar.make(outerdetails, "Please connect to internet", Snackbar.LENGTH_LONG)
                    .show()
        }


    }


    private fun onViewSet() {
        val sdf = SimpleDateFormat("dd-MM-yyyy")
        val currentdate = sdf.format(Date())


        //fetchMailListFromDB
        bookingssViewModel.getMailListFromDB(requireContext(), booking_id)
                ?.observe(viewLifecycleOwner, Observer { maidlist ->
                    try {

                        if (maidlist != null) {
                            if (maidlist.size != 0) {

                                maidarraylist = maidlist


                                for (item in maidlist) {
                                    // maidarraylist = listOf(item)
                                    maidActive = 0
                                    when (item.maid_attandence) {
                                        "1" -> maidActive = 1
                                    }
                                }
                                Log.d("Maid Observer", "onViewSet:----------- " + maidActive)

                                if (maidActive == 1) {
                                    startButtonClicked = false
                                    btn_start.setBackgroundResource(R.drawable.bg_green)

                                } else if (maidActive == 0) {
                                    startButtonClicked = true
                                    btn_start.setBackgroundResource(R.drawable.bg_button_off)
                                }


                                maidOnBoardAdapter = MaidOnBoardAdapter(
                                        requireContext(),
                                        maidarraylist
                                ) { maidtable: MaidTable?, i: Int, list: List<MaidTable?>? ->
                                    MaidClick(maidtable)

                                }
                                val lm = LinearLayoutManager(requireContext())
                                lm.orientation = LinearLayoutManager.HORIZONTAL
                                rv_maidlistdetails.setLayoutManager(lm)
                                rv_maidlistdetails.setAdapter(maidOnBoardAdapter)
                            }
                        }

                    } catch (e: Exception) {
                    }

                    /*GlobalScope.launch(Dispatchers.Main) {
            val maidlistitems = bookingssViewModel.getMailListFromDB(requireContext(), booking_id)
            maidlistitems?.let { maidlist ->}

                     }*/

                })


        //fetchCustomerDetailsFromDB
        bookingssViewModel.getCustomerDetails(requireContext(), booking_id)
                ?.observe(viewLifecycleOwner, Observer { customerdetails ->

                    try {

                        if (customerdetails != null) {
                            Log.d("CustomerDetails", "onViewSet: " + customerdetails)
                            if (customerdetails.size != 0) {

                                customerDetailsFromDb = customerdetails

                                Log.d("maidarraylist", "onViewSet:----------- " + maidarraylist)

                                Log.d("maidActive", "onViewSet:----------- " + maidActive)


                                Log.e(
                                        "Booking date",
                                        "onViewSet: date " + currentdate + " - " + customerDetailsFromDb?.get(
                                                0
                                        )?.schedule_date
                                )

                                when (customerdetails[0].service_status) {
                                    0 -> {
                                        txt_status.text = "Pending"
                                        txt_status.setTextColor(getResources().getColor(R.color.color_pending_dark))
                                        img_status.setImageResource(R.drawable.ic_pending)

                                        if (maidActive == 1) {
                                            startButtonClicked = false
                                            btn_start.setBackgroundResource(R.drawable.bg_green)
                                        } else if (maidActive == 0) {
                                            startButtonClicked = true
                                            btn_start.setBackgroundResource(R.drawable.bg_button_off)
                                        }
                                        Log.d("maidActive", "onViewSet: maid " + maidActive)
                                        stopButtonClicked = false
                                        btn_stop.setBackgroundResource(R.drawable.bg_red)
                                        transferButtonClicked = false
                                        btn_transfer.setBackgroundResource(R.drawable.bg_purple)
                                    }
                                    1 -> {
                                        txt_status.text = "Ongoing"
                                        txt_status.setTextColor(getResources().getColor(R.color.color_ongoing_dark))
                                        img_status.setImageResource(R.drawable.ic_ongoing_icon)

                                        startButtonClicked = true
                                        btn_start.setBackgroundResource(R.drawable.bg_button_off)
                                        stopButtonClicked = false
                                        btn_stop.setBackgroundResource(R.drawable.bg_red)
                                        transferButtonClicked = false
                                        btn_transfer.setBackgroundResource(R.drawable.bg_purple)
                                    }
                                    2 -> {
                                        txt_status.text = "Finished"
                                        txt_status.setTextColor(getResources().getColor(R.color.color_finished_dark))
                                        img_status.setImageResource(R.drawable.ic_finished)

                                        startButtonClicked = true
                                        btn_start.setBackgroundResource(R.drawable.bg_button_off)
                                        stopButtonClicked = true
                                        btn_stop.setBackgroundResource(R.drawable.bg_button_off)
                                        transferButtonClicked = true
                                        btn_transfer.setBackgroundResource(R.drawable.bg_button_off)
                                    }
                                    3 -> {
                                        txt_status.text = "Cancelled"
                                        txt_status.setTextColor(getResources().getColor(R.color.color_cancelled_dark))
                                        img_status.setImageResource(R.drawable.ic_cancelled)

                                        startButtonClicked = true
                                        btn_start.setBackgroundResource(R.drawable.bg_button_off)
                                        stopButtonClicked = true
                                        btn_stop.setBackgroundResource(R.drawable.bg_button_off)
                                        transferButtonClicked = true
                                        btn_transfer.setBackgroundResource(R.drawable.bg_button_off)
                                    }
                                    else -> {
                                        startButtonClicked = true
                                        btn_start.setBackgroundResource(R.drawable.bg_button_off)
                                        stopButtonClicked = true
                                        btn_stop.setBackgroundResource(R.drawable.bg_button_off)
                                        transferButtonClicked = true
                                        btn_transfer.setBackgroundResource(R.drawable.bg_button_off)
                                    }
                                }


                                txt_customername.text = customerdetails[0].customer_name
                                txt_customernumber.text = customerdetails[0].customerMobile
                                txt_area.text = customerdetails[0].area
                                txt_customeraddress.text = customerdetails[0].customerAddress
                                txt_notes.text = customerdetails[0].bookingNote

                                when (customerdetails[0].keyStatus) {
                                    "0" -> txt_key.text = "No"
                                    "1" -> txt_key.text = "Yes"
                                }


                                val _24HourTimeshiftStart = customerdetails[0].shift_start
                                val _24HourSDFS = SimpleDateFormat("HH:mm")
                                val _12HourSDFS = SimpleDateFormat("hh:mm a")
                                val shiftStart = _24HourSDFS.parse(_24HourTimeshiftStart)

                                val _24HourTimeshiftEnd = customerdetails[0].shift_end
                                val _24HourSDF = SimpleDateFormat("HH:mm")
                                val _12HourSDF = SimpleDateFormat("hh:mm a")
                                val shiftEnd = _24HourSDF.parse(_24HourTimeshiftEnd)

                                txt_servicetime.text =
                                        _12HourSDFS.format(shiftStart) + " - " + _12HourSDF.format(shiftEnd)
                                txt_housecleaning.text = customerdetails[0].servicetype
                                txt_cleaningmaterial.text = customerdetails[0].tools
                                txt_extra.text = customerdetails[0].extraService

                                when (customerdetails[0].paymentStatus) {
                                    0 -> payment_status.text = "Not Paid"
                                    1 -> payment_status.text = "Paid"
                                }


                                txt_serviceamount_value.text = "AED " + customerdetails[0].serviceFee
                                txt_paidamount_value.text =
                                        "AED " + customerdetails[0].paidAmount.toString()
                                txt_balance_value.text = "AED " + customerdetails[0].balance.toString()

                                schedulesFromDb = customerdetails as ArrayList<BookingsTable>?


                                if (currentdate.toString() != customerDetailsFromDb?.get(0)?.schedule_date) {

                                    Log.e("Booking date", "onViewSet: date " + "True")

                                    startButtonClicked = true
                                    btn_start.setBackgroundResource(R.drawable.bg_button_off)
                                    stopButtonClicked = true
                                    btn_stop.setBackgroundResource(R.drawable.bg_button_off)
                                    transferButtonClicked = true
                                    btn_transfer.setBackgroundResource(R.drawable.bg_button_off)
                                    //  maidButtonClicked = true
                                }


                            }
                        }

                    } catch (e: Exception) {
                    }

                    /*GlobalScope.launch(Dispatchers.Main) {
                        val customerdetailslist = bookingssViewModel.getCustomerDetails(requireContext(), booking_id)

                        customerdetailslist?.let { customerdetails ->


                        }
                    }*/
                })


    }


    override fun onResume() {
        super.onResume()
        Log.d("TAG", "onResume: ")
        onCustomerViewset()


        // -------------------------------------- Timer ----------------------

        /*  Log.d("Service Status", "onResume: " + service_status)
          if (!service_status.isNullOrEmpty()) {
              timerdataSet(service_status)
          }*/
        // -------------------------------------- ----------------------

    }

    // -------------------------------------- Timer ----------------------

    /*private fun timerdataSet(serviceStatus: String?) {
        timeInSeconds = 0L
        if (serviceStatus == "1") {
            GlobalScope.launch(Dispatchers.Main) {
                try {


                    val dataArray = bookingssViewModel.getCounterTimeing(requireContext(), booking_id)




                    if (dataArray != null) {
                        if (dataArray?.size != 0) {
                            dataArray?.let { timer ->

                                countdatalist = ArrayList()
                                (countdatalist as ArrayList<TimeCountdownTable?>).clear()
                                countdatalist = timer
                                Log.d("countdatalist", "onResume: " + countdatalist)

                                if (countdatalist!!.isNotEmpty()) {

                                    val nowtime = Calendar.getInstance()
                                    val hour = nowtime[Calendar.HOUR_OF_DAY]
                                    val minute = nowtime[Calendar.MINUTE]
                                    val second = nowtime[Calendar.SECOND]


                                    val now = Time(hour, minute, second)
                                    val old = Time(
                                            countdatalist?.get(0)?.service_start_hour!!.toInt(),
                                            countdatalist?.get(0)?.service_start_minute!!.toInt(),
                                            countdatalist?.get(0)?.service_start_second!!.toInt()
                                    )

                                    val diff: Time

                                    diff = difference(now, old)

                                    Log.d("difference", "onResume: " + diff.toString())

                                    toseconds(diff.toString())

                                }


                            }
                        }
                    }


                } catch (e: Exception) {
                }
            }

        }

    }*/
    // --------------------------------------  ----------------------


    private fun maidlist() {
        GlobalScope.launch(Dispatchers.Main) {
            bookingssViewModel.getMaiddetailslist(
                    "8bd34d06f406c4b2",
                    "1",
                    "2021-05-14",
                    prefs.token,
                    booking_id
            )
        }
    }

    private fun transferListFetch() {

        @SuppressLint("SimpleDateFormat")
        val sdf1 = SimpleDateFormat("yyyy/MM/dd")
        val currentDate = sdf1.format(Date())

        GlobalScope.launch(Dispatchers.Main) {
            bookingssViewModel.getTransferlist(
                    prefs.user_id.toString(),
                    prefs.user_type.toString(),
                    currentDate,
                    prefs.token,
                    booking_id
            )
        }
    }

    private fun onClick() {


        requireActivity().img_direction.setOnClickListener {
            // Log.d(" Outer - Customer Details ", "onClick: " + customerDetailsFromDb)

            Log.e(
                    "Location",
                    customerDetailsFromDb?.get(0)?.customerLatitude + customerDetailsFromDb?.get(0)?.customerLongitude
            )

            if (customerDetailsFromDb?.get(0)?.customerLatitude!!.isEmpty() || customerDetailsFromDb?.get(
                            0
                    )?.customerLongitude!!.isEmpty()
            ) {
                Toast.makeText(
                        requireContext(),
                        "Location information not available",
                        Toast.LENGTH_SHORT
                ).show()

                Log.e(
                        "Location",
                        customerDetailsFromDb?.get(0)?.customerLatitude + customerDetailsFromDb?.get(0)?.customerLongitude
                )

            } else {
                val gmmIntentUri: Uri
                gmmIntentUri = if (customerDetailsFromDb?.get(0)?.customerLatitude!!.isEmpty()) {
                    Uri.parse(
                            "google.navigation:q=" + customerDetailsFromDb?.get(0)?.area.toString() + ", "
                                    + customerDetailsFromDb?.get(0)?.customerAddress
                    )
                } else {
                    Uri.parse(
                            "google.navigation:q=" + customerDetailsFromDb?.get(0)?.customerLatitude
                                    .toString() + "," + customerDetailsFromDb?.get(0)?.customerLongitude
                    )
                }
                val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                mapIntent.setPackage("com.google.android.apps.maps")
                if (mapIntent.resolveActivity(requireContext().getPackageManager()) != null) {
                    startActivity(mapIntent)
                } else {
                    Toast.makeText(
                            requireContext(),
                            "Google Maps not available, Please install from play store.",
                            Toast.LENGTH_SHORT
                    ).show()
                }
            }


        }
        requireActivity().img_call.setOnClickListener {
            makePhoneCall()
        }

        btn_start.setOnClickListener {
            Log.d("Start", "onViewSet: " + maidarraylist)

            if (!startButtonClicked) {
                val alertDialog = AlertDialog.Builder(requireContext()).create()
                alertDialog.setTitle("Confirmation")
                alertDialog.setMessage("Do you want to continue ?")

                alertDialog.setButton(
                        AlertDialog.BUTTON_POSITIVE, "Confirm"
                ) { dialog, which ->
                    if (requireContext().isConnectedToNetwork()) {
                        startService()
                    } else {
                        Snackbar.make(
                                outerdetails,
                                "Please connect to internet",
                                Snackbar.LENGTH_LONG
                        )
                                .show()
                    }
                    dialog.dismiss()
                }

                alertDialog.setButton(
                        AlertDialog.BUTTON_NEGATIVE, "Cancel"
                ) { dialog, which -> dialog.dismiss() }
                alertDialog.show()

                val btnPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                val btnNegative = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)

                val layoutParams = btnPositive.layoutParams as LinearLayout.LayoutParams
                layoutParams.weight = 10f
                btnPositive.layoutParams = layoutParams
                btnNegative.layoutParams = layoutParams


            }
        }

        btn_stop.setOnClickListener {
            if (!stopButtonClicked) {

/*
                stopTimer()
*/

                val bundle = Bundle()
                bundle.putString("Booking_id", booking_id)
                bundle.putString(
                        "Customer_id",
                        customerDetailsFromDb?.get(0)?.customer_id.toString()
                )
                bundle.putString(
                        "Service_amount",
                        customerDetailsFromDb?.get(0)?.serviceFee.toString()
                )
                bundle.putString(
                        "Service_status",
                        customerDetailsFromDb?.get(0)?.service_status.toString()
                )
                bundle.putString("Total", customerDetailsFromDb?.get(0)?.total.toString())
                bundle.putString(
                        "Outstanding_Balance",
                        customerDetailsFromDb?.get(0)?.outstandingBalance.toString()
                )
                bundle.putString(
                        "Payment_status",
                        customerDetailsFromDb?.get(0)?.paymentStatus.toString()
                )
                bundle.putString("MOP", customerDetailsFromDb?.get(0)?.mop.toString())
                bundle.putString(
                        "Paid_amount",
                        customerDetailsFromDb?.get(0)?.paidAmount.toString()
                )

                findNavController().navigate(
                        R.id.action_bookingdetails_to_paymentdetails, bundle
                )
            }
        }

        btn_transfer.setOnClickListener {
            if (!transferButtonClicked) {

                Loader.showLoader(requireContext())
                if (requireContext().isConnectedToNetwork()) {
                    transferListFetch()
                } else {
                    Loader.hideLoader()
                    Snackbar.make(outerdetails, "Please connect to internet", Snackbar.LENGTH_LONG)
                            .show()

                }

            }
        }
    }

    private fun startService() {

        val sdf = SimpleDateFormat("hh:mm:ss")
        starttime = sdf.format(Date())

        @SuppressLint("SimpleDateFormat")
        val sdf1 = SimpleDateFormat("yyyy/MM/dd")
        val currentDate = sdf1.format(Date())

        Loader.showLoader(requireContext())
        GlobalScope.launch(Dispatchers.Main) {

            bookingssViewModel.getStartService(
                    requireContext(),
                    prefs.user_id,
                    prefs.user_type,
                    currentDate,
                    prefs.token,
                    booking_id, "1", starttime
            )
        }
    }

    private fun transferdailog() {

        selected_driver= ""
        selected_zone= ""

        if (transferlist?.size != 0) {

            val transferbuilder = AlertDialog.Builder(requireContext())
            val transferview: View =
                    LayoutInflater.from(requireContext()).inflate(R.layout.dialog_transfer, null)
            transferbuilder.setView(transferview)
            val transferalertDialog: AlertDialog = transferbuilder.create()

            rv_transferSchedule = transferview.findViewById(R.id.rv_transferSchedule)

            transferAdapter = TransferAdapter(requireContext(), transferlist) {
                selected_driver = it?.tabletId.toString()
                selected_zone = it?.zoneId.toString()
            }
            val layoutManager = GridLayoutManager(requireContext(), 2)
            rv_transferSchedule.layoutManager = layoutManager
            rv_transferSchedule.adapter = transferAdapter

            val img_closetransfer = transferview.findViewById(R.id.img_closetransfer) as ImageView
            img_closetransfer.setOnClickListener {
                onCustomerViewset()
                transferalertDialog.dismiss()
            }

            val transferButton = transferview.findViewById(R.id.transferButton) as Button

            transferButton.setOnClickListener {
                Log.d("TAG", "transferdailog: "+selected_driver+" , "+selected_zone)

                if (selected_driver.isNullOrEmpty()|| selected_zone.isNullOrEmpty()){
                    Toast.makeText(requireContext(),"   Please select zone   ",Toast.LENGTH_LONG).show()
                }else {
                    transferConfirmationDialog(selected_driver, selected_zone)
                    transferalertDialog.dismiss()
                }
            }
            transferalertDialog.show()

        } else {

            Snackbar.make(outerdetails, "Transfer List is empty", Snackbar.LENGTH_LONG).show()

        }
    }

    private fun transferConfirmationDialog(selected_driver: String, selected_zone: String) {
        Log.d("TAG", "transferConfirmationDialog: " + selected_driver + selected_zone)

        val transferconfirmbuilder = AlertDialog.Builder(requireContext())
        val confirmview: View =
                LayoutInflater.from(requireContext())
                        .inflate(R.layout.dialog_transfer_confirmation, null)
        transferconfirmbuilder.setView(confirmview)
        val transferalertDialog: AlertDialog = transferconfirmbuilder.create()

        val btn_transferall: TextView = confirmview.findViewById(R.id.btn_transferall)
        val btn_selectedonly: TextView = confirmview.findViewById(R.id.btn_selectedonly)
        val img_closeselection: ImageView = confirmview.findViewById(R.id.img_closeselection)

        img_closeselection.setOnClickListener {
            onCustomerViewset()
            transferalertDialog.dismiss()
        }

        btn_transferall.setOnClickListener {
            @SuppressLint("SimpleDateFormat")
            val sdf1 = SimpleDateFormat("yyyy/MM/dd")
            val currentDate = sdf1.format(Date())

            if (requireContext().isConnectedToNetwork()) {

                Loader.showLoader(requireContext())
                GlobalScope.launch(Dispatchers.Main) {
                    bookingssViewModel.getBookingsTransfer(
                            requireContext(),
                            prefs.user_id.toString(),
                            prefs.user_type.toString(),
                            currentDate,
                            prefs.token,
                            booking_id, selected_zone, "1"
                    )
                }
                transferalertDialog.dismiss()

            } else {
                Snackbar.make(outerdetails, "Please connect to internet", Snackbar.LENGTH_LONG)
                        .show()
            }

        }
        btn_selectedonly.setOnClickListener {

            @SuppressLint("SimpleDateFormat")
            val sdf1 = SimpleDateFormat("yyyy/MM/dd")
            val currentDate = sdf1.format(Date())

            if (requireContext().isConnectedToNetwork()) {
                Loader.showLoader(requireContext())
                GlobalScope.launch(Dispatchers.Main) {
                    bookingssViewModel.getBookingsTransfer(
                            requireContext(),
                            prefs.user_id.toString(),
                            prefs.user_type.toString(),
                            currentDate,
                            prefs.token,
                            booking_id, selected_zone, "0"
                    )
                }
                transferalertDialog.dismiss()
            } else {
                Snackbar.make(outerdetails, "Please connect to internet", Snackbar.LENGTH_LONG)
                        .show()
            }
        }

        transferalertDialog.show()
    }


    private fun makePhoneCall() {
        if (customerDetailsFromDb?.get(0)?.customerMobile.isNullOrEmpty()) {
            Toast.makeText(
                    requireContext(),
                    "Contact information not available",
                    Toast.LENGTH_SHORT
            ).show()
        } else {
            val intent = Intent(Intent.ACTION_DIAL)
            //intent.data = Uri.parse("tel:" + "8002258")
            intent.data = Uri.parse("tel:" + customerDetailsFromDb?.get(0)?.customerMobile)
            startActivity(intent)
        }


    }

    private fun hasCallingPermision(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                        requireContext(),
                        Manifest.permission.CALL_PHONE
                ) === PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.CALL_PHONE), REQ_CODE_CALL
        )
        return false
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String?>,
            grantResults: IntArray
    ) {
        when (requestCode) {
            REQ_CODE_CALL -> if (grantResults.size > 0) {
                val CallPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED
                if (CallPermission) {
                    // makePhoneCall(txt_customernumber.text.toString())
                    makePhoneCall()

                }
            }
        }
    }

// -------------------------------------- Timer ----------------------
    // Timer -------------------------------


  /*  private fun resetTimerView() {
        timeInSeconds = 0L
        initStopWatch()
    }

    private fun initStopWatch() {
        txt_timershow.text = getString(R.string.init_stop_watch_value)
    }

    private fun startTimer() {
        mHandler = Handler(Looper.getMainLooper())
        mStatusChecker.run()
    }

    private var mStatusChecker: Runnable = object : Runnable {
        override fun run() {
            try {
                timeInSeconds = timeInSeconds + 1

                Log.e("timeInSeconds", timeInSeconds.toString())
                updateStopWatchView(timeInSeconds)
            } finally {
                mHandler!!.postDelayed(this, mInterval.toLong())
            }
        }
    }

    private fun updateStopWatchView(timeInSeconds: Long) {
        val formattedTime = TimerUtility.getFormattedStopWatch((timeInSeconds * 1000))
        Log.e("formattedTime", formattedTime)

        if (formattedTime.isNotEmpty()) {
            _countdownTimer.value = formattedTime
        }
        // txt_timershow.text = formattedTime

    }

    private fun stopTimer() {
        mHandler?.removeCallbacks(mStatusChecker)
    }

    override fun onDestroy() {
        super.onDestroy()
        stopTimer()
    }*/




    /* private fun toseconds(difftime: String) {

         val h1 = difftime.split(":".toRegex()).toTypedArray()

         val hour = h1[0].toInt()
         val minute = h1[1].toInt()
         val second = h1[2].toInt()

         val temp: Int
         temp = second + 60 * minute + 3600 * hour

         Log.d("secondsss", "onResume: " + temp)

         timeInSeconds = temp.toLong()
         Log.e("timeInSeconds", "toseconds: " + timeInSeconds)
         println("secondsss$temp")

         startTimer()
     }

     fun difference(start: Time, stop: Time): Time {
         val diff = Time(0, 0, 0)

         if (stop.seconds > start.seconds) {
             --start.minutes
             start.seconds += 60
         }

         diff.seconds = start.seconds - stop.seconds
         if (stop.minutes > start.minutes) {
             --start.hours
             start.minutes += 60
         }

         diff.minutes = start.minutes - stop.minutes
         diff.hours = start.hours - stop.hours

         return diff
     }*/

    // --------------------------------------  ----------------------



    private fun MaidClick(maidtable: MaidTable?) {
        if (requireContext().isConnectedToNetwork()) {

            @SuppressLint("SimpleDateFormat")
            val sdf1 = SimpleDateFormat("yyyy/MM/dd")
            val currentDate = sdf1.format(Date())  //booking_date

            var spf = SimpleDateFormat("dd-MM-yyyy")
            val newDatee = spf.parse(booking_date)
            spf = SimpleDateFormat("yyyy-MM-dd")
            val date: String = spf.format(newDatee)


            Loader.showLoader(requireContext())
            GlobalScope.launch(Dispatchers.Main) {
                bookingssViewModel.getMaidbookingsDetailslist(
                        requireContext(),
                        prefs.user_id,
                        prefs.user_type,
                        date,
                        prefs.token,
                        maidtable?.maid_id,
                        booking_id
                )
            }


        } else {
            Snackbar.make(outerdetails, "Please connect to internet", Snackbar.LENGTH_LONG).show()
        }


    }

    private fun clearLogin() {
        // prefs.clearSession(requireContext())
        prefs.user_details = ""
        prefs.user_id = 0
        prefs.user_name = ""
        prefs.user_type = ""
        prefs.token = ""
        prefs.image = ""
        prefs.phone = ""
        prefs.email = ""
        prefs.Latitude = ""
        prefs.Longitude = ""
    }


    override fun onPause() {
        super.onPause()
        try {
            Loader.hideLoader()
        } catch (e: Exception) {
        }
    }

    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
                this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }
}