package com.azinova.emaid_crew.ui.fragment.ui.notification

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.azinova.emaid_crew.R
import com.azinova.emaid_crew.model.response.notification.NotificationDetailsItem

class NotificationAdapter(val context: Context,val notificationDetails: List<NotificationDetailsItem?>?) : RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {

    lateinit var notificationDetailsAdapter: NotificationDetailsAdapter

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): NotificationAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.card_notificationdates, parent, false))

    }

    override fun onBindViewHolder(holder: NotificationAdapter.ViewHolder, position: Int) {

        if (notificationDetails?.get(position)?.notificationList?.size != 0) {

            holder.txt_notificationdate.visibility = View.VISIBLE
            holder.rv_notifications.visibility = View.VISIBLE

            holder.txt_notificationdate.text = notificationDetails?.get(position)?.date

            notificationDetailsAdapter = NotificationDetailsAdapter(
                context,
                notificationDetails?.get(position)?.notificationList
            )
            val lmList = LinearLayoutManager(context)
            lmList.orientation = LinearLayoutManager.VERTICAL
            holder.rv_notifications.setLayoutManager(lmList)
            holder.rv_notifications.setAdapter(notificationDetailsAdapter)

        }else{
            holder.txt_notificationdate.visibility = View.GONE
            holder.rv_notifications.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int {
        return notificationDetails!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
    var rv_notifications = itemView.findViewById<RecyclerView>(R.id.rv_notifications)
    var txt_notificationdate = itemView.findViewById<TextView>(R.id.txt_notificationdate)

}
}