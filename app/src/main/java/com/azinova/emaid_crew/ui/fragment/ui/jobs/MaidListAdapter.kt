package com.azinova.emaid_crew.ui.fragment.ui.jobs

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.azinova.emaid_crew.R

class MaidListAdapter(val context: Context) :
        RecyclerView.Adapter<MaidListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MaidListAdapter.ViewHolder {
      return  ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.card_maidlist, parent, false))
    }

    override fun onBindViewHolder(holder: MaidListAdapter.ViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return 3
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}