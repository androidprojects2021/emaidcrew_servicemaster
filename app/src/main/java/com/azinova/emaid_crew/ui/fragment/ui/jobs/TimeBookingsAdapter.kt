package com.azinova.emaid_crew.ui.fragment.ui.jobs

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.azinova.emaid_crew.R
import com.azinova.emaid_crew.model.response.schedules.SceduleTimeingResponse
import com.azinova.emaid_crew.model.response.schedules.ScheduleResponse
import com.azinova.emaid_crew.utils.SharedPref
import java.text.SimpleDateFormat

class TimeBookingsAdapter(
    val context: Context,
    val bookingsFromDb: List<SceduleTimeingResponse>,
    val onItemClicked: (ScheduleResponse?, Int, List<ScheduleResponse?>?) -> Unit
) :
    RecyclerView.Adapter<TimeBookingsAdapter.ViewHolder>() {
    private val prefs = SharedPref


    lateinit var bookingsAdapter: BookingsAdapter


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TimeBookingsAdapter.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.card_timewise, parent, false)
        )

    }

    override fun onBindViewHolder(holder: TimeBookingsAdapter.ViewHolder, position: Int) {

        if (bookingsFromDb.get(position).scheduleTimeing != null) {

            val _24HourTime = bookingsFromDb.get(position).scheduleTimeing
            val _24HourSDF = SimpleDateFormat("HH:mm")
            val _12HourSDF = SimpleDateFormat("hh:mm a")
            val _24HourDt = _24HourSDF.parse(_24HourTime)


            holder.txt_time.text = _12HourSDF.format(_24HourDt)


            when (prefs.user_type) {
                "0" -> {
                    holder.img_maid.visibility = View.GONE
                    holder.txt_maidcount.visibility = View.GONE
                }
                "1" -> {
                    holder.img_maid.visibility = View.VISIBLE
                    holder.txt_maidcount.visibility = View.VISIBLE
                }
            }
        }

            if (bookingsFromDb.get(position).noOfMaids == 1) {
                holder.txt_maidcount.text =
                    bookingsFromDb.get(position).noOfMaids.toString() + " Maid"
            } else {
                holder.txt_maidcount.text =
                    bookingsFromDb.get(position).noOfMaids.toString() + " Maids"
            }

        if (bookingsFromDb[position].schedulebookings != null && bookingsFromDb[position].schedulebookings!!.isNotEmpty()) {

            bookingsAdapter =
                BookingsAdapter(context, bookingsFromDb[position].schedulebookings)
                { scheduleResponse: ScheduleResponse?, i: Int, list: List<ScheduleResponse?>? ->
                    onItemClicked.invoke(scheduleResponse, i, list)
                }
            val lmList = LinearLayoutManager(context)
            lmList.orientation = LinearLayoutManager.HORIZONTAL
            holder.rv_bookings.setLayoutManager(lmList)
            holder.rv_bookings.setAdapter(bookingsAdapter)

        }

    }

    override fun getItemCount(): Int {
        return bookingsFromDb.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var rv_bookings = itemView.findViewById<RecyclerView>(R.id.rv_bookings)
        var txt_time = itemView.findViewById<TextView>(R.id.txt_time)
        var txt_maidcount = itemView.findViewById<TextView>(R.id.txt_maidcount)
        var img_maid = itemView.findViewById<ImageView>(R.id.img_maid)

    }


}