package com.azinova.emaid_crew.ui.fragment.ui.notification

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.azinova.emaid_crew.R
import com.azinova.emaid_crew.commons.Loader
import com.azinova.emaid_crew.database.EmaidDatabase
import com.azinova.emaid_crew.ui.activity.home.MainActivity
import com.azinova.emaid_crew.ui.activity.login.LoginActivity
import com.azinova.emaid_crew.utils.SharedPref
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_notification.*
import java.text.SimpleDateFormat
import java.util.*


class NotificationFragment : Fragment() {
    private val prefs = SharedPref
    var database: EmaidDatabase? = null

    private lateinit var parent: MainActivity

    private lateinit var notificationViewModel: NotificationViewModel
    private lateinit var rv_notificationlist: RecyclerView
    lateinit var notificationAdapter: NotificationAdapter

    var currentdate: String = " "

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_notification, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        notificationViewModel = ViewModelProvider(this).get(NotificationViewModel::class.java)

        parent = activity as MainActivity

        rv_notificationlist = view.findViewById(R.id.rv_notificationlist)


        setUpNotificationlist()
        observedIn()

    }

    private fun observedIn() {

        notificationViewModel.notificationlistStatus.observe(viewLifecycleOwner, androidx.lifecycle.Observer { notificationlistStatus ->

            if (notificationlistStatus.status.equals("success")) {
                Loader.hideLoader()


                if (notificationlistStatus.notificationDetails?.size != 0) {

                   /* if ((notificationlistStatus.notificationDetails?.get(0)?.notificationList?.size == 0) &&
                            (notificationlistStatus.notificationDetails?.get(1)?.notificationList?.size == 0) &&
                            (notificationlistStatus.notificationDetails?.get(2)?.notificationList?.size == 0)) {
                        rv_notificationlist.visibility = View.GONE
                        l_nonotificationList.visibility = View.VISIBLE
                    } else {*/

                        rv_notificationlist.visibility = View.VISIBLE
                        l_nonotificationList.visibility = View.GONE

                        notificationAdapter = NotificationAdapter(requireContext(), notificationlistStatus.notificationDetails)
                        val lmList = LinearLayoutManager(context)
                        lmList.orientation = LinearLayoutManager.VERTICAL
                        rv_notificationlist.setLayoutManager(lmList)
                        rv_notificationlist.setAdapter(notificationAdapter)
                   // }

                } else {
                    rv_notificationlist.visibility = View.GONE
                    l_nonotificationList.visibility = View.VISIBLE
                }


            } else if (notificationlistStatus.status.equals("token_error")) {
                Loader.hideLoader()
                Toast.makeText(requireContext(), notificationlistStatus.message, Toast.LENGTH_SHORT)
                        .show()


                clearLogin()
                parent.stopServiceTracker()
                database?.clearAllTables()

                val intent = Intent(requireContext(), LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                activity?.finish()

            } else {
                Loader.hideLoader()
                Toast.makeText(
                        requireContext(),
                        notificationlistStatus.message,
                        Toast.LENGTH_SHORT
                )
                        .show()

            }


        })

    }

    private fun setUpNotificationlist() {


        val sdf = SimpleDateFormat("yyyy/MM/dd")
        currentdate = sdf.format(Date())

        Loader.showLoader(requireContext())
        if (requireContext().isConnectedToNetwork()) {
            getNotifications(
                    prefs.user_id.toString(),
                    currentdate,
                    prefs.user_type.toString(),
                    prefs.token
            )
        } else {
            Loader.hideLoader()
            Snackbar.make(outernotifications, "Please connect to internet", Snackbar.LENGTH_LONG).show()
        }


    }

    private fun getNotifications(user_id: String, currentdate: String, user_type: String, token: String?) {
        notificationViewModel.getNotificationList(user_id, currentdate, user_type, token)

    }

    private fun clearLogin() {
        // prefs.clearSession(requireContext())
        prefs.user_details = ""
        prefs.user_id = 0
        prefs.user_name = ""
        prefs.user_type = ""
        prefs.token = ""
        prefs.image = ""
        prefs.phone = ""
        prefs.email = ""
        prefs.Latitude = ""
        prefs.Longitude = ""
    }

    override fun onPause() {
        super.onPause()
        try {
            Loader.hideLoader()
        } catch (e: Exception) {
        }
    }

    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
                this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }
}