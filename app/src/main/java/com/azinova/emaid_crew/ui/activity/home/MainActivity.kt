package com.azinova.emaid_crew.ui.activity.home

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.azinova.emaid_crew.R
import com.azinova.emaid_crew.commons.ExitAppDialog
import com.azinova.emaid_crew.commons.Loader
import com.azinova.emaid_crew.database.EmaidDatabase
import com.azinova.emaid_crew.service.LocationService
import com.azinova.emaid_crew.ui.activity.login.LoginActivity
import com.azinova.emaid_crew.utils.PermissionUtils
import com.azinova.emaid_crew.utils.SharedPref
import com.azinova.emaid_crew.utils.notification.BaseActivity
import com.azinova.emaid_crew.utils.notification.Myservices
import com.azinova.emaid_crew.utils.notification.NotificationDioalogue
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.fragment_more.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : BaseActivity(), ExitAppDialog.ExitClick {
    private lateinit var navController: NavController
    private lateinit var bottomNavigationView: BottomNavigationView

    private lateinit var mainActivityViewModel: MainActivityViewModel

    companion object {
        private var REQUEST_LOCATION_CODE = 101
    }

    private val prefs = SharedPref
    var database: EmaidDatabase? = null


    private lateinit var txt_title: TextView
   // private lateinit var img_filter: ImageView
    private lateinit var img_notification: ImageView
    private lateinit var img_calender: ImageView
    private lateinit var img_sync: ImageView


    var newdate: String = " "

    lateinit var datePickerDialog: DatePickerDialog
    var year = 0
    var month = 0
    var dayOfMonth = 0
    lateinit var calendar: Calendar

    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        mainActivityViewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)

        Log.d("Emaid crew test", "generateNotification: " + intent.extras)

        if (intent.extras != null) {
            if (intent.extras!!.getString("message") != null) {

                val myIntent = Intent(this, Myservices::class.java)
                myIntent.putExtra("message", intent.extras!!.getString("message"))
                myIntent.putExtra("heading", "Notification")
                startService(myIntent)
            }
        }


        onLocation()

        try {
            if (intent.extras!!.getString("page")
                    .equals("notification")
            ) {
                NotificationDioalogue.synchrinoouserror(
                    this,
                    "Update Error", "Please Update Again"
                )
            }
        } catch (e: Exception) {
            // TODO: handle exception
        }

        //Getting the Navigation Controller
        navController = Navigation.findNavController(this, R.id.nav_host_fragment)

        bottomNavigationView = findViewById(R.id.bottomNavigationView)

        txt_title = findViewById(R.id.txt_title)
        img_notification = findViewById(R.id.img_notification)
        img_calender = findViewById(R.id.img_calender)
        img_sync = findViewById(R.id.img_sync)


        bottomNavigationView.setupWithNavController(navController)





        onDestination()
        onclick()
        onObserve()
    }



    private fun onLocation() {
        when {
            PermissionUtils.isAccessFineLocationGranted(this) -> {
                when {
                    PermissionUtils.isLocationEnabled(this) -> {
                        serviceGps()
                    }
                    else -> {
                        PermissionUtils.showGPSNotEnabledDialog(this)
                    }
                }
            }
            else -> {
                PermissionUtils.requestAccessFineLocationPermission(
                    this,
                    REQUEST_LOCATION_CODE
                )


            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_LOCATION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    when {
                        PermissionUtils.isLocationEnabled(this) -> {
                        }
                        else -> {

                            PermissionUtils.showGPSNotEnabledDialog(this)
                        }
                    }
                } else {
                    Toast.makeText(
                        this,
                        "Location Permission not granted",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        onLocation()

    }

    private fun onObserve() {
        mainActivityViewModel.bookingsDetailsSyncStatus.observe(
            this,
            androidx.lifecycle.Observer { bookingsDetailsSyncStatus ->
                if (bookingsDetailsSyncStatus.status.equals("success")) {
                    Loader.hideLoader()
                    Toast.makeText(
                        this,
                        "Updated",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (bookingsDetailsSyncStatus.status.equals("token_error")) {
                    Loader.hideLoader()
                    Toast.makeText(this, bookingsDetailsSyncStatus.message, Toast.LENGTH_SHORT)
                        .show()


                    clearLogin()
                    this.stopServiceTracker()
                    database?.clearAllTables()

                    val intent = Intent(this, LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    this.finish()

                } else {
                    Loader.hideLoader()
                    Toast.makeText(
                        this,
                        bookingsDetailsSyncStatus.message,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
    }

    private fun clearLogin() {
        // prefs.clearSession(requireContext())
        prefs.user_details = ""
        prefs.user_id = 0
        prefs.user_name = ""
        prefs.user_type = ""
        prefs.token = ""
        prefs.image = ""
        prefs.phone = ""
        prefs.email = ""
        prefs.Latitude = ""
        prefs.Longitude = ""
    }

    private fun serviceGps() {
        ContextCompat.startForegroundService(this, Intent(this, LocationService::class.java))

        // startService(Intent(this, LocationService::class.java))

        Log.e("serv ", "start")
    }

    private fun onclick() {
        img_notification.setOnClickListener {
            findNavController(R.id.nav_host_fragment).navigate(R.id.navigation_notification)
        }
        img_back.setOnClickListener {
            onBackPressed()
        }
        img_search.setOnClickListener {
            search_view.showSearch()
        }

        @SuppressLint("SimpleDateFormat")
        val sdf = SimpleDateFormat("yyyy/MM/dd")
        val currentDate = sdf.format(Date())

        img_sync?.setOnClickListener {
            Loader.showLoader(this)
            if (this.isConnectedToNetwork()) {
                GlobalScope.launch(Dispatchers.Main) {
                    mainActivityViewModel.fetDatFromServer(applicationContext, currentDate)
                }
            } else {
                Loader.hideLoader()
                Snackbar.make(outermore, "Please connect to internet", Snackbar.LENGTH_LONG).show()
            }

        }
    }

    private fun onDestination() {

        val sdf = SimpleDateFormat("dd/MM/yyyy")
        newdate = sdf.format(Date())

        findNavController(R.id.nav_host_fragment).addOnDestinationChangedListener { controller, destination, arguments ->
            when (destination.id) {
                R.id.navigation_jobs -> {

                    txt_title.text = "Bookings"
                    toolbar_layout.visibility = View.VISIBLE
                    layout_menu.visibility = View.VISIBLE
                    layout_search.visibility = View.GONE
                    img_filter.visibility = View.VISIBLE
                    img_notification.visibility = View.VISIBLE
                    img_calender.visibility = View.GONE
                    img_sync.visibility = View.GONE
                    cv_bottomNavigationView.visibility = View.VISIBLE
                }
                R.id.navigation_payment -> {

                    txt_title.text = newdate
                    toolbar_layout.visibility = View.VISIBLE
                    layout_menu.visibility = View.VISIBLE
                    layout_search.visibility = View.GONE
                    img_calender.visibility = View.VISIBLE
                    img_notification.visibility = View.VISIBLE
                    img_filter.visibility = View.GONE
                    img_sync.visibility = View.GONE
                    cv_bottomNavigationView.visibility = View.VISIBLE

                }
                R.id.navigation_report -> {

                    txt_title.text = "Booking Report"
                    toolbar_layout.visibility = View.VISIBLE
                    layout_menu.visibility = View.GONE
                    layout_search.visibility = View.GONE
                    img_calender.visibility = View.GONE
                    img_filter.visibility = View.GONE
                    img_notification.visibility = View.VISIBLE
                    img_sync.visibility = View.GONE
                    cv_bottomNavigationView.visibility = View.VISIBLE

                }
                R.id.navigation_more -> {

                    txt_title.text = ""
                    toolbar_layout.visibility = View.VISIBLE
                    layout_menu.visibility = View.VISIBLE
                    layout_search.visibility = View.GONE
                    img_calender.visibility = View.GONE
                    img_filter.visibility = View.GONE
                    img_notification.visibility = View.GONE
                    img_sync.visibility = View.VISIBLE
                    cv_bottomNavigationView.visibility = View.VISIBLE

                }
                R.id.navigation_editprofile -> {
                    txt_moretitle.text = "Edit Profile"
                    toolbar_layout.visibility = View.GONE
                    img_search.visibility = View.GONE
                    layout_search.visibility = View.VISIBLE
                    cv_bottomNavigationView.visibility = View.GONE

                }
                R.id.navigation_notification -> {
                    txt_moretitle.text = "Notifications"

                    img_search.visibility = View.GONE
                    toolbar_layout.visibility = View.GONE
                    layout_search.visibility = View.VISIBLE
                    cv_bottomNavigationView.visibility = View.GONE

                }
                R.id.navigation_maidattendence -> {
                    txt_moretitle.text = "Maid Attendance"

                    img_search.visibility = View.VISIBLE
                    toolbar_layout.visibility = View.GONE
                    layout_search.visibility = View.VISIBLE
                    cv_bottomNavigationView.visibility = View.GONE

                }
                else -> {
                    /*supportActionBar?.title = null
                    actionbar?.setDisplayHomeAsUpEnabled(false)
                    actionbar?.setDisplayShowHomeEnabled(false)*/

                    txt_title.text = ""
                    toolbar_layout.visibility = View.VISIBLE
                    layout_search.visibility = View.GONE
                    img_calender.visibility = View.GONE
                    img_filter.visibility = View.GONE
                    img_notification.visibility = View.GONE
                    img_sync.visibility = View.GONE
                    cv_bottomNavigationView.visibility = View.VISIBLE

                }

            }
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        if (search_view.isSearchOpen) {
            search_view.closeSearch()
        } else if (findNavController(R.id.nav_host_fragment).currentDestination?.id == R.id.navigation_jobs) {
            ExitAppDialog.showDialog(this, this)
        } else {
            super.onBackPressed()
        }
    }

    override fun proceedExit() {
        finish()
    }

    fun stopServiceTracker(){
        stopService(Intent(this, LocationService::class.java))

        //stopService(Intent(this, LocationService::class.java))
    }

    override fun onPause() {
        super.onPause()
        try {
            Loader.hideLoader()
        } catch (e: Exception) {
        }
    }

    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }
}