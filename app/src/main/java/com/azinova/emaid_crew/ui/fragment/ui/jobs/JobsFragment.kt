package com.azinova.emaid_crew.ui.fragment.ui.jobs

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.azinova.emaid_crew.R
import com.azinova.emaid_crew.commons.Loader
import com.azinova.emaid_crew.database.EmaidDatabase
import com.azinova.emaid_crew.database.repository.BookingsRepository
import com.azinova.emaid_crew.model.response.schedules.SceduleTimeingResponse
import com.azinova.emaid_crew.model.response.schedules.ScheduleResponse
import com.azinova.emaid_crew.ui.activity.detail.BookingDetailsActivity
import com.azinova.emaid_crew.ui.activity.home.MainActivity
import com.azinova.emaid_crew.ui.activity.login.LoginActivity
import com.azinova.emaid_crew.utils.SharedPref
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_jobs.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList


class JobsFragment : Fragment() {

    private lateinit var jobsViewModel: JobsViewModel

    private lateinit var parent: MainActivity
    var database: EmaidDatabase? = null

    private val prefs = SharedPref

    var newbookinglist: MutableList<SceduleTimeingResponse> = ArrayList()

    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var l_nobookingsList: LinearLayout

    private lateinit var rv_bookingdates: RecyclerView
    private lateinit var rv_bookingtimeing: RecyclerView
    lateinit var dateBookingsAdapter: DateBookingsAdapter
    var timeBookingsAdapter: TimeBookingsAdapter? = null

    var FilterPosition: Int = -1

    var CheckPosition = 0
    lateinit var alertDialog: AlertDialog

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_jobs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        jobsViewModel = ViewModelProvider(this).get(JobsViewModel::class.java)

        parent = activity as MainActivity

        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout)
        l_nobookingsList = view.findViewById(R.id.l_nobookingsList)

        rv_bookingdates = view.findViewById(R.id.rv_bookingdates)
        rv_bookingtimeing = view.findViewById(R.id.rv_bookingtimeing)

        rv_bookingtimeing.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        rv_bookingdates.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)

        CheckPosition = 0
        callDateData(FilterPosition)
        observe()
        clicks()
    }

    override fun onResume() {
        super.onResume()

        CheckPosition = 0

        callDateData(FilterPosition)
        Log.d("TAG", "Position: 2 - " + CheckPosition)

    }

    private fun callDateData(FilterPosition: Int) {
        val sdf = SimpleDateFormat("dd-MM-yyyy")
        //val sdf = "25-06-2021"
        var currentdate = sdf.format(Date())

        // DatesFromDb
        GlobalScope.launch(Dispatchers.Main){
            try {


                val dateArray = jobsViewModel.getBookingDates(requireContext())
                //new change
//                if (dateArray.contains(currentdate)){
//                    Log.e("Test", "CurentDate " )
//
//                }else{
//                    Log.e("Test", "CurentDateAdded " )
//                    dateArray.add(currentdate)
//                }
                if (dateArray != null) {
                    if (dateArray.isEmpty()) {


                        rv_bookingtimeing.visibility = View.GONE
                        l_nobookingsList.visibility = View.VISIBLE

                    } else {
                        Log.e("test", "callDate:$dateArray ")

                        val dateTimeFormatter: DateTimeFormatter = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                            DateTimeFormatter.ofPattern("dd-MM-yyyy")


                        } else {
                            TODO("VERSION.SDK_INT < O")
                        }

                        val sortlist = dateArray?.sortedBy {
                            LocalDate.parse(it, dateTimeFormatter)
                        }

                        sortlist?.let { date ->


                            dateBookingsAdapter = DateBookingsAdapter(requireContext(), date) {
                                if (it.isNotEmpty()) {
                                    callBookingDate(it, FilterPosition)
                                }
                            }
                            rv_bookingdates.adapter = dateBookingsAdapter


                            when (CheckPosition) {
                                0 -> {
                                    val datelist: List<String> = date
                                    val selected = datelist.filter { it.equals(currentdate) }
                                    Log.e("TAG", "callDateData: " + selected)

                                    if (selected.isEmpty()) {

                                            insertDate(currentdate)

//                                        val builder = AlertDialog.Builder(requireContext())
//
//                                        val sync_view: View =
//                                                LayoutInflater.from(requireContext())
//                                                        .inflate(R.layout.dialog_sync, null)
//                                        builder.setView(sync_view)
//
//                                        alertDialog = builder.create()
//                                        alertDialog.setCancelable(true)
//
//                                        val txt_enable_sync_now: TextView = sync_view.findViewById(R.id.txt_enable_sync_now)
//                                        val txt_cancel: TextView = sync_view.findViewById(R.id.txt_cancel)
//
//                                        txt_enable_sync_now.setOnClickListener {
//
//                                            if (requireContext().isConnectedToNetwork()) {
//                                                fetDatFromServer(requireContext())
//                                                alertDialog.dismiss()
//                                            } else {
//
//                                                Snackbar.make(
//                                                        outerjobs,
//                                                        "Please connect to internet",
//                                                        Snackbar.LENGTH_LONG
//                                                ).show()
//                                            }
//                                        }
//
//                                        txt_cancel.setOnClickListener {
//
//                                            alertDialog.dismiss()
//                                        }
//
//                                        alertDialog.show()

                                    }
                                }
                            }


                        }
                    }

                    }


            }


            catch (e: Exception) {
                Log.e("callDateData", "Exception: ${e.message}" )

                    clearLogin()
                    parent.stopServiceTracker()
                    BookingsRepository.database?.clearAllTables()

                    val intent = Intent(requireContext(), LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    activity?.finish()

            }

        }

    }

    private fun insertDate(currentdate: String) {
        GlobalScope.launch {
            jobsViewModel.insertDate(currentdate)
        }

    }


    private fun callBookingDate(date: String, filterPosition: Int) {

        GlobalScope.launch(Dispatchers.Main) {

            val bookingTimeList = jobsViewModel.getBookingTimeings(requireContext(), date, filterPosition)
            val bookingList = SceduleTimeingResponse()

            bookingTimeList?.let {
                newbookinglist = ArrayList()
                newbookinglist.clear()
                newbookinglist = it


                if ((newbookinglist.size == 0)  ) {

                    rv_bookingtimeing.visibility = View.GONE
                    l_nobookingsList.visibility = View.VISIBLE
                    Snackbar.make(outerjobs, "No bookings Found", Snackbar.LENGTH_SHORT).show()


                } else if (newbookinglist.get(0).schedulebookings?.get(0)?.booking_id == null ){
                    rv_bookingtimeing.visibility = View.GONE
                    l_nobookingsList.visibility = View.VISIBLE
                    Snackbar.make(outerjobs, "No bookings Found", Snackbar.LENGTH_SHORT).show()

                }
                else {
                    Log.d("Status Based  .......", "callBookingDate: " + newbookinglist)

                    l_nobookingsList.visibility = View.GONE
                    rv_bookingtimeing.visibility = View.VISIBLE

                    timeBookingsAdapter = TimeBookingsAdapter(
                            requireContext(),
                            newbookinglist
                    ) { scheduleResponse: ScheduleResponse?, i: Int, list: List<ScheduleResponse?>? ->

                        val intent = Intent(requireContext(), BookingDetailsActivity::class.java)
                        intent.putExtra("Customer_id", scheduleResponse?.customer_id.toString())
                        intent.putExtra("Booking_id", scheduleResponse?.booking_id.toString())
                        intent.putExtra("Schedule_date", scheduleResponse?.schedule_date.toString())
                        intent.putExtra("Service_status", scheduleResponse?.service_status.toString())
                        startActivity(intent)
                    }
                    rv_bookingtimeing.adapter = timeBookingsAdapter

                }


                       /* if (newbookinglist.size != 0 || newbookinglist[0].schedulebookings?.get(0)?.booking_id != 0) {
                            Log.d("Status Based  .......", "callBookingDate: " + newbookinglist)

                            l_nobookingsList.visibility = View.GONE
                            rv_bookingtimeing.visibility = View.VISIBLE

                            timeBookingsAdapter = TimeBookingsAdapter(
                                    requireContext(),
                                    newbookinglist
                            ) { scheduleResponse: ScheduleResponse?, i: Int, list: List<ScheduleResponse?>? ->

                                val intent = Intent(requireContext(), BookingDetailsActivity::class.java)
                                intent.putExtra("Customer_id", scheduleResponse?.customer_id.toString())
                                intent.putExtra("Booking_id", scheduleResponse?.booking_id.toString())
                                intent.putExtra("Schedule_date", scheduleResponse?.schedule_date.toString())
                                intent.putExtra("Service_status", scheduleResponse?.service_status.toString())
                                startActivity(intent)
                            }
                            rv_bookingtimeing.adapter = timeBookingsAdapter
                        } else {
                            rv_bookingtimeing.visibility = View.GONE
                            l_nobookingsList.visibility = View.VISIBLE
                            Snackbar.make(outerjobs, "No bookings Found", Snackbar.LENGTH_LONG).show()

                        }*/

            }
        }
    }


    private fun clicks() {


        swipeRefreshLayout.setOnRefreshListener {

            if (requireContext().isConnectedToNetwork()) {
                fetDatFromServer(requireContext())
            } else {
                swipeRefreshLayout.isRefreshing = false
                Snackbar.make(outerjobs, "Please connect to internet", Snackbar.LENGTH_LONG).show()
            }

        }

        requireActivity().img_filter.setOnClickListener {

            val builder = AlertDialog.Builder(requireContext())
            val view: View =
                    LayoutInflater.from(requireContext())
                            .inflate(R.layout.dialog_filter, null)
            builder.setView(view)
            val alertDialog = builder.create()

            val checkbox_all = view.findViewById<CheckBox>(R.id.checkbox_condition_all)
            val checkbox_pending = view.findViewById<CheckBox>(R.id.checkbox_conditions_pending)
            val checkbox_Ongoing = view.findViewById<CheckBox>(R.id.checkbox_conditions_Ongoing)
            val checkbox_Finished = view.findViewById<CheckBox>(R.id.checkbox_conditions_Finished)
            val checkbox_Cancelled = view.findViewById<CheckBox>(R.id.checkbox_conditions_Cancelled)

            CheckPosition = 1

            when (FilterPosition) {
                -1 -> checkbox_all.isChecked = true
                0 -> checkbox_pending.isChecked = true
                1 -> checkbox_Ongoing.isChecked = true
                2 -> checkbox_Finished.isChecked = true
                3 -> checkbox_Cancelled.isChecked = true

            }

            checkbox_all.setOnClickListener {
                FilterPosition = -1
                checkbox_all.isChecked = true
                checkbox_pending.isChecked = false
                checkbox_Ongoing.isChecked = false
                checkbox_Finished.isChecked = false
                checkbox_Cancelled.isChecked = false
                callDateData(FilterPosition)

            }
            checkbox_pending.setOnClickListener {
                FilterPosition = 0
                checkbox_pending.isChecked = true
                checkbox_all.isChecked = false
                checkbox_Ongoing.isChecked = false
                checkbox_Finished.isChecked = false
                checkbox_Cancelled.isChecked = false
                callDateData(FilterPosition)

            }
            checkbox_Ongoing.setOnClickListener {
                FilterPosition = 1
                checkbox_Ongoing.isChecked = true
                checkbox_all.isChecked = false
                checkbox_pending.isChecked = false
                checkbox_Finished.isChecked = false
                checkbox_Cancelled.isChecked = false
                callDateData(FilterPosition)

            }
            checkbox_Finished.setOnClickListener {
                FilterPosition = 2
                checkbox_Finished.isChecked = true
                checkbox_all.isChecked = false
                checkbox_pending.isChecked = false
                checkbox_Ongoing.isChecked = false
                checkbox_Cancelled.isChecked = false
                callDateData(FilterPosition)

            }
            checkbox_Cancelled.setOnClickListener {
                FilterPosition = 3
                checkbox_Cancelled.isChecked = true
                checkbox_all.isChecked = false
                checkbox_pending.isChecked = false
                checkbox_Ongoing.isChecked = false
                checkbox_Finished.isChecked = false
                callDateData(FilterPosition)

            }


            alertDialog.show()
        }
    }

    private fun fetDatFromServer(context: Context) {

        @SuppressLint("SimpleDateFormat")
        val sdf = SimpleDateFormat("yyyy/MM/dd")
        val currentDate = sdf.format(Date())

        GlobalScope.launch(Dispatchers.Main) {
            jobsViewModel.fetDatFromServer(context, currentDate)

            CheckPosition = 1
            callDateData(FilterPosition)
        }
    }

    private fun observe() {
        jobsViewModel.bookingsDetailsRefresh.observe(
                viewLifecycleOwner,
                Observer { bookingsDetailsRefresh ->

                    if (bookingsDetailsRefresh.status.equals("success")) {
                        swipeRefreshLayout.isRefreshing = false
                        CheckPosition = 1

                        callDateData(FilterPosition)
                        Toast.makeText(
                                requireContext(),
                                "Refreshed",
                                Toast.LENGTH_SHORT
                        ).show()
                    } else if (bookingsDetailsRefresh.status.equals("token_error")) {
                        swipeRefreshLayout.isRefreshing = false
                        Toast.makeText(requireContext(), bookingsDetailsRefresh.message, Toast.LENGTH_SHORT)
                                .show()


                        clearLogin()
                        parent.stopServiceTracker()
                        database?.clearAllTables()

                        val intent = Intent(requireContext(), LoginActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        startActivity(intent)
                        activity?.finish()

                    } else {
                        swipeRefreshLayout.isRefreshing = false
                        Toast.makeText(requireContext(), bookingsDetailsRefresh.message, Toast.LENGTH_SHORT)
                                .show()
                    }

                })
    }

    private fun clearLogin() {
        // prefs.clearSession(requireContext())
        prefs.user_details = ""
        prefs.user_id = 0
        prefs.user_name = ""
        prefs.user_type = ""
        prefs.token = ""
        prefs.image = ""
        prefs.phone = ""
        prefs.email = ""
        prefs.Latitude = ""
        prefs.Longitude = ""
    }

    override fun onPause() {
        super.onPause()
        try {
            Loader.hideLoader()
        } catch (e: Exception) {
        }
    }

    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
                this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }

//    override fun onDestroy() {
//        val builder = AlertDialog.Builder(requireActivity())
//        alertDialog = builder.create()
//        if (alertDialog!=null && alertDialog.isShowing){
//            alertDialog.show()
//        }else{
//            alertDialog.cancel()
//        }
//        super.onDestroy()
//    }

}