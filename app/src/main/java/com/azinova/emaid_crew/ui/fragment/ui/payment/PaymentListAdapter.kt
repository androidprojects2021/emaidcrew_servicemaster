package com.azinova.emaid_crew.ui.fragment.ui.payment

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.azinova.emaid_crew.R
import com.azinova.emaid_crew.model.response.payment.PaymentListItem
import com.bumptech.glide.Glide
import java.text.SimpleDateFormat

class PaymentListAdapter(val context: Context, val paymentList: List<PaymentListItem?>?) : RecyclerView.Adapter<PaymentListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentListAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.card_paymentlist, parent, false))

    }

    override fun onBindViewHolder(holder: PaymentListAdapter.ViewHolder, position: Int) {
        if (!(position % 2 == 1)) {
            holder.layout_paylist.setBackgroundColor(ContextCompat.getColor(context, R.color.ultra_light))
        }
        val _24HourTimeshiftStart = paymentList?.get(position)?.shiftStart
        val _24HourSDFS = SimpleDateFormat("HH:mm")
        val _12HourSDFS = SimpleDateFormat("hh:mm a")
        val shiftStart = _24HourSDFS.parse(_24HourTimeshiftStart)

        val _24HourTimeshiftEnd= paymentList?.get(position)?.shiftEnd
        val _24HourSDF = SimpleDateFormat("HH:mm")
        val _12HourSDF = SimpleDateFormat("hh:mm a")
        val shiftEnd = _24HourSDF.parse(_24HourTimeshiftEnd)


        holder.tv_name.text = paymentList?.get(position)?.customerName
        holder.tv_time.text = _12HourSDFS.format(shiftStart) + " - " + _12HourSDF.format(shiftEnd)
        holder.txt_billingvalue.text = "AED " + paymentList?.get(position)?.billing
        holder.txt_collectedvalue.text = "AED " + paymentList?.get(position)?.collected
        if (paymentList?.get(position)?.paymentstatus == 0) {
            holder.imgstatus.setImageResource(R.drawable.ic_pending_icon)

        } else if (paymentList?.get(position)?.paymentstatus == 1) {
            holder.imgstatus.setImageResource(R.drawable.ic_finished_icon)
        }

        if (paymentList?.get(position)?.maidDetails!!.isNotEmpty()) {
            if (paymentList?.get(position)?.maidDetails?.size!! >= 3) {

                holder.txt_maid1.visibility = View.VISIBLE
                holder.img_maid1.visibility = View.VISIBLE
                holder.txt_maid2.visibility = View.VISIBLE
                holder.img_maid2.visibility = View.VISIBLE
                holder.txt_maid3.visibility = View.VISIBLE
                holder.img_maid3.visibility = View.VISIBLE

                holder.txt_maid1.text = paymentList.get(position)?.maidDetails?.get(0)?.maidName
                holder.txt_maid2.text = paymentList.get(position)?.maidDetails?.get(1)?.maidName
                holder.txt_maid3.text = paymentList.get(position)?.maidDetails?.get(2)?.maidName

                Glide.with(context)
                    .load(paymentList.get(position)?.maidDetails?.get(0)?.maidimage)
                    .placeholder(R.drawable.nomaidimage)
                    .error(R.drawable.nomaidimage)
                    .into(holder.img_maid1)
                Glide.with(context)
                    .load(paymentList.get(position)?.maidDetails?.get(1)?.maidimage)
                    .placeholder(R.drawable.nomaidimage)
                    .error(R.drawable.nomaidimage)
                    .into(holder.img_maid2)
                Glide.with(context)
                    .load(paymentList.get(position)?.maidDetails?.get(3)?.maidimage)
                    .placeholder(R.drawable.nomaidimage)
                    .error(R.drawable.nomaidimage)
                    .into(holder.img_maid3)


                val rem = paymentList.get(position)?.maidDetails?.size!! - 3
                if (rem > 0) {
                    holder.txt_remaining.text = "+" + rem
                } else {
                    holder.txt_remaining.visibility = View.INVISIBLE
                }
            } else {
                val maidarraysize = paymentList.get(position)?.maidDetails?.size!!
                if (maidarraysize == 1) {

                    holder.txt_maid1.visibility = View.VISIBLE
                    holder.txt_maid2.visibility = View.INVISIBLE
                    holder.txt_maid3.visibility = View.INVISIBLE

                    holder.img_maid1.visibility = View.VISIBLE
                    holder.img_maid2.visibility = View.INVISIBLE
                    holder.img_maid3.visibility = View.INVISIBLE

                    holder.txt_remaining.visibility = View.INVISIBLE

                    holder.txt_maid1.text = paymentList.get(position)?.maidDetails?.get(0)?.maidName
                    Glide.with(context)
                        .load(paymentList.get(position)?.maidDetails?.get(0)?.maidimage)
                        .placeholder(R.drawable.nomaidimage)
                        .error(R.drawable.nomaidimage)
                        .into(holder.img_maid1)
                } else if (maidarraysize == 2) {

                    holder.txt_maid1.visibility = View.VISIBLE
                    holder.txt_maid2.visibility = View.VISIBLE
                    holder.txt_maid3.visibility = View.INVISIBLE

                    holder.img_maid1.visibility = View.VISIBLE
                    holder.img_maid2.visibility = View.VISIBLE
                    holder.img_maid3.visibility = View.INVISIBLE

                    holder.txt_remaining.visibility = View.INVISIBLE

                    holder.txt_maid1.text = paymentList.get(position)?.maidDetails?.get(0)?.maidName
                    holder.txt_maid2.text = paymentList.get(position)?.maidDetails?.get(1)?.maidName

                    Glide.with(context)
                        .load(paymentList.get(position)?.maidDetails?.get(0)?.maidimage)
                        .placeholder(R.drawable.nomaidimage)
                        .error(R.drawable.nomaidimage)
                        .into(holder.img_maid1)
                    Glide.with(context)
                        .load(paymentList.get(position)?.maidDetails?.get(1)?.maidimage)
                        .placeholder(R.drawable.nomaidimage)
                        .error(R.drawable.nomaidimage)
                        .into(holder.img_maid2)
                }
            }
        }

    }

    override fun getItemCount(): Int {
        return paymentList!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var layout_paylist = itemView.findViewById<ConstraintLayout>(R.id.layout_paylist)
        var imgstatus = itemView.findViewById<ImageView>(R.id.imgstatus)
        var tv_name = itemView.findViewById<TextView>(R.id.tv_name)
        var tv_time = itemView.findViewById<TextView>(R.id.tv_time)
        var txt_billingvalue = itemView.findViewById<TextView>(R.id.txt_billingvalue)
        var txt_collectedvalue = itemView.findViewById<TextView>(R.id.txt_collectedvalue)
        var txt_maid1 = itemView.findViewById<TextView>(R.id.txt_maid1)
        var txt_maid2 = itemView.findViewById<TextView>(R.id.txt_maid2)
        var txt_maid3 = itemView.findViewById<TextView>(R.id.txt_maid3)
        var txt_remaining = itemView.findViewById<TextView>(R.id.txt_remaining)
        var img_maid1 = itemView.findViewById<ImageView>(R.id.img_maid1)
        var img_maid2 = itemView.findViewById<ImageView>(R.id.img_maid2)
        var img_maid3 = itemView.findViewById<ImageView>(R.id.img_maid3)


    }
}