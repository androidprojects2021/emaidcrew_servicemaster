package com.azinova.emaid_crew.ui.fragment.ui.booking_details.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.azinova.emaid_crew.R
import com.azinova.emaid_crew.model.response.transfer.TransferListItem

class TransferAdapter(val requireContext: Context, val transferlist: List<TransferListItem?>?,val itemSelected: (TransferListItem?) -> Unit) : RecyclerView.Adapter<TransferAdapter.ViewHolder>() {
    private var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransferAdapter.ViewHolder {
        return ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                        R.layout.card_transferlist,
                        parent,
                        false
                )
        )
    }

    override fun onBindViewHolder(holder: TransferAdapter.ViewHolder, position: Int) {
        holder.txt_drivername.text = transferlist?.get(position)?.tabletDriver
        holder.txt_zone.text = transferlist?.get(position)?.zoneName

        if (position == selectedPosition) {
            itemSelected.invoke(transferlist?.get(position))

            holder.l_transfercell.setBackgroundResource(R.drawable.bg_purple)
            holder.txt_drivername.setTextColor(Color.WHITE)
            holder.txt_zone.setTextColor(Color.WHITE)
        } else {
            holder.l_transfercell.setBackgroundResource(R.drawable.bg_square_purple)
            holder.txt_drivername.setTextColor(Color.parseColor("#5f467f"))
            holder.txt_zone.setTextColor(Color.parseColor("#5f467f"))
        }

        holder.l_transfercell.setOnClickListener {
            onStateChangedListener(position, holder)
        }
    }

    private fun onStateChangedListener(position: Int, holder: TransferAdapter.ViewHolder) {
        selectedPosition = position
        itemSelected.invoke(transferlist?.get(position))

        holder.l_transfercell.setBackgroundResource(R.drawable.bg_purple)
        holder.txt_drivername.setTextColor(Color.WHITE)
        holder.txt_zone.setTextColor(Color.WHITE)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return transferlist!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txt_drivername = itemView.findViewById<TextView>(R.id.txt_drivername)
        var txt_zone = itemView.findViewById<TextView>(R.id.txt_zone)
        var l_transfercell = itemView.findViewById<ConstraintLayout>(R.id.l_transfercell)

    }
}