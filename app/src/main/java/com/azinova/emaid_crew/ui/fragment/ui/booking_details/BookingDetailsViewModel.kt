package com.azinova.emaid_crew.ui.fragment.ui.booking_details

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.azinova.emaid_crew.database.repository.BookingsRepository
import com.azinova.emaid_crew.database.tables.BookingsTable
import com.azinova.emaid_crew.database.tables.MaidTable
import com.azinova.emaid_crew.database.tables.TimeCountdownTable
import com.azinova.emaid_crew.model.response.maiddetails.ResponseMaidBookingDetailsList
import com.azinova.emaid_crew.model.response.maiddetails.ResponseMaidDetailsList
import com.azinova.emaid_crew.model.response.maidstatus.ResponseMaidStatusChange
import com.azinova.emaid_crew.model.response.start.ResponseStartService
import com.azinova.emaid_crew.model.response.transfer.ResponseBookingTransfer
import com.azinova.emaid_crew.model.response.transfer.ResponseTransferlist
import com.azinova.emaid_crew.model.response.transfer.TransferDetailsItem
import com.azinova.emaid_crew.network.ApiClient
import com.azinova.emaid_crew.utils.SharedPref
import java.text.SimpleDateFormat
import java.util.*

class BookingDetailsViewModel : ViewModel() {
    private val prefs = SharedPref

    var customerDetailsFromDb: LiveData<List<BookingsTable>>? = null
    // var customerDetailsFromDb: List<BookingsTable>? = null

    var maidlistFromDb: LiveData<List<MaidTable>>? = null
    // var maidlistFromDb: List<MaidTable>? = null


    var countdatalist: List<TimeCountdownTable>? = null

    var newmaidlistFromDb: List<MaidTable>? = null
    var newCustomerDetailsFromDb: List<BookingsTable>? = null

    var transfereddatalist: List<TransferDetailsItem>? = null

    private val _maidlistStatus = MutableLiveData<ResponseMaidDetailsList?>()
    val maidlistStatus: LiveData<ResponseMaidDetailsList?>
        get() = _maidlistStatus

    private val _TransferlistStatus = MutableLiveData<ResponseTransferlist?>()
    val TransferlistStatus: LiveData<ResponseTransferlist?>
        get() = _TransferlistStatus

    private val _transferStatus = MutableLiveData<ResponseBookingTransfer?>()
    val transferStatus: LiveData<ResponseBookingTransfer?>
        get() = _transferStatus

    private val _startServiceStatus = MutableLiveData<ResponseStartService?>()
    val startServiceStatus: LiveData<ResponseStartService?>
        get() = _startServiceStatus

    private val _maidBookingsStatus = MutableLiveData<ResponseMaidBookingDetailsList?>()
    val maidBookingsStatus: LiveData<ResponseMaidBookingDetailsList?>
        get() = _maidBookingsStatus

    private val _maidStatusChange = MutableLiveData<ResponseMaidStatusChange?>()
    val maidStatusChange: LiveData<ResponseMaidStatusChange?>
        get() = _maidStatusChange

    fun getCustomerDetails(context: Context, booking_id: String): LiveData<List<BookingsTable>>? {
        customerDetailsFromDb = BookingsRepository.getCustomerDetails(context, booking_id)
        return customerDetailsFromDb
    }

    fun getMailListFromDB(context: Context, booking_id: String): LiveData<List<MaidTable>>? {
        maidlistFromDb = BookingsRepository.getMailListFromDB(context, booking_id)
        return maidlistFromDb
    }

    fun reset() {
        _TransferlistStatus.value = null
        _maidBookingsStatus.value = null
        _maidStatusChange.value = null
        _startServiceStatus.value = null
        _transferStatus.value = null
        _maidlistStatus.value = null

    }

    suspend fun getMaiddetailslist(
            userid: String,
            usertype: String,
            date: String,
            token: String?,
            bookingId: String
    ) {

        try {
            val response = ApiClient.getRetrofit()
                    .getMaidDetailslist(userid, usertype, date, token!!, bookingId)

            if (response.status.equals("success")) {
                _maidlistStatus.value = response
            } else {
                _maidlistStatus.value = response
            }


        } catch (e: Exception) {
            _maidlistStatus.value = ResponseMaidDetailsList(message = "Something went wrong", status = "error")

        }

    }

    suspend fun getTransferlist(
            userid: String,
            usertype: String,
            date: String,
            token: String?,
            bookingId: String
    ) {
        try {
            val response = ApiClient.getRetrofit().getTransferlist(userid, usertype, date, token!!, bookingId)

            if (response.status.equals("success")) {

                _TransferlistStatus.value = response
            } else {
                _TransferlistStatus.value = response
            }


        } catch (e: Exception) {
            Log.e("Exception", "getTransferlist: ")
            _TransferlistStatus.value =  ResponseTransferlist(message = "Something went wrong", status = "error")

        }


    }

    suspend fun getBookingsTransfer(
            context: Context,
            userid: String,
            usertype: String,
            date: String,
            token: String?,
            bookingId: String,
            selected_zone: String,
            transfer_type: String
    ) {
        try {
            val response = ApiClient.getRetrofit().getBookingsTransfer(
                    userid,
                    usertype,
                    date,
                    token!!,
                    bookingId,
                    selected_zone,
                    transfer_type
            )

            if (response.status.equals("success")) {

                when (response.transferAll) {
                    "0" -> DeleteTransferbooking(context, bookingId)
                    "1" -> {
                        if (response.transferDetails != null && response.transferDetails.isNotEmpty()) {
                            Log.d("Delet List", "getBookingsTransfer: " + response.transferDetails)

                            for (bookingItem in response.transferDetails) {
                                DeleteTransferbooking(context, bookingItem.bookingId.toString())
                            }
                        }
                    }

                }


                if (response.transferDetails != null && response.transferDetails.isNotEmpty()) {

                    for (bookingItem in response.transferDetails) {
                        transferupdates(context, bookingItem.timeing, bookingItem.noOfMaids)
                    }
                }


                _transferStatus.value = response

            } else {
                _transferStatus.value = response
            }


        } catch (e: Exception) {
            _transferStatus.value =  ResponseBookingTransfer(message = "Something went wrong", status = "error")
        }

    }

    suspend fun transferupdates(context: Context, timeing: String?, noOfMaids: Int?) {
        BookingsRepository.transferupdates(context, timeing, noOfMaids)

    }

    suspend fun getStartService(
            requireContext: Context,
            userId: Int,
            userType: String?,
            currentDate: String,
            token: String?,
            bookingId: String,
            status: String,
            starttime: String
    ) {
        try {
            val response = ApiClient.getRetrofit().getStartService(
                    userId.toString(),
                    userType.toString(),
                    currentDate,
                    token.toString(),
                    bookingId,
                    status, "", "", "", "", "","")
            if (response.status.equals("success")) {

                val sdf = SimpleDateFormat("HH:mm aaa")  //HH:mm aaa
                val starttime = sdf.format(Date())


                _startServiceStatus.value = response
                updateStartServiceStatus(
                        requireContext, response.bookingId.toString(),
                        response.serviceStatus.toString(),response.starttime
                )
// -------------------------------------- Timer ----------------------
              /*  insertCounterDatasToDB(requireContext, bookingId, "1")
                Log.d("insertTimeToDB", "onClick: " + bookingId + ", " + starttime)*/
// --------------------------------------  ----------------------

            } else {
                _startServiceStatus.value = response
            }


        } catch (e: Exception) {
            _startServiceStatus.value =  ResponseStartService(message = "Something went wrong", status = "error")
        }
    }



// -------------------------------------- Timer ----------------------

   /* private suspend fun insertCounterDatasToDB(context: Context, bookingId: String, service_status: String) {
        val nowtime = Calendar.getInstance()
        val hour = nowtime[Calendar.HOUR_OF_DAY]
        val minute = nowtime[Calendar.MINUTE]
        val second = nowtime[Calendar.SECOND]

        BookingsRepository.insertCounterTimeData(context, bookingId, service_status, hour, minute, second)
    }*/
// --------------------------------------  ----------------------


    private suspend fun DeleteTransferbooking(context: Context, bookingId: String) {
        BookingsRepository.DeleteTransferbooking(context, bookingId)
    }

    private suspend fun deleteTransferedall(context: Context) {
        BookingsRepository.deleteTransferedallBooking(context, "0")
    }

    private suspend fun updateStartServiceStatus(
            context: Context,
            bookingId: String,
            serviceStatus: String,
            starttime: String?
    ) {
        BookingsRepository.updatestartbookingStatus(context, bookingId, serviceStatus,starttime)
    }

    suspend fun getMaidbookingsDetailslist(requireContext: Context, userId: Int, userType: String?, currentDate: String, token: String?, maidId: String?, bookingId: String) {
        try {
            val response = ApiClient.getRetrofit().getMaidbookingsDetailslist(
                    userId.toString(),
                    userType.toString(),
                    currentDate,
                    token.toString(),
                    maidId.toString(),
                    bookingId

            )
            if (response.status.equals("success")) {
                _maidBookingsStatus.value = response


            } else {
                _maidBookingsStatus.value = response
            }

        } catch (e: Exception) {
            _maidBookingsStatus.value =  ResponseMaidBookingDetailsList(message = "Something went wrong", status = "error")
        }
    }

    suspend fun getMaidStatusChange(requireContext: Context, userId: Int, userType: String?, currentDate: String, token: String?, status: Int, maidId: String?) {
        try {
            val response = ApiClient.getRetrofit().getMaidStatusChange(
                    userId.toString(),
                    userType.toString(),
                    currentDate,
                    token.toString(),
                    status.toString(), maidId.toString()

            )
            if (response.status.equals("success")) {
                _maidStatusChange.value = response

                updateMaidStatusChange(
                        requireContext, response.maidId.toString(),
                        response.maidDetails?.maidAttandence.toString()
                )

            } else {
                _maidStatusChange.value = response
            }

        } catch (e: Exception) {
            _maidStatusChange.value = ResponseMaidStatusChange(message = "Something went wrong", status = "error")
        }


    }

    private suspend fun updateMaidStatusChange(
            context: Context,
            maidId: String,
            maidAttandence: String
    ) {
        BookingsRepository.updateMaidStatusChange(context, maidId, maidAttandence)
    }



    // -------------------------------------- Timer ----------------------

    /* suspend fun getCounterTimeing(context: Context, bookingId: String): List<TimeCountdownTable>? {
         countdatalist = BookingsRepository.getCounterTimeing(context, bookingId)
         return countdatalist
     }*/

    // -------------------------------------- Timer ----------------------


    suspend fun NewMailListFromDB(requireContext: Context, bookingId: String): List<MaidTable>? {
        newmaidlistFromDb = BookingsRepository.NewMailListFromDB(requireContext, bookingId)
        return newmaidlistFromDb
    }

   suspend fun NewCustomerDetailsFromDB(requireContext: Context, bookingId: String): List<BookingsTable>? {
       newCustomerDetailsFromDb = BookingsRepository.NewCustomerDetailsFromDB(requireContext, bookingId)
      return newCustomerDetailsFromDb
    }


}