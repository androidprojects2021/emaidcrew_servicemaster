package com.azinova.emaid_crew.ui.fragment.ui.booking_details

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.azinova.emaid_crew.database.repository.BookingsRepository
import com.azinova.emaid_crew.model.response.cancel.ResponseCancelService
import com.azinova.emaid_crew.model.response.finish.ResponseFinishService
import com.azinova.emaid_crew.network.ApiClient
import java.text.SimpleDateFormat
import java.util.*

class PaymentDetailsViewModel : ViewModel() {

    private val _finishServiceStatus = MutableLiveData<ResponseFinishService>()
    val finishServiceStatus: LiveData<ResponseFinishService>
        get() = _finishServiceStatus

    private val _cancelServiceStatus = MutableLiveData<ResponseCancelService>()
    val cancelServiceStatus: LiveData<ResponseCancelService>
        get() = _cancelServiceStatus

    suspend fun getFinishService(
        requireContext: Context,
        userId: Int,
        userType: String?,
        currentDate: String,
        token: String?,
        bookingId: Int?,
        status: String,
        paytype: String,
        paid_value: String,
        receiptnumber: String,
        paymentmode: Int,
        outstandingBalance: String?,
        notes: String
    ) {
        try {
            val response = ApiClient.getRetrofit().getFinishService(
                    userId.toString(),
                    userType.toString(),
                    currentDate,
                    token.toString(),
                    bookingId.toString(),
                    status, paytype,paid_value,receiptnumber,paymentmode.toString(),outstandingBalance.toString(),notes)

            Log.d("TAG", "getFinishService: "+response.copy())
            if (response.status.equals("success")) {

                val sdf = SimpleDateFormat("HH:mm aaa")  //HH:mm aaa
                val stoptime = sdf.format(Date())

                updateFinishServiceStatus(
                        requireContext, response.bookingId.toString(),
                        response.serviceStatus.toString(),
                    response.amount,response.outstandingAmount.toString(),response.method,response.payment!!.toInt(),response.balance,response.endtime
                )

                // -------------------------------------- Timer ----------------------

/*
                deleteCounterTimeData(requireContext,bookingId.toString())
*/

                // -------------------------------------- Timer ----------------------

                _finishServiceStatus.value = response

            } else {
                _finishServiceStatus.value = response
            }
        } catch (e: Exception) {
            _finishServiceStatus.value = ResponseFinishService(message = "Something went wrong", status = "error")
        }
    }

    suspend fun updateFinishServiceStatus(context: Context, bookingId: String, serviceStatus: String, paid_amount: String?, outstandingAmount: String?,
                                          mop: String?, payment_status: Int, balance: Double?, endtime: String?) {
        var MOP = ""
        when(mop){
            "0" -> MOP = "Cash"
            "1" -> MOP = "Card"
            "2" -> MOP = "Cheque"
            else -> MOP = ""
        }
        BookingsRepository.updatefinishbookingStatus(context, bookingId, serviceStatus,paid_amount,outstandingAmount,MOP,payment_status,balance,endtime)

    }

    suspend fun getCancelService(
        requireContext: Context,
        userId: Int,
        userType: String?,
        currentDate: String,
        token: String?,
        bookingId: Int?,
        status: String,
        notes: String
    ) {
        try {
            val response = ApiClient.getRetrofit().getCancelService(
                    userId.toString(),
                    userType.toString(),
                    currentDate,
                    token.toString(),
                    bookingId.toString(),
                    status, "0","", "", "","",notes)
            if (response.status.equals("success")) {
                _cancelServiceStatus.value = response
                updateCancelServiceStatus(
                        requireContext, response.bookingId.toString(),
                        response.serviceStatus.toString())

                // -------------------------------------- Timer ----------------------

/*
                deleteCounterTimeData(requireContext,bookingId.toString())
*/
                // -------------------------------------- Timer ----------------------

            } else {
                _cancelServiceStatus.value = response
            }
        } catch (e: Exception) {
            _cancelServiceStatus.value = ResponseCancelService(message = "Something went wrong", status = "error")
        }
    }

    suspend fun updateCancelServiceStatus(context: Context, bookingId: String, serviceStatus: String) {
        BookingsRepository.updatecancelbookingStatus(context, bookingId, serviceStatus)
    }


    // -------------------------------------- Timer ----------------------

    /* private suspend fun deleteCounterTimeData(context: Context, bookingId: String) {
         BookingsRepository.deleteCounterTimeData(context, bookingId)
     }*/

    // --------------------------------------  ----------------------

}