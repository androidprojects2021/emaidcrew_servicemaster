package com.azinova.emaid_crew.ui.fragment.ui.notification

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.azinova.emaid_crew.R
import com.azinova.emaid_crew.model.response.notification.NotificationListItem

class NotificationDetailsAdapter(val context: Context, val notificationList: List<NotificationListItem?>?) : RecyclerView.Adapter<NotificationDetailsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
    ): NotificationDetailsAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.card_notificationview, parent, false))

    }

    override fun onBindViewHolder(holder: NotificationDetailsAdapter.ViewHolder, position: Int) {
        when(notificationList?.get(position)?.type){
            "1" -> {
                holder.img_notificationicon.setImageResource(R.drawable.ic_bookingadded_icon)
                holder.txt_notification_title.text = "New Booking"
            }
            "2" -> {
                holder.img_notificationicon.setImageResource(R.drawable.ic_bookingcancel_icon)
                holder.txt_notification_title.text = "Bookings Cancelled"
            }
            "3" -> {
                holder.img_notificationicon.setImageResource(R.drawable.ic_bookingschange_icon)
                holder.txt_notification_title.text = "Bookings Change"
            }
            "4" -> {
                holder.img_notificationicon.setImageResource(R.drawable.ic_general_icon)
                holder.txt_notification_title.text = "General"
            }
            "5" -> {
                holder.img_notificationicon.setImageResource(R.drawable.ic_tranfer_maid_icon)
                holder.txt_notification_title.text = "Transfer Maid"
            }
            "6" -> {
                holder.img_notificationicon.setImageResource(R.drawable.ic_alert_icon)
                holder.txt_notification_title.text = "Alert"
            }
        }

        val string: List<String> = notificationList?.get(position)?.message!!.split(":")
        holder.txt_notification.text = String.format("Maid     : %s", string[1].replace(", Customer", ""))

        holder.txt_notificationcustomer.setText(String.format("Customer : %s", string[2].replace(", Shift", "")))

        holder.txt_notificationshift.setText(String.format("Time : %s", string[3] + string[4] + " - " + string[5]))


        val s: String = notificationList?.get(position)?.message!!
        val lengthS = s.length

        holder.txt_notificationshift.setText(String.format("Time : %s", s.substring(s.indexOf("Shift : ") + 8, lengthS)))

        holder.txt_timeing.text = notificationList?.get(position)?.time

        if ((position+1).equals(notificationList?.size)) // array.size
        { holder.view.visibility = View.GONE}
    }

    override fun getItemCount(): Int {
        return notificationList?.size!!
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txt_notification = itemView.findViewById<TextView>(R.id.txt_notification)
        var txt_notification_title = itemView.findViewById<TextView>(R.id.txt_notification_title)
        var txt_notificationcustomer = itemView.findViewById<TextView>(R.id.txt_notificationcustomer)
        var txt_notificationshift = itemView.findViewById<TextView>(R.id.txt_notificationshift)
        var txt_timeing = itemView.findViewById<TextView>(R.id.txt_timeing)
        var view = itemView.findViewById<View>(R.id.view)
        var img_notificationicon = itemView.findViewById<ImageView>(R.id.img_notificationicon)

    }
}