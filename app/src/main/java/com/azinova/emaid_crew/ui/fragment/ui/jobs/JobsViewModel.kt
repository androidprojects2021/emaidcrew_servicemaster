package com.azinova.emaid_crew.ui.fragment.ui.jobs

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.azinova.emaid_crew.database.repository.BookingsRepository
import com.azinova.emaid_crew.database.tables.BookingsTable
import com.azinova.emaid_crew.database.tables.MaidTable
import com.azinova.emaid_crew.model.response.bookings.ResponseBookings
import com.azinova.emaid_crew.model.response.bookings.ScheduleItem
import com.azinova.emaid_crew.model.response.schedules.MaidDetails
import com.azinova.emaid_crew.model.response.schedules.SceduleTimeingResponse
import com.azinova.emaid_crew.model.response.schedules.ScheduleResponse
import com.azinova.emaid_crew.network.ApiClient
import com.azinova.emaid_crew.utils.SharedPref
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class JobsViewModel : ViewModel() {
    private val prefs = SharedPref

    var bookingsFromDb: LiveData<List<BookingsTable>>? = null

    //var bookingdatesFromDb: LiveData<List<String>>? = null
    var bookingdatesFromDb: List<String>? = null//old
//    var bookingdatesFromDb: List<String>? = ArrayList<String>()//new change

    //var bookingTimeingsFromDb: LiveData<List<String>>? = null
    var bookingTimeingsFromDb: List<String>? = null

    //var schedulesFromDb: LiveData<List<ScheduleResponse>>? = null   // BookingsTable
    var schedulesFromDb: List<BookingsTable>? = null

    //var maidListFromDb: LiveData<List<MaidTable>>? = null
    var maidListFromDb: List<MaidTable>? = null

    private val _bookingsDetailsRefresh = MutableLiveData<ResponseBookings>()  //ResponseBookings
    val bookingsDetailsRefresh: LiveData<ResponseBookings>
        get() = _bookingsDetailsRefresh


    fun getBookingDetails(context: Context, selecteddate: String): LiveData<List<BookingsTable>>? {
        bookingsFromDb = BookingsRepository.getBookingDetails(context, selecteddate)
        return bookingsFromDb
    }

    suspend fun getBookingTimeings(context: Context, selecteddate: String, FilterPosition: Int): ArrayList<SceduleTimeingResponse>? {

        bookingTimeingsFromDb = BookingsRepository.getBookingTimeings(context, selecteddate,FilterPosition)
        Log.d("Status Based ......Time", "callBookingDate: "+bookingTimeingsFromDb)


        val bookingList = ArrayList<SceduleTimeingResponse>()
        bookingTimeingsFromDb?.let {

            it.forEach {
                val scheduleListFromDB = getschedulesbytime(context, selecteddate, it,FilterPosition)
                val bookinglist = SceduleTimeingResponse()

                if (scheduleListFromDB != null && scheduleListFromDB.isNotEmpty()) {
                    val customerDetailArray = ArrayList<ScheduleResponse>()

                    for (bookingListItem in scheduleListFromDB) {
                        val maidDetailsList = ArrayList<MaidDetails>()
                        val scheduleResponsenew = ScheduleResponse()
                        val maidListFromDb = BookingsRepository.getMaidList(context, bookingListItem.booking_id.toString())
                        Log.d("TAG", "getBookingTimeings: "+maidListFromDb)

                        bookinglist.noOfMaids = bookingListItem.no_of_maids
                        bookinglist.scheduleDate = bookingListItem.schedule_date
                        bookinglist.scheduleTimeing = bookingListItem.timeing

                        scheduleResponsenew.schedule_date = bookingListItem.schedule_date
                        scheduleResponsenew.customer_id = bookingListItem.customer_id
                        scheduleResponsenew.booking_id = bookingListItem.booking_id
                        scheduleResponsenew.customer_name = bookingListItem.customer_name
                        scheduleResponsenew.service_status = bookingListItem.service_status
                        scheduleResponsenew.shift_start = bookingListItem.shift_start
                        scheduleResponsenew.shift_end = bookingListItem.shift_end
                        scheduleResponsenew.area = bookingListItem.area
                        scheduleResponsenew.cleaning_material = bookingListItem.cleaning_material
                        scheduleResponsenew.customerAddress = bookingListItem.customerAddress
                        scheduleResponsenew.customerMobile = bookingListItem.customerMobile
                        scheduleResponsenew.customerType = bookingListItem.customerType
                        scheduleResponsenew.customerCode = bookingListItem.customerCode
                        scheduleResponsenew.keyStatus = bookingListItem.keyStatus
                        scheduleResponsenew.bookingNote = bookingListItem.bookingNote
                        scheduleResponsenew.customernotes = bookingListItem.customernotes
                        scheduleResponsenew.serviceFee = bookingListItem.serviceFee
                        scheduleResponsenew.zone = bookingListItem.zone
                        scheduleResponsenew.mop = bookingListItem.mop
                        scheduleResponsenew.extraService = bookingListItem.extraService
                        scheduleResponsenew.tools = bookingListItem.tools
                        scheduleResponsenew.total = bookingListItem.total
                        scheduleResponsenew.balance = bookingListItem.balance
                        scheduleResponsenew.outstandingBalance = bookingListItem.outstandingBalance
                        scheduleResponsenew.servicetype = bookingListItem.servicetype
                        scheduleResponsenew.paymentStatus = bookingListItem.paymentStatus
                        scheduleResponsenew.paidAmount = bookingListItem.paidAmount
                        scheduleResponsenew.customerLatitude = bookingListItem.customerLatitude
                        scheduleResponsenew.customerLongitude = bookingListItem.customerLongitude
                        scheduleResponsenew.starttime = bookingListItem.starttime
                        scheduleResponsenew.endtime = bookingListItem.endtime

                        maidListFromDb?.let {
                            for (maidList in maidListFromDb) {
                                val maidDetails = MaidDetails()
                                maidDetails.customer_id = maidList.customer_id
                                maidDetails.booking_id = maidList.booking_id
                                maidDetails.maid_id = maidList.maid_id
                                maidDetails.maid_name = maidList.maid_name
                                maidDetails.maid_attandence = maidList.maid_attandence
                                maidDetails.service_status = maidList.service_status
                                maidDetails.maid_image = maidList.maid_image
                                maidDetailsList.add(maidDetails)
                                scheduleResponsenew.maidlist = maidDetailsList

                            }
                        }
                        customerDetailArray.add(scheduleResponsenew)
                        bookinglist.schedulebookings = customerDetailArray

                    }


                }
                bookingList.add(bookinglist)
            }
        }
        return bookingList

    }

    suspend fun getBookingDates(context: Context): List<String>? {
        bookingdatesFromDb = BookingsRepository.getBookingDates(context)
        return bookingdatesFromDb
    }

    suspend fun getschedulesbytime(
        context: Context,
        scheduleDate: String?,
        timeing: String?,
        FilterPosition: Int
    ): List<BookingsTable>? {
        schedulesFromDb = BookingsRepository.getscheduleDetailsbytime(context, scheduleDate, timeing,FilterPosition)
        return schedulesFromDb
    }



    suspend fun fetDatFromServer(context: Context, currentDate: String) {
        @SuppressLint("SimpleDateFormat")
        val sdf = SimpleDateFormat("dd-MM-yyyy")
        val dates = sdf.format(Date())

        try {
            val response = ApiClient.getRetrofit().getBookings(prefs.user_id.toString(), prefs.user_type.toString(), currentDate,prefs.token.toString())

            Log.d("Refresh", "getBookings: " + response)

            if (response.status.equals("success")) {
                insertToDb(context, response.schedule)
//                for (items in response.schedule!!){
//                    if (items != null) {
//                        if (items.scheduleDate?.contains(dates)!!){
//                            Log.e("Test", "CurentDate " )
//                            insertToDb(context, response.schedule)
//                        }else{
//                            Log.e("Test", "CurentDateAdded " )
//                            addCurrentDate(response.schedule,dates,context)
//                        }
//                    }
//                }
                _bookingsDetailsRefresh.value = response
            } else {
                _bookingsDetailsRefresh.value = response
            }
        } catch (e: Exception) {
            Log.e("throw", "fetDatFromServer: "+e.message)
            _bookingsDetailsRefresh.value =  ResponseBookings(message = "Something went wrong", status = "error")

        }

    }

//    private suspend fun addCurrentDate(schedule: List<ScheduleItem?>, currentDate: String, context: Context) {
//        val getBookings = schedule as ArrayList<ScheduleItem>
//        val dates = ScheduleItem(Schedule(null,null,null,null,null,null,null,
//                null,null,null,null,null,null,null,null,null,null,
//                null,null,null,null,null,null,null,null,null,
//                null,null,null,null,null), null,currentDate,null,null)
//        getBookings.add(dates)
//        insertToDb(context,getBookings)
//    }

    private suspend fun insertToDb(context: Context, schedule: List<ScheduleItem?>?) {
        BookingsRepository.insertData(context, schedule)
    }

    fun insertDate(currentdate: String) {

        BookingsRepository.insertDate(currentdate)

    }


}