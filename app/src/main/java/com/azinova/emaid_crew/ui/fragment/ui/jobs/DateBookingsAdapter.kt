package com.azinova.emaid_crew.ui.fragment.ui.jobs

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.azinova.emaid_crew.R

class DateBookingsAdapter(val context: Context, val bookingdatelist: List<String>, val itemSelected: (String) -> Unit) : RecyclerView.Adapter<DateBookingsAdapter.ViewHolder>(){
    private var selectedPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DateBookingsAdapter.ViewHolder {
        return ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                        R.layout.card_datewise,
                        parent,
                        false
                )
        )
    }

    override fun onBindViewHolder(holder: DateBookingsAdapter.ViewHolder, position: Int) {
        Log.d("", "onBindViewHolder: " + bookingdatelist.size + "  ," + bookingdatelist.get(position))
//        Log.e("Test", "Date: ${getDateTime()}")
        holder.txt_date.text = bookingdatelist.get(position)
//        if (bookingdatelist.equals(getDateTime())){
//
//        }else{
//            holder.txt_date.text = getDateTime()
//            holder.txt_date.text = bookingdatelist.get(position)
//        }




        if (position == selectedPosition) {
            itemSelected.invoke(bookingdatelist.get(position))
            holder.txt_date.setTextColor(Color.parseColor("#5f467f"))
            holder.indicater.visibility = View.VISIBLE
        } else {
            holder.txt_date.setTextColor(Color.parseColor("#818194"))
            holder.indicater.visibility = View.GONE
        }

        holder.Ll_datebooking.setOnClickListener {
            onStateChangedListener(position, holder) }

    }

    private fun onStateChangedListener(position: Int, holder: ViewHolder) {
        selectedPosition = position
        holder.txt_date.setTextColor(Color.parseColor("#5f467f"))
        holder.indicater.visibility = View.VISIBLE
        notifyDataSetChanged()

    }

    override fun getItemCount(): Int {
        return bookingdatelist.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txt_date = itemView.findViewById<TextView>(R.id.txt_date)
        var indicater = itemView.findViewById<View>(R.id.indicater)
        var Ll_datebooking = itemView.findViewById<LinearLayout>(R.id.Ll_datebooking)

    }



  

}