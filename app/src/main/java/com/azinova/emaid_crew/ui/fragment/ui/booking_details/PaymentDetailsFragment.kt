package com.azinova.emaid_crew.ui.fragment.ui.booking_details

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.azinova.emaid_crew.R
import com.azinova.emaid_crew.commons.Loader
import com.azinova.emaid_crew.database.EmaidDatabase
import com.azinova.emaid_crew.ui.activity.detail.BookingDetailsActivity
import com.azinova.emaid_crew.ui.activity.login.LoginActivity
import com.azinova.emaid_crew.utils.SharedPref
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.payment_details.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class PaymentDetailsFragment : Fragment() {
    val prefs = SharedPref
    var database: EmaidDatabase? = null

    private lateinit var paymentDetailsViewModel: PaymentDetailsViewModel

    private lateinit var parent: BookingDetailsActivity

    var paymentmode: Int = -1
    var paytype: Int = 0

    //var mlist: ArrayList<BookingsTable>? = ArrayList()

    var booking_id: String = ""
    var customer_id: String = ""
    var serviceFee: String = ""
    var service_status: String = ""
    var total: String = ""
    var outstandingBalance: String = ""
    var paymentStatus: String = ""
    var mop: String = ""
    var paidAmount: String = ""


    private var finishButtonClicked = false
    private var cancelButtonClicked = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.payment_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        /* mlist = arguments?.getSerializable("Booking_Details") as ArrayList<BookingsTable>
         Log.d("TAG", "onViewCreated: " + mlist)*/

        paymentDetailsViewModel = ViewModelProvider(this).get(PaymentDetailsViewModel::class.java)

        parent = activity as BookingDetailsActivity

        booking_id = requireArguments().getString("Booking_id").toString()
        customer_id = requireArguments().getString("Customer_id").toString()
        serviceFee = requireArguments().getString("Service_amount").toString()
        service_status = requireArguments().getString("Service_status").toString()
        total = requireArguments().getString("Total").toString()
        outstandingBalance = requireArguments().getString("Outstanding_Balance").toString()
        paymentStatus = requireArguments().getString("Payment_status").toString()
        mop = requireArguments().getString("MOP").toString()
        paidAmount = requireArguments().getString("Paid_amount").toString()


        initview(view)
        observeList()
        onClick()


    }

    private fun observeList() {
        paymentDetailsViewModel.finishServiceStatus.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer { finishServiceStatus ->

                if (finishServiceStatus.status.equals("success")) {
                    Loader.hideLoader()
                    Toast.makeText(
                        requireContext(),
                        finishServiceStatus.message,
                        Toast.LENGTH_SHORT
                    )
                        .show()
                    requireActivity().finish()
                }

                else if (finishServiceStatus.status.equals("token_error")) {
                    Loader.hideLoader()
                    Toast.makeText(requireContext(), finishServiceStatus.message, Toast.LENGTH_SHORT)
                            .show()


                    clearLogin()
                   // parent.stopServiceTracker()
                    database?.clearAllTables()

                    val intent = Intent(requireContext(), LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    activity?.finish()

                }
                else {
                    Loader.hideLoader()
                    Toast.makeText(
                        requireContext(),
                        finishServiceStatus.message,
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
            }
        )

        paymentDetailsViewModel.cancelServiceStatus.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer { cancelServiceStatus ->
                if (cancelServiceStatus.status.equals("success")) {
                    Loader.hideLoader()
                    Toast.makeText(
                        requireContext(),
                        cancelServiceStatus.message,
                        Toast.LENGTH_SHORT
                    )
                        .show()
                    requireActivity().finish()
                }
                else if (cancelServiceStatus.status.equals("token_error")) {
                    Loader.hideLoader()
                    Toast.makeText(requireContext(), cancelServiceStatus.message, Toast.LENGTH_SHORT)
                            .show()


                    clearLogin()
                   // parent.stopServiceTracker()
                    database?.clearAllTables()

                    val intent = Intent(requireContext(), LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    activity?.finish()

                }

                else {
                    Loader.hideLoader()
                    Toast.makeText(
                        requireContext(),
                        cancelServiceStatus.message,
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }

            })

    }


    private fun initview(view: View) {
        serviceamount_value.text = "AED " + serviceFee
/*
        cleaningmaterial_value.text = "AED "
*/
        total_value.text = "AED " + total


        when (paymentStatus) {
            "0" -> txt_paymenstatus.text = "Not Paid"
            "1" -> txt_paymenstatus.text = "Paid"
        }

//-----

        when (service_status) {
            "0" -> {

                finishButtonClicked = true
                btn_finishbooking.setBackgroundResource(R.drawable.bg_button_off)

                cancelButtonClicked = true
                btn_cancelservice.setBackgroundResource(R.drawable.bg_button_off)

            }
            "1" -> {

                finishButtonClicked = false
                btn_finishbooking.setBackgroundResource(R.drawable.bg_green)

                cancelButtonClicked = false
                btn_cancelservice.setBackgroundResource(R.drawable.bg_red)

            }

        }





        txt_outstanding_balance.text = "AED " + outstandingBalance
        txt_paid_value.text = Editable.Factory.getInstance()
            .newEditable("")
        txt_receiptnumbervalue.text = Editable.Factory.getInstance()
            .newEditable("")

        when (mop) {
            "Cash" -> {
                paymentmode = 0
                l_cash.setBackgroundResource(R.drawable.bg_purple_left_curve)
                l_card.setBackgroundResource(R.drawable.bg_grey_square)
                l_cheque.setBackgroundResource(R.drawable.bg_greysqu_rightcurve)

                img_cash.setImageResource(R.drawable.ic_cash_icon)
                imgcard.setImageResource(R.drawable.ic_card_icon)
                imgcheque.setImageResource(R.drawable.ic_cheque)

                cash_click.setTextColor(getResources().getColor(R.color.white))
                card_click.setTextColor(getResources().getColor(R.color.color_grey_common))
                cheque_click.setTextColor(getResources().getColor(R.color.color_grey_common))
            }
            "Card" -> {
                paymentmode = 1

                l_cash.setBackgroundResource(R.drawable.bg_greysqu_leftcurve)
                l_card.setBackgroundResource(R.drawable.bg_purple_square)
                l_cheque.setBackgroundResource(R.drawable.bg_greysqu_rightcurve)

                img_cash.setImageResource(R.drawable.ic_cash_grey)
                imgcard.setImageResource(R.drawable.ic_card_white)
                imgcheque.setImageResource(R.drawable.ic_cheque)

                cash_click.setTextColor(getResources().getColor(R.color.color_grey_common))
                card_click.setTextColor(getResources().getColor(R.color.white))
                cheque_click.setTextColor(getResources().getColor(R.color.color_grey_common))
            }
            "Cheque" -> {
                paymentmode = 2

                l_cash.setBackgroundResource(R.drawable.bg_greysqu_leftcurve)
                l_card.setBackgroundResource(R.drawable.bg_grey_square)
                l_cheque.setBackgroundResource(R.drawable.bg_purple_right_curve)

                img_cash.setImageResource(R.drawable.ic_cash_grey)
                imgcard.setImageResource(R.drawable.ic_card_icon)
                imgcheque.setImageResource(R.drawable.ic_cheque_white)

                cash_click.setTextColor(getResources().getColor(R.color.color_grey_common))
                card_click.setTextColor(getResources().getColor(R.color.color_grey_common))
                cheque_click.setTextColor(getResources().getColor(R.color.white))
            }
            else -> {
                paymentmode = -1

                l_cash.setBackgroundResource(R.drawable.bg_greysqu_leftcurve)
                l_card.setBackgroundResource(R.drawable.bg_grey_square)
                l_cheque.setBackgroundResource(R.drawable.bg_greysqu_rightcurve)

                img_cash.setImageResource(R.drawable.ic_cash_grey)
                imgcard.setImageResource(R.drawable.ic_card_icon)
                imgcheque.setImageResource(R.drawable.ic_cheque)

                cash_click.setTextColor(getResources().getColor(R.color.color_grey_common))
                card_click.setTextColor(getResources().getColor(R.color.color_grey_common))
                cheque_click.setTextColor(getResources().getColor(R.color.color_grey_common))
            }
        }




    }

    private fun onClick() {
        l_cash.setOnClickListener {
            paymentmode = 0
            l_cash.setBackgroundResource(R.drawable.bg_purple_left_curve)
            l_card.setBackgroundResource(R.drawable.bg_grey_square)
            l_cheque.setBackgroundResource(R.drawable.bg_greysqu_rightcurve)

            img_cash.setImageResource(R.drawable.ic_cash_icon)
            imgcard.setImageResource(R.drawable.ic_card_icon)
            imgcheque.setImageResource(R.drawable.ic_cheque)

            cash_click.setTextColor(getResources().getColor(R.color.white))
            card_click.setTextColor(getResources().getColor(R.color.color_grey_common))
            cheque_click.setTextColor(getResources().getColor(R.color.color_grey_common))

        }
        l_card.setOnClickListener {
            paymentmode = 1

            l_cash.setBackgroundResource(R.drawable.bg_greysqu_leftcurve)
            l_card.setBackgroundResource(R.drawable.bg_purple_square)
            l_cheque.setBackgroundResource(R.drawable.bg_greysqu_rightcurve)

            img_cash.setImageResource(R.drawable.ic_cash_grey)
            imgcard.setImageResource(R.drawable.ic_card_white)
            imgcheque.setImageResource(R.drawable.ic_cheque)

            cash_click.setTextColor(getResources().getColor(R.color.color_grey_common))
            card_click.setTextColor(getResources().getColor(R.color.white))
            cheque_click.setTextColor(getResources().getColor(R.color.color_grey_common))

        }
        l_cheque.setOnClickListener {
            paymentmode = 2

            l_cash.setBackgroundResource(R.drawable.bg_greysqu_leftcurve)
            l_card.setBackgroundResource(R.drawable.bg_grey_square)
            l_cheque.setBackgroundResource(R.drawable.bg_purple_right_curve)

            img_cash.setImageResource(R.drawable.ic_cash_grey)
            imgcard.setImageResource(R.drawable.ic_card_icon)
            imgcheque.setImageResource(R.drawable.ic_cheque_white)

            cash_click.setTextColor(getResources().getColor(R.color.color_grey_common))
            card_click.setTextColor(getResources().getColor(R.color.color_grey_common))
            cheque_click.setTextColor(getResources().getColor(R.color.white))

        }

        btn_finishbooking.setOnClickListener {
            Log.d("btn_finishbooking", "onClick: "+finishButtonClicked)
            if (!finishButtonClicked) {


                if (requireContext().isConnectedToNetwork()) {

                    if (!TextUtils.isEmpty(txt_paid_value.text.toString())){
                        if (TextUtils.isEmpty(txt_receiptnumbervalue.text.toString())) {
                            Snackbar.make(
                                    outerpaymentdetails,
                                    "Please enter Receipt Number",
                                    Snackbar.LENGTH_SHORT
                            ).show()

                        } else if (paymentmode == -1) {
                            Snackbar.make(
                                    outerpaymentdetails,
                                    "Please select payment mode ",
                                    Snackbar.LENGTH_SHORT
                            ).show()

                        }else{
                            Log.d("btn_finishbooking 1", "onClick: ")

                            finishService("1")
                        }
                    }

                    else{
                        Log.d("btn_finishbooking  2", "onClick: ")

                        finishService("0")
                    }



                } else {
                    Snackbar.make(
                        outerpaymentdetails,
                        "Please connect to internet",
                        Snackbar.LENGTH_LONG
                    ).show()
                }
               /* if (TextUtils.isEmpty(txt_paid_value.text.toString())) {
                    Snackbar.make(
                        outerpaymentdetails,
                        "Please enter Paid Amount",
                        Snackbar.LENGTH_SHORT
                    ).show()
                } else if (TextUtils.isEmpty(txt_receiptnumbervalue.text.toString())) {
                    Snackbar.make(
                        outerpaymentdetails,
                        "Please enter Receipt Number",
                        Snackbar.LENGTH_SHORT
                    ).show()

                } else if (paymentmode == -1) {
                    Snackbar.make(
                        outerpaymentdetails,
                        "Please select payment mode ",
                        Snackbar.LENGTH_SHORT
                    ).show()

                } else { }*/

            }
        }
        btn_cancelservice.setOnClickListener {
            if (!cancelButtonClicked) {
            if (requireContext().isConnectedToNetwork()) {
                cancelService()
            } else {
                Snackbar.make(
                    outerpaymentdetails,
                    "Please connect to internet",
                    Snackbar.LENGTH_LONG
                ).show()
            }
        }
        }
    }

    private fun cancelService() {
        @SuppressLint("SimpleDateFormat")
        val sdf1 = SimpleDateFormat("yyyy/MM/dd")
        val currentDate = sdf1.format(Date())


        val alertDialog = AlertDialog.Builder(requireContext()).create()
        alertDialog.setTitle("Confirmation")
        alertDialog.setMessage("Do you want to continue ?")

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Confirm"
        ) { dialog, which ->
            Loader.showLoader(requireContext())
            GlobalScope.launch(Dispatchers.Main) {

                paymentDetailsViewModel.getCancelService(
                    requireContext(),
                    prefs.user_id,
                    prefs.user_type,
                    currentDate,
                    prefs.token,
                    booking_id.toInt(), "3",edit_notes.text.toString()
                )
            }
            dialog.dismiss()
        }

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel"
        ) { dialog, which -> dialog.dismiss() }
        alertDialog.show()

        val btnPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
        val btnNegative = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)

        val layoutParams = btnPositive.layoutParams as LinearLayout.LayoutParams
        layoutParams.weight = 10f
        btnPositive.layoutParams = layoutParams
        btnNegative.layoutParams = layoutParams


        /*val cancelbuilder = AlertDialog.Builder(requireContext())
        val cancelview: View =
            LayoutInflater.from(requireContext()).inflate(R.layout.dialog_cancel, null)
        cancelbuilder.setView(cancelview)
        val cancelalertDialog = cancelbuilder.create()

        val confirm = cancelview.findViewById<TextView>(R.id.confirm)
        val cancel = cancelview.findViewById<TextView>(R.id.cancel)

        confirm.setOnClickListener {
            Loader.showLoader(requireContext())
            GlobalScope.launch(Dispatchers.Main) {

                paymentDetailsViewModel.getCancelService(
                    requireContext(),
                    prefs.user_id,
                    prefs.user_type,
                    currentDate,
                    prefs.token,
                    booking_id.toInt(), "3"
                )
            }
            cancelalertDialog.dismiss()

        }
        cancel.setOnClickListener {
            cancelalertDialog.dismiss()
        }

        cancelalertDialog.show()*/


    }

    private fun finishService(paymenttype: String) {

        Log.d("btn_finishbooking", "onClick: "+"Confirmation alert")
        Log.d("payment", "onClick: "+paymenttype)



        /*when (paymentStatus) {
            "0" -> paytype = 1     //"Not Paid"
            "1" -> paytype = 0     //"Paid"
        }*/




        val alertDialog = AlertDialog.Builder(requireContext()).create()
        alertDialog.setTitle("Confirmation")
        alertDialog.setMessage("Do you want to continue ?")

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Confirm"
        ) { dialog, which ->

            finalFinish(paymenttype)

            dialog.dismiss()
        }

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel"
        ) { dialog, which -> dialog.dismiss() }
        alertDialog.show()

        val btnPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
        val btnNegative = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)

        val layoutParams = btnPositive.layoutParams as LinearLayout.LayoutParams
        layoutParams.weight = 10f
        btnPositive.layoutParams = layoutParams
        btnNegative.layoutParams = layoutParams


    }

    private fun finalFinish(paymenttype: String) {

        @SuppressLint("SimpleDateFormat")
        val sdf1 = SimpleDateFormat("yyyy/MM/dd")
        val currentDate = sdf1.format(Date())

        Loader.showLoader(requireContext())
            GlobalScope.launch(Dispatchers.Main) {
                Log.d("MOP", "finishService: " + paymentmode)

                paymentDetailsViewModel.getFinishService(
                        requireContext(),
                        prefs.user_id,
                        prefs.user_type,
                        currentDate,
                        prefs.token,
                        booking_id.toInt(),
                        "2",
                       paymenttype,
                        txt_paid_value.text.toString(),
                        txt_receiptnumbervalue.text.toString(),
                        paymentmode,
                        outstandingBalance,edit_notes.text.toString()
                )
            }
    }
    private fun clearLogin() {
        // prefs.clearSession(requireContext())
        prefs.user_details = ""
        prefs.user_id = 0
        prefs.user_name = ""
        prefs.user_type = ""
        prefs.token = ""
        prefs.image = ""
        prefs.phone = ""
        prefs.email = ""
        prefs.Latitude = ""
        prefs.Longitude = ""
    }


    override fun onPause() {
        super.onPause()
        try {
            Loader.hideLoader()
        } catch (e: Exception) {
        }
    }


    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }


}