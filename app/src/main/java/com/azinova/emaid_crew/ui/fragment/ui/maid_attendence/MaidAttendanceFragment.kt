package com.azinova.emaid_crew.ui.fragment.ui.maid_attendence

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.azinova.emaid_crew.R
import com.azinova.emaid_crew.commons.Loader
import com.azinova.emaid_crew.database.EmaidDatabase
import com.azinova.emaid_crew.model.response.maidattendance.MaidDetailsItem
import com.azinova.emaid_crew.ui.activity.home.MainActivity
import com.azinova.emaid_crew.ui.activity.login.LoginActivity
import com.azinova.emaid_crew.utils.SharedPref
import com.google.android.material.snackbar.Snackbar
import com.miguelcatalan.materialsearchview.MaterialSearchView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_maid_attendance.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*


class MaidAttendanceFragment : Fragment() {
    private lateinit var maidAttendanceViewModel: MaidAttendanceViewModel

    private val prefs = SharedPref
    var database: EmaidDatabase? = null

    private lateinit var parent: MainActivity

    lateinit var maidAttendenceAdapter: MaidAttendenceAdapter

    var currentdate: String = " "

    var maidstatuschange = 0

    var SearchList = ArrayList<MaidDetailsItem>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_maid_attendance, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        maidAttendanceViewModel = ViewModelProvider(this).get(MaidAttendanceViewModel::class.java)

        parent = activity as MainActivity

        initView()
        clickListen()
        observedIn()
    }

    private fun initView() {
        if (activity?.search_view?.isSearchOpen!!) {
            activity?.search_view?.clearFocus()
            activity?.search_view?.closeSearch()
        }
        val sdf = SimpleDateFormat("yyyy/MM/dd")
        currentdate = sdf.format(Date())

        Loader.showLoader(requireContext())
        if (requireContext().isConnectedToNetwork()) {
            getmaidAttendence(
                prefs.user_id.toString(),
                currentdate,
                prefs.user_type.toString(),
                prefs.token
            )
        } else {
            Loader.hideLoader()
            Snackbar.make(outermaidattendance, "Please connect to internet", Snackbar.LENGTH_LONG).show()
        }


    }

    private fun getmaidAttendence(
        user_id: String,
        currentdate: String,
        user_type: String,
        token: String?
    ) {
        maidAttendanceViewModel.getmaidAttendence(user_id, currentdate, user_type, token)

    }

    private fun observedIn() {

        maidAttendanceViewModel.maidAttendencelistStatus.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer { maidAttendencelistStatus ->
                Log.d("TAG", "observedIn: " + maidAttendencelistStatus)

                if (maidAttendencelistStatus.status.equals("success")) {
                    Loader.hideLoader()


                    if (maidAttendencelistStatus.maidDetails?.size != 0) {
                        rv_maidAttendence.visibility = View.VISIBLE
                        l_nomaidsList.visibility = View.GONE

                        maidAttendenceAdapter = MaidAttendenceAdapter(
                            requireContext(),
                            maidAttendencelistStatus.maidDetails
                        ) { maidDetailsItem: MaidDetailsItem?, maidstatus: Int, position: Int, list: List<MaidDetailsItem?>? ->
                            maidstatuschange(maidDetailsItem, maidstatus)
                            Log.d("STATUS", "observedIn: " + maidstatus)
                        }
                        val lmList = LinearLayoutManager(context)
                        lmList.orientation = LinearLayoutManager.VERTICAL
                        rv_maidAttendence.setLayoutManager(lmList)
                        rv_maidAttendence.setAdapter(maidAttendenceAdapter)


                        //Search
                        activity?.search_view?.setOnQueryTextListener(object :
                            MaterialSearchView.OnQueryTextListener {
                            override fun onQueryTextSubmit(query: String?): Boolean {
                                Log.d("MaidAttendance", "onQueryTextSubmit: " + query)
                                return false
                            }

                            override fun onQueryTextChange(newText: String?): Boolean {
                                Log.d("MaidAttendance", "onQueryTextChange: " + newText)

                                SearchList.clear()
                                if (newText.equals("")) {
                                    (maidAttendenceAdapter as MaidAttendenceAdapter).refreshData( maidAttendencelistStatus.maidDetails)
                                }else{

                                    if (maidAttendencelistStatus.maidDetails?.size!! > 0) {
                                        for (item in maidAttendencelistStatus.maidDetails) {
                                            if (item?.maidName.toString().contains(newText.toString(), ignoreCase = true)) {
                                                SearchList.add(item!!)
                                            }
                                        }
                                        Log.d("SEARCH SUCCESS", "onQueryTextChange: " + SearchList.size)
                                        (maidAttendenceAdapter as MaidAttendenceAdapter).refreshData(SearchList)
                                    }


                                }
                                return true
                            }

                        })


                    } else {
                        rv_maidAttendence.visibility = View.GONE
                        l_nomaidsList.visibility = View.VISIBLE
                    }





                }  else if (maidAttendencelistStatus.status.equals("token_error")) {
                    Loader.hideLoader()
                    Toast.makeText(requireContext(), maidAttendencelistStatus.message, Toast.LENGTH_SHORT)
                            .show()


                    clearLogin()
                    parent.stopServiceTracker()
                    database?.clearAllTables()

                    val intent = Intent(requireContext(), LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    activity?.finish()

                }



                else {
                    Loader.hideLoader()
                    Toast.makeText(
                        requireContext(),
                        maidAttendencelistStatus.message,
                        Toast.LENGTH_SHORT
                    )
                        .show()

                }


            })


        maidAttendanceViewModel.maidStatusChange.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer { maidStatusChange ->
                if (maidStatusChange.status.equals("success")) {
                    Loader.hideLoader()
                    initView()
                    Toast.makeText(requireContext(), maidStatusChange.message, Toast.LENGTH_SHORT)
                        .show()

                }
                else if (maidStatusChange.status.equals("token_error")) {
                    Loader.hideLoader()
                    Toast.makeText(requireContext(), maidStatusChange.message, Toast.LENGTH_SHORT)
                            .show()


                    clearLogin()
                    parent.stopServiceTracker()
                    database?.clearAllTables()

                    val intent = Intent(requireContext(), LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    activity?.finish()

                }
                else {
                    Loader.hideLoader()
                    Toast.makeText(requireContext(), maidStatusChange.message, Toast.LENGTH_SHORT)
                        .show()
                }
            })
    }

    private fun maidstatuschange(maidDetailsItem: MaidDetailsItem?, status: Int) {


        when (maidDetailsItem?.maidAttandence) {
            "0" -> maidstatuschange = 1
            "1" -> maidstatuschange = 2
            "2" -> maidstatuschange = 1
        }
        if (requireContext().isConnectedToNetwork()) {

            @SuppressLint("SimpleDateFormat")
            val sdf1 = SimpleDateFormat("yyyy/MM/dd")
            val currentDate = sdf1.format(Date())

            Loader.showLoader(requireContext())
            GlobalScope.launch(Dispatchers.Main) {
                maidAttendanceViewModel.getMaidStatusChange(
                    requireContext(),
                    prefs.user_id,
                    prefs.user_type,
                    currentDate,
                    prefs.token,
                    maidstatuschange,
                    maidDetailsItem?.maidId
                )
            }


        } else {
            Snackbar.make(outermaidattendance, "Please connect to internet", Snackbar.LENGTH_LONG).show()
        }


    }

    private fun clickListen() {

    }

    private fun clearLogin() {
        // prefs.clearSession(requireContext())
        prefs.user_details = ""
        prefs.user_id = 0
        prefs.user_name = ""
        prefs.user_type = ""
        prefs.token = ""
        prefs.image = ""
        prefs.phone = ""
        prefs.email = ""
        prefs.Latitude = ""
        prefs.Longitude = ""
    }

    override fun onPause() {
        super.onPause()
        try {
            Loader.hideLoader()
        } catch (e: Exception) {
        }
    }

    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }
}