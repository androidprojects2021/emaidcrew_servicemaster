package com.azinova.emaid_crew.ui.fragment.ui.more

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.azinova.emaid_crew.R
import com.azinova.emaid_crew.commons.Loader
import com.azinova.emaid_crew.commons.LogoutConfirmDialog
import com.azinova.emaid_crew.database.EmaidDatabase
import com.azinova.emaid_crew.ui.activity.home.MainActivity
import com.azinova.emaid_crew.ui.activity.login.LoginActivity
import com.azinova.emaid_crew.utils.SharedPref
import kotlinx.android.synthetic.main.fragment_more.*


class MoreFragment : Fragment(), LogoutConfirmDialog.LogoutClick {
    private val prefs = SharedPref
    var database: EmaidDatabase? = null

    private lateinit var parent: MainActivity

    private lateinit var moreFragmentViewModel: MoreFragmentViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_more, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        moreFragmentViewModel = ViewModelProvider(this).get(MoreFragmentViewModel::class.java)

        parent = activity as MainActivity


        initView()
        clickListen()
        observeLogOut()


    }

    private fun observeLogOut() {
        moreFragmentViewModel.logoutStatus.observe(viewLifecycleOwner, Observer {
            Loader.hideLoader()
            if (it.status.equals("success")){
                parent.stopServiceTracker()
                database?.clearAllTables()
                clearLogin()

                val intent = Intent(requireContext(), LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                activity?.finish()
            }else{
                Toast.makeText(requireContext(),it.message,Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun initView() {
        txt_username.text = prefs.user_name
        if (prefs.email.isNullOrEmpty()) {
            txt_useremail.visibility = View.GONE
        } else {
            txt_useremail.visibility = View.VISIBLE
            txt_useremail.text = prefs.email
        }

        if (prefs.image!!.isNotEmpty()) {

            val savedUri: Uri? = prefs.image?.toUri()
            img_profile.setImageURI(savedUri)


        } else {
            img_profile.setImageResource(R.drawable.ic_user_icon)
        }

        when (prefs.user_type) {
            "0" -> l_maidattendencesettings.visibility = View.GONE
            "1" -> l_maidattendencesettings.visibility = View.VISIBLE
        }

    }


    private fun clickListen() {


        img_editprofile.setOnClickListener {
            findNavController().navigate(
                R.id.action_more_to_editprofile,
            )
        }
        l_maidattendencesettings.setOnClickListener {
            findNavController().navigate(
                R.id.action_more_to_maidattendence,
            )
        }
        l_notificationsettings.setOnClickListener {

            findNavController().navigate(
                R.id.action_more_to_notification,
            )
        }
        l_ResetApplicationsettings.setOnClickListener {
            Toast.makeText(requireContext(), "In Progress", Toast.LENGTH_LONG).show()
        }
        l_logoutsettings.setOnClickListener {
            LogoutConfirmDialog.showDialog(requireContext(), this)

        }
    }

    override fun proceedLogout() {
//        clearLogin()
        Loader.showLoader(requireContext())
        moreFragmentViewModel.logOut(prefs.user_id.toString(),prefs.user_type.toString(),prefs.token)

        /*startActivity(Intent(requireContext(), LoginActivity::class.java))
        activity?.finish()*/
    }

    private fun clearLogin() {
        // prefs.clearSession(requireContext())
        prefs.user_details = ""
        prefs.user_id = 0
        prefs.user_name = ""
        prefs.user_type = ""
        prefs.token = ""
        prefs.image = ""
        prefs.phone = ""
        prefs.email = ""
        prefs.Latitude = ""
        prefs.Longitude = ""
    }

    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }
}