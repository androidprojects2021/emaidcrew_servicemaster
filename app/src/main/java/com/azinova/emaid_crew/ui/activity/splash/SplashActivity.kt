package com.azinova.emaid_crew.ui.activity.splash

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.azinova.emaid_crew.R
import com.azinova.emaid_crew.ui.activity.home.MainActivity
import com.azinova.emaid_crew.ui.activity.login.LoginActivity
import com.azinova.emaid_crew.utils.notification.BaseActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashActivity : BaseActivity() {


    private lateinit var splash_viewModel: SplashActivityViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        supportActionBar?.hide()

        splash_viewModel = ViewModelProvider(this).get(SplashActivityViewModel::class.java)
        observeLoggedIn()

    }

    private fun observeLoggedIn() {
        splash_viewModel.loginStatus.observe(this, Observer { loginStatus ->
            try {
                if (loginStatus) {

                    GlobalScope.launch {
                        delay(1000)
                        val intent = Intent(applicationContext, MainActivity::class.java)
                        intent.flags =
                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK


                        if (getIntent().extras != null) {

                            Log.e("data","generateNotification" +getIntent().getExtras()?.getString("body").toString())
                            if (getIntent().extras!!.getString("body") != null) {
                                intent.putExtra("message", getIntent().extras!!.getString("body"))
                            }
                        }
                        startActivity(intent)
                    }
                } else {

                    GlobalScope.launch {
                        delay(1000)
                        val intent = Intent(applicationContext, LoginActivity::class.java)
                        intent.flags =
                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        startActivity(intent)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }


}