package com.azinova.emaid_crew.ui.activity.home

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.azinova.emaid_crew.database.repository.BookingsRepository
import com.azinova.emaid_crew.model.response.bookings.ResponseBookings
import com.azinova.emaid_crew.model.response.bookings.ScheduleItem
import com.azinova.emaid_crew.network.ApiClient
import com.azinova.emaid_crew.utils.SharedPref

class MainActivityViewModel :ViewModel(){

    private val prefs = SharedPref

    private val _bookingsDetailsSyncStatus = MutableLiveData<ResponseBookings>()
    val bookingsDetailsSyncStatus: LiveData<ResponseBookings>
        get() = _bookingsDetailsSyncStatus

    suspend fun fetDatFromServer(context: Context, currentDate: String) {
        try {
            val response = ApiClient.getRetrofit().getBookings(
                prefs.user_id.toString(),
                prefs.user_type.toString(),
                currentDate,
                prefs.token.toString()
            )

            Log.d("Refresh", "getBookings: " + response)

            if (response.status.equals("success")) {
                insertToDb(context, response.schedule)
                _bookingsDetailsSyncStatus.value = response
            } else {
                _bookingsDetailsSyncStatus.value = response
            }
        } catch (e: Exception) {
            Log.e("throw", "fetDatFromServer: ")
            ResponseBookings(message = "Something went wrong", status = "error")
            _bookingsDetailsSyncStatus.value = ResponseBookings(message = "Something went wrong", status = "error")

        }

    }

    private suspend fun insertToDb(context: Context, schedule: List<ScheduleItem?>?) {
        BookingsRepository.insertData(context, schedule)
    }

}