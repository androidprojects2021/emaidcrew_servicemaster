package com.azinova.emaid_crew.ui.activity.splash

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.azinova.emaid_crew.model.response.login.Userdetails
import com.azinova.emaid_crew.utils.SharedPref
import com.google.gson.Gson

class SplashActivityViewModel (application: Application): AndroidViewModel(application) {

    private val prefs= SharedPref

    private val _loginStatus = MutableLiveData<Boolean>()
    val  loginStatus: LiveData<Boolean>
        get()=_loginStatus

    init {

        application.let { prefs.with(it) }
        _loginStatus.value=false
        isLoggedIn()

    }

    private fun isLoggedIn() {
        if(!prefs.user_details.equals("")){
            val userDetails = Gson().fromJson(prefs.user_details, Userdetails::class.java)
            if (userDetails!=null){
                _loginStatus.value = userDetails.id?.toInt()!=0
            }else {
                _loginStatus.value=false
                clearLogin()
            }
        }else {
            _loginStatus.value=false
            clearLogin()
        }
    }

    private fun clearLogin() {
        prefs.clearSession(getApplication())

    }




}