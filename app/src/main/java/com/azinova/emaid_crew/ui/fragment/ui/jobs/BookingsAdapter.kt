package com.azinova.emaid_crew.ui.fragment.ui.jobs

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.azinova.emaid_crew.R
import com.azinova.emaid_crew.model.response.schedules.ScheduleResponse

class BookingsAdapter(val context: Context, val schedulelist: List<ScheduleResponse?>?, val onItemClicked: (ScheduleResponse?, Int, List<ScheduleResponse?>?) -> Unit) : RecyclerView.Adapter<BookingsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookingsAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.card_bookings, parent, false))

    }

    override fun onBindViewHolder(holder: BookingsAdapter.ViewHolder, position: Int) {
        if (schedulelist?.get(position)?.service_status == 0) {
            holder.layout_schedule.setBackgroundResource(R.color.color_pending_light)
            holder.indicater.setBackgroundResource(R.color.color_pending_dark)
            holder.txt_paymentstatus.setBackgroundResource(R.color.color_pending_dark)

            holder.tv_time.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_pending_time, 0, 0, 0)
            holder.tv_location.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_pending_zone, 0, 0, 0)
            holder.tv_material.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_pending_material, 0, 0, 0)

        } else if (schedulelist?.get(position)?.service_status == 1) {
            holder.layout_schedule.setBackgroundResource(R.color.color_ongoing_light)
            holder.indicater.setBackgroundResource(R.color.color_ongoing_dark)
            holder.txt_paymentstatus.setBackgroundResource(R.color.color_ongoing_dark)

            holder.tv_time.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_ongoing_time, 0, 0, 0)
            holder.tv_location.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_zone, 0, 0, 0)
            holder.tv_material.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_material, 0, 0, 0)

        } else if (schedulelist?.get(position)?.service_status == 2) {
            holder.layout_schedule.setBackgroundResource(R.color.color_finished_light)
            holder.indicater.setBackgroundResource(R.color.color_finished_dark)
            holder.txt_paymentstatus.setBackgroundResource(R.color.color_finished_dark)

            holder.tv_time.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_finished_time, 0, 0, 0)
            holder.tv_location.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_finished_zone, 0, 0, 0)
            holder.tv_material.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_finished_material, 0, 0, 0)

        } else if (schedulelist?.get(position)?.service_status == 3) {
            holder.layout_schedule.setBackgroundResource(R.color.color_cancelled_light)
            holder.indicater.setBackgroundResource(R.color.color_cancelled_dark)
            holder.txt_paymentstatus.setBackgroundResource(R.color.color_cancelled_dark)

            holder.tv_time.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_cancel_time, 0, 0, 0)
            holder.tv_location.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_cancel_zone, 0, 0, 0)
            holder.tv_material.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_cancel_material, 0, 0, 0)

        }

        if (schedulelist?.get(position)?.paymentStatus.toString() == "0") {
            holder.txt_paymentstatus.visibility = View.INVISIBLE //= "Not Paid"
        } else if (schedulelist?.get(position)?.paymentStatus.toString() == "1") {
            holder.txt_paymentstatus.visibility = View.VISIBLE // = "Paid"
        }



        holder.tv_name.text = schedulelist?.get(position)?.customer_name.toString()
        holder.tv_time.text =
            schedulelist!!.get(position)?.shift_start + " - " + schedulelist!!.get(position)?.shift_end
        holder.tv_location.text = schedulelist.get(position)?.zone
        holder.tv_material.text = schedulelist.get(position)?.cleaning_material


        if (schedulelist[position]?.maidlist != null){
            if (schedulelist[position]?.maidlist!!.isNotEmpty()) {
                if (schedulelist.get(position)?.maidlist?.size!! >= 3) {
                    holder.txt_maid1.visibility = View.VISIBLE
                    holder.img_maid1.visibility = View.VISIBLE
                    holder.txt_maid1.text = schedulelist.get(position)?.maidlist?.get(0)?.maid_name

                    if (schedulelist.get(position)?.maidlist?.get(0)?.maid_image!!.isNotEmpty()) {

                        val savedUri: Uri? =
                            schedulelist.get(position)?.maidlist?.get(0)?.maid_image!!.toUri()
                        holder.img_maid1.setImageURI(savedUri)

                    } else {
                        holder.img_maid1.setImageResource(R.drawable.nomaidimage)
                    }

                    holder.txt_maid2.visibility = View.VISIBLE
                    holder.img_maid2.visibility = View.VISIBLE
                    holder.txt_maid2.text = schedulelist.get(position)?.maidlist?.get(1)?.maid_name


                    if (schedulelist.get(position)?.maidlist?.get(1)?.maid_image!!.isNotEmpty()) {

                        val savedUri: Uri? =
                            schedulelist.get(position)?.maidlist?.get(1)?.maid_image!!.toUri()
                        holder.img_maid2.setImageURI(savedUri)

                    } else {
                        holder.img_maid2.setImageResource(R.drawable.nomaidimage)
                    }


                    holder.txt_maid3.visibility = View.VISIBLE
                    holder.img_maid3.visibility = View.VISIBLE
                    holder.txt_maid3.text = schedulelist.get(position)?.maidlist?.get(2)?.maid_name

                    if (schedulelist.get(position)?.maidlist?.get(2)?.maid_image!!.isNotEmpty()) {

                        val savedUri: Uri? =
                            schedulelist.get(position)?.maidlist?.get(2)?.maid_image!!.toUri()
                        holder.img_maid3.setImageURI(savedUri)


                    } else {
                        holder.img_maid3.setImageResource(R.drawable.nomaidimage)
                    }


                    val rem = schedulelist.get(position)?.maidlist!!.size - 3
                    if (rem > 0) {
                        holder.txt_remaining.text = "+" + rem
                    } else {
                        holder.txt_remaining.visibility = View.INVISIBLE
                    }
                } else {
                    val maidarraysize = schedulelist.get(position)?.maidlist!!.size
                    if (maidarraysize == 1) {
                        holder.txt_maid1.visibility = View.VISIBLE
                        holder.txt_maid2.visibility = View.INVISIBLE
                        holder.txt_maid3.visibility = View.INVISIBLE

                        holder.img_maid1.visibility = View.VISIBLE
                        holder.img_maid2.visibility = View.INVISIBLE
                        holder.img_maid3.visibility = View.INVISIBLE

                        holder.txt_remaining.visibility = View.INVISIBLE

                        holder.txt_maid1.text =
                            schedulelist.get(position)?.maidlist?.get(0)?.maid_name

                        if (schedulelist.get(position)?.maidlist?.get(0)?.maid_image!!.isNotEmpty()) {

                            val savedUri: Uri? =
                                schedulelist.get(position)?.maidlist?.get(0)?.maid_image!!.toUri()
                            holder.img_maid1.setImageURI(savedUri)

                        } else {
                            holder.img_maid1.setImageResource(R.drawable.nomaidimage)
                        }

                    } else if (maidarraysize == 2) {
                        holder.txt_maid1.visibility = View.VISIBLE
                        holder.txt_maid2.visibility = View.VISIBLE
                        holder.txt_maid3.visibility = View.INVISIBLE

                        holder.img_maid1.visibility = View.VISIBLE
                        holder.img_maid2.visibility = View.VISIBLE
                        holder.img_maid3.visibility = View.INVISIBLE

                        holder.txt_remaining.visibility = View.INVISIBLE

                        holder.txt_maid1.text =
                            schedulelist.get(position)?.maidlist?.get(0)?.maid_name
                        holder.txt_maid2.text =
                            schedulelist.get(position)?.maidlist?.get(1)?.maid_name

                        if (schedulelist.get(position)?.maidlist?.get(1)?.maid_image!!.isNotEmpty()) {

                            val savedUri: Uri? =
                                schedulelist.get(position)?.maidlist?.get(1)?.maid_image!!.toUri()
                            holder.img_maid2.setImageURI(savedUri)


                        } else {
                            holder.img_maid2.setImageResource(R.drawable.nomaidimage)
                        }

                        if (schedulelist.get(position)?.maidlist?.get(0)?.maid_image!!.isNotEmpty()) {

                            val savedUri: Uri? =
                                schedulelist.get(position)?.maidlist?.get(0)?.maid_image!!.toUri()
                            holder.img_maid1.setImageURI(savedUri)


                        } else {
                            holder.img_maid1.setImageResource(R.drawable.nomaidimage)
                        }
                    }
                }
            }
    }

        holder.cv_booking.setOnClickListener {
            onItemClicked.invoke(schedulelist.get(position), position, schedulelist)
        }


    }

    override fun getItemCount(): Int {
        return schedulelist?.size!!
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var layout_schedule = itemView.findViewById<LinearLayout>(R.id.layout_schedule)
        var indicater = itemView.findViewById<View>(R.id.indicater)
        var cv_booking = itemView.findViewById<CardView>(R.id.cv_booking)
        var tv_name = itemView.findViewById<TextView>(R.id.tv_name)
        var tv_time = itemView.findViewById<TextView>(R.id.tv_time)
        var tv_location = itemView.findViewById<TextView>(R.id.tv_location)
        var tv_material = itemView.findViewById<TextView>(R.id.tv_material)
        var txt_maid1 = itemView.findViewById<TextView>(R.id.txt_maid1)
        var txt_maid2 = itemView.findViewById<TextView>(R.id.txt_maid2)
        var txt_maid3 = itemView.findViewById<TextView>(R.id.txt_maid3)
        var txt_remaining = itemView.findViewById<TextView>(R.id.txt_remaining)
        var img_maid1 = itemView.findViewById<ImageView>(R.id.img_maid1)
        var img_maid2 = itemView.findViewById<ImageView>(R.id.img_maid2)
        var img_maid3 = itemView.findViewById<ImageView>(R.id.img_maid3)
        var txt_paymentstatus = itemView.findViewById<TextView>(R.id.txt_paymentstatus)
    }


}