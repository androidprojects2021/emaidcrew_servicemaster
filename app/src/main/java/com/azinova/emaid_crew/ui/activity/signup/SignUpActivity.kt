package com.azinova.emaid_crew.ui.activity.signup

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.azinova.emaid_crew.R
import com.azinova.emaid_crew.ui.activity.login.LoginActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {
    var usertype: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        clickListen()
        observeLoggedIn()
    }

    private fun observeLoggedIn() {

    }

    private fun clickListen() {
        linearLayout_login.setOnClickListener {
            val intent = Intent(applicationContext, LoginActivity::class.java)
            startActivity(intent)
        }

        signup_crew_click.setOnClickListener {
            usertype = 0

            signup_crew_click.setBackgroundResource(R.drawable.bg_purple_left_curve)
            signup_crew_click.setTextColor(Color.WHITE)

            signup_driver_click.setBackgroundResource(R.drawable.bg_square_rightcurve)
            signup_driver_click.setTextColor(getResources().getColor(R.color.colorPrimary))
        }
        signup_driver_click.setOnClickListener {
            usertype = 1

            signup_crew_click.setBackgroundResource(R.drawable.bg_square_leftcurve)
            signup_crew_click.setTextColor(getResources().getColor(R.color.colorPrimary))

            signup_driver_click.setBackgroundResource(R.drawable.bg_purple_right_curve)
            signup_driver_click.setTextColor(Color.WHITE)

        }
        signupButton.setOnClickListener {

            if (txt_userfullname.text.toString().isEmpty()) {
                Snackbar.make(outersignup, "Please Enter Username", Snackbar.LENGTH_SHORT)
                    .show()
            } else if ((txt_userphoneno.text.toString().isEmpty()) || (!(isValidMobile(
                    txt_userphoneno.text.toString()
                )))
            ) {
                Snackbar.make(
                    outersignup,
                    "Please Enter  Proper Phone Number",
                    Snackbar.LENGTH_SHORT
                )
                    .show()
            } else if ((txt_useremailid.text.toString()
                    .isEmpty()) || (!(isValidMail(txt_useremailid.text.toString())))
            ) {
                Snackbar.make(outersignup, "Please Enter Proper email id", Snackbar.LENGTH_SHORT)
                    .show()
            } else if (txt_userpassword.text.toString().isEmpty()) {
                Snackbar.make(outersignup, "Please Enter Password", Snackbar.LENGTH_SHORT)
                    .show()
            } else if (!txt_userpassword.text.toString()
                    .equals(txt_userconfirmpassword.text.toString())
            ) {
                Snackbar.make(outersignup, "Password Missmatch", Snackbar.LENGTH_SHORT)
                    .show()
            } else {
                Toast.makeText(this, "SignUp Success", Toast.LENGTH_LONG).show()
            }

        }

    }

    private fun isValidMail(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun isValidMobile(phone: String): Boolean {
        return Patterns.PHONE.matcher(phone).matches()
    }
}