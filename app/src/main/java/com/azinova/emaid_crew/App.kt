package com.azinova.emaid_crew

import android.app.Application
import android.util.Log
import com.azinova.emaid_crew.utils.SharedPref
import net.khirr.library.foreground.Foreground

class App: Application() ,Foreground.Listener{
    private val prefs = SharedPref

    override fun onCreate() {
        super.onCreate()

        //  Initialize
        Foreground.Companion.init(this)
        //  Add listener
        Foreground.Companion.addListener(this)
        val foregroundListener = object: Foreground.Listener {
            override fun background() {
                Log.e("Listener Foreground", "Go to background")
            }

            override fun foreground() {
                Log.e("Listener Foreground", "Go to foreground")
            }
        }

        Foreground.addListener(foregroundListener)
    }

    override fun background() {
        Log.e("Foreground", "Go to background");
    }

    override fun foreground() {
        Log.e("Foreground", "Go to foreground");
    }
}