package com.azinova.emaid_crew.network

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiClient {

    // private const val BASE_URL = "https://booking.emaid.info/service-master-demo/crewappapi/" // Servicemaster Demo

    private const val BASE_URL = "https://booking.emaid.info/service-master/crewappapi/" //  Servicemaster live

    fun getRetrofit(): ApiInterface {

         var interceptor = HttpLoggingInterceptor()
         interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

         var client = OkHttpClient.Builder()
             .addInterceptor(interceptor)
             .connectTimeout(60, TimeUnit.SECONDS)
             .readTimeout(60, TimeUnit.SECONDS)
             .writeTimeout(60, TimeUnit.SECONDS)
             .addNetworkInterceptor { chain: Interceptor.Chain ->
                 val request = chain.request().newBuilder()
                     .build()
                 chain.proceed(request)
             }.build()


        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
            .create(ApiInterface::class.java)
    }


}