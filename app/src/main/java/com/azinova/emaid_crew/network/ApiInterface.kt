package com.azinova.emaid_crew.network

import androidx.annotation.NonNull
import com.azinova.emaid_crew.model.response.bookings.ResponseBookings
import com.azinova.emaid_crew.model.response.cancel.ResponseCancelService
import com.azinova.emaid_crew.model.response.edit_profile.ResponseEditProfile
import com.azinova.emaid_crew.model.response.finish.ResponseFinishService
import com.azinova.emaid_crew.model.response.location.ResponseLocationUpdate
import com.azinova.emaid_crew.model.response.login.ResponseLogin
import com.azinova.emaid_crew.model.response.logout.ResponseLogout
import com.azinova.emaid_crew.model.response.maidattendance.ResponseMaidAttendance
import com.azinova.emaid_crew.model.response.maiddetails.ResponseMaidBookingDetailsList
import com.azinova.emaid_crew.model.response.maiddetails.ResponseMaidDetailsList
import com.azinova.emaid_crew.model.response.maidstatus.ResponseMaidStatusChange
import com.azinova.emaid_crew.model.response.notification.ResponseNotification
import com.azinova.emaid_crew.model.response.payment.ResponsePayment
import com.azinova.emaid_crew.model.response.report.ResponseScheduleReport
import com.azinova.emaid_crew.model.response.start.ResponseStartService
import com.azinova.emaid_crew.model.response.transfer.ResponseBookingTransfer
import com.azinova.emaid_crew.model.response.transfer.ResponseTransferlist
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST


interface ApiInterface {

    @FormUrlEncoded
    @POST("appLogin")
    suspend fun Login(
        @Field("username") @NonNull username: String,
        @Field("userpassword") @NonNull userpassword: String,
        @Field("maid_driver_status") @NonNull maid_driver_status: String,
        @Field("date") @NonNull date: String,
        @Field("device_regid") @NonNull device_regid: String
    ): ResponseLogin

    @FormUrlEncoded
    @POST("getbookings_schedule")
    suspend fun getBookings(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String
    ): ResponseBookings

    @FormUrlEncoded
    @POST("payment_list")
    suspend fun getPaymentlist(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_status") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String
    ): ResponsePayment

    @FormUrlEncoded
    @POST("maid_details_list")
    suspend fun getMaidDetailslist(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_status") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String,
        @Field("booking_id") @NonNull booking_id: String
    ): ResponseMaidDetailsList

    @FormUrlEncoded
    @POST("get_transfer_list")
    suspend fun getTransferlist(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String,
        @Field("booking_id") @NonNull booking_id: String
    ): ResponseTransferlist

    @FormUrlEncoded
    @POST("transfer_maid")
    suspend fun getBookingsTransfer(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String,
        @Field("booking_id") @NonNull booking_id: String,
        @Field("zone_id") @NonNull zone_id: String,
        @Field("transfer_all") @NonNull transfer_type: String
    ): ResponseBookingTransfer

    // START SERVICE
    @FormUrlEncoded
    @POST("service_start_stop")
    suspend fun getStartService(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String,
        @Field("booking_id") @NonNull booking_id: String,
        @Field("status") @NonNull status: String,
        @Field("payment") @NonNull payment: String,
        @Field("amount") @NonNull amount: String,
        @Field("receipt_no") @NonNull receipt_no: String,
        @Field("method") @NonNull method: String,
        @Field("outstanding_amount") @NonNull outstanding_amount: String,
        @Field("notes") @NonNull notes: String
    ): ResponseStartService

    // FINISH SERVICE
    @FormUrlEncoded
    @POST("service_start_stop")
    suspend fun getFinishService(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String,
        @Field("booking_id") @NonNull booking_id: String,
        @Field("status") @NonNull status: String,
        @Field("payment") @NonNull payment: String,
        @Field("amount") @NonNull amount: String,
        @Field("receipt_no") @NonNull receipt_no: String,
        @Field("method") @NonNull method: String,
        @Field("outstanding_amount") @NonNull outstanding_amount: String,
        @Field("notes") @NonNull notes: String
    ): ResponseFinishService

    // CANCEL SERVICE
    @FormUrlEncoded
    @POST("service_start_stop")
    suspend fun getCancelService(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String,
        @Field("booking_id") @NonNull booking_id: String,
        @Field("status") @NonNull status: String,
        @Field("payment") @NonNull payment: String,
        @Field("amount") @NonNull amount: String,
        @Field("receipt_no") @NonNull receipt_no: String,
        @Field("method") @NonNull method: String,
        @Field("outstanding_amount") @NonNull outstanding_amount: String,
        @Field("notes") @NonNull notes: String
    ): ResponseCancelService


    @FormUrlEncoded
    @POST("maid_in_out")
    suspend fun getMaidStatusChange(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String,
        @Field("status") @NonNull status: String,
        @Field("maid_id") @NonNull maid_id: String
    ): ResponseMaidStatusChange

    @FormUrlEncoded
    @POST("maid_details")
    suspend fun getMaidbookingsDetailslist(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String,
        @Field("maid_id") @NonNull maid_id: String,
        @Field("booking_id") @NonNull booking_id: String
    ): ResponseMaidBookingDetailsList


    @FormUrlEncoded
    @POST("maid_attendance_list")
    suspend fun getMaidAttendancelist(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String
    ): ResponseMaidAttendance

    @FormUrlEncoded
    @POST("schedule_reports")
    suspend fun getScheduleReport(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String
    ): ResponseScheduleReport

    @FormUrlEncoded
    @POST("profile_edit")
    suspend fun getEditProfile(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String,
        @Field("name") @NonNull name: String,
        @Field("phone") @NonNull phone: String,
        @Field("email") @NonNull email: String
    ): ResponseEditProfile

    @FormUrlEncoded
    @POST("update_location")
   suspend fun getLocationUpdate(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String,
        @Field("latitude") @NonNull latitude: String,
        @Field("longitude") @NonNull longitude: String
    ): ResponseLocationUpdate

    @FormUrlEncoded
    @POST("get_notifications")
    suspend fun getnotificationList(
            @Field("id") @NonNull id: String,
            @Field("maid_driver_stat") @NonNull type: String,
            @Field("date") @NonNull date: String,
            @Field("token") @NonNull token: String
    ): ResponseNotification

    @FormUrlEncoded
    @POST("logout")
    suspend fun hitLogout(
            @Field("id") @NonNull id: String,
            @Field("maid_driver_stat") @NonNull type: String,
            @Field("token") @NonNull token: String
    ): ResponseLogout

}
