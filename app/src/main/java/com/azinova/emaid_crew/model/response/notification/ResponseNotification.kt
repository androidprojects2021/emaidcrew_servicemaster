package com.azinova.emaid_crew.model.response.notification

import com.google.gson.annotations.SerializedName

data class ResponseNotification(

	@field:SerializedName("notification_details")
	val notificationDetails: List<NotificationDetailsItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class NotificationListItem(

	@field:SerializedName("date")
	val n_date: String? = null,

	@field:SerializedName("time")
	val time: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("type")
	val type: String? = null
)

data class NotificationDetailsItem(

	@field:SerializedName("notification_list")
	val notificationList: List<NotificationListItem?>? = null,

	@field:SerializedName("Date")
	val date: String? = null
)
