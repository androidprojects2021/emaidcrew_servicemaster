package com.azinova.emaid_crew.model.response.schedules

data class MaidDetails(

        var maid_id: String? = null,

        var customer_id: String? = null,

        var booking_id: Int? = null,

        var maid_name: String? = null,

        var maid_attandence: String? = null,

        var service_status: Int? = null,

        var maid_image: String? = null


)
