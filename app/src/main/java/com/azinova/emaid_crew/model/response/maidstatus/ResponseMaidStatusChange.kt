package com.azinova.emaid_crew.model.response.maidstatus

import com.google.gson.annotations.SerializedName

data class ResponseMaidStatusChange(

	@field:SerializedName("attandence_status")
	val attandenceStatus: Int? = null,

	@field:SerializedName("maid_id")
	val maidId: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("maid_details")
	val maidDetails: MaidDetails? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class MaidDetails(

	@field:SerializedName("maid_attandence")
	val maidAttandence: String? = null,

	@field:SerializedName("maid_id")
	val maidId: String? = null,

	@field:SerializedName("maid_country")
	val maidCountry: String? = null,

	@field:SerializedName("maid_name")
	val maidName: String? = null,

	@field:SerializedName("maid_image")
	val maidImage: String? = null
)
