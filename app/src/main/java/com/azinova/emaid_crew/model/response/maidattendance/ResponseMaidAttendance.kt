package com.azinova.emaid_crew.model.response.maidattendance

import com.google.gson.annotations.SerializedName

data class ResponseMaidAttendance(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("maid_details")
	val maidDetails: List<MaidDetailsItem?>? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class MaidDetailsItem(

	@field:SerializedName("maid_attandence")
	val maidAttandence: String? = null,

	@field:SerializedName("maid_id")
	val maidId: String? = null,

	@field:SerializedName("maid_country")
	val maidCountry: String? = null,

	@field:SerializedName("maid_name")
	val maidName: String? = null,

	@field:SerializedName("maid_image")
	val maid_image: String? = null
)
