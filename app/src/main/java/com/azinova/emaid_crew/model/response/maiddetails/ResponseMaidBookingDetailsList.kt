package com.azinova.emaid_crew.model.response.maiddetails

import com.google.gson.annotations.SerializedName

data class ResponseMaidBookingDetailsList(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("maid_details")
	val maidDetails: MaidDetails? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class MaidDetails(

	@field:SerializedName("next_schedules")
	val nextSchedules: List<NextSchedulesItemList?>? = null,

	@field:SerializedName("maid_attendance")
	val maidAttendance: String? = null,

	@field:SerializedName("maid_id")
	val maidId: String? = null,

	@field:SerializedName("maid_name")
	val maidName: String? = null,

	@field:SerializedName("maid_image")
	val maidImage: String? = null
)

data class NextSchedulesItemList(

	@field:SerializedName("booking_id")
	val bookingId: String? = null,

	@field:SerializedName("shift_start")
	val shiftStart: String? = null,

	@field:SerializedName("customer_address")
	val customerAddress: String? = null,

	@field:SerializedName("zone")
	val zone: String? = null,

	@field:SerializedName("shift_end")
	val shiftEnd: String? = null,

	@field:SerializedName("customer_name")
	val customerName: String? = null,

	@field:SerializedName("customer_id")
	val customerId: String? = null,

	@field:SerializedName("schedule_date")
	val scheduleDate: String? = null,

	@field:SerializedName("service_status")
	val service_status: String? = null
)
