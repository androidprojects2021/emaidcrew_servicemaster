package com.azinova.emaid_crew.model.response.transfer

import com.google.gson.annotations.SerializedName

data class ResponseTransferlist(

	@field:SerializedName("transfer_list")
	val transferList: List<TransferListItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class TransferListItem(

	@field:SerializedName("zone_id")
	val zoneId: String? = null,

	@field:SerializedName("tablet_id")
	val tabletId: String? = null,

	@field:SerializedName("tablet_driver")
	val tabletDriver: String? = null,

	@field:SerializedName("zone_name")
	val zoneName: String? = null
)

