package com.azinova.emaid_crew.model.input

import com.google.gson.annotations.SerializedName

data class InputBookings(

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("maid_driver_stat")
	val maidDriverStat: String? = null,

	@field:SerializedName("date")
	val date: String? = null
)
