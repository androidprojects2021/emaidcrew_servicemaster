package com.azinova.emaid_crew.model.response.bookings

import com.google.gson.annotations.SerializedName

data class ResponseBookings(

	@field:SerializedName("schedule")
	val schedule: List<ScheduleItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class ScheduleItem(

	@field:SerializedName("Schedule")
	val schedule: Schedule? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("schedule_date")
	val scheduleDate: String? = null,

	@field:SerializedName("maid_details")
	val maidDetails: List<MaidDetailsItem?>? = null,

	@field:SerializedName("schedule_timeing")
	val scheduleTimeing: ScheduleTimeing? = null
)

data class MaidDetailsItem(

	@field:SerializedName("maid_attandence")
	val maidAttandence: String? = null,

	@field:SerializedName("maid_id")
	val maidId: String? = null,

	@field:SerializedName("maid_country")
	val maidCountry: String? = null,

	@field:SerializedName("customer_id")
	val customerId: String? = null,

	@field:SerializedName("maid_name")
	val maidName: String? = null,

	@field:SerializedName("maid_image")
	val maid_image: String? = null
)

data class Schedule(

	@field:SerializedName("customer_type")
	val customerType: String? = null,

	@field:SerializedName("extra_service")
	val extraService: String? = null,

	@field:SerializedName("key_status")
	val keyStatus: String? = null,

	@field:SerializedName("tools")
	val tools: String? = null,

	@field:SerializedName("booking_note")
	val bookingNote: String? = null,

	@field:SerializedName("customer_notes")
	val customernotes: String? = null,

	@field:SerializedName("mop")
	val mop: String? = null,

	@field:SerializedName("booking_id")
	val bookingId: String? = null,

	@field:SerializedName("total")
	val total: String? = null,

	@field:SerializedName("balance")
	val balance: Double? = null,

	@field:SerializedName("zone")
	val zone: String? = null,

	@field:SerializedName("outstanding_balance")
	val outstandingBalance: String? = null,

	@field:SerializedName("shift_end")
	val shiftEnd: String? = null,

	@field:SerializedName("customer_longitude")
	val customerLongitude: String? = null,

	@field:SerializedName("customer_code")
	val customerCode: String? = null,

	@field:SerializedName("area")
	val area: String? = null,

	@field:SerializedName("customer_address")
	val customerAddress: String? = null,

	@field:SerializedName("customer_mobile")
	val customerMobile: String? = null,

	@field:SerializedName("cleaning_material")
	val cleaningMaterial: String? = null,

	@field:SerializedName("payment_status")
	val paymentStatus: Int? = null,

	@field:SerializedName("service_fee")
	val serviceFee: String? = null,

	@field:SerializedName("shift_start")
	val shiftStart: String? = null,

	@field:SerializedName("servicetype")
	val servicetype: String? = null,

	@field:SerializedName("customer_latitude")
	val customerLatitude: String? = null,

	@field:SerializedName("new_ref")
	val newRef: String? = null,

	@field:SerializedName("service_status")
	val serviceStatus: Int? = null,

	@field:SerializedName("paid_amount")
	val paidAmount: Double? = null,

	@field:SerializedName("customer_name")
	val customerName: String? = null,

	@field:SerializedName("customer_id")
	val customerId: String? = null,

	@field:SerializedName("starttime")
	val starttime: String? = null,

	@field:SerializedName("endtime")
	val endtime: String? = null
)

data class ScheduleTimeing(

	@field:SerializedName("No.of.maids")
	val noOfMaids: Int? = null,

	@field:SerializedName("timeing")
	val timeing: String? = null
)
