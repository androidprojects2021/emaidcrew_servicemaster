package com.azinova.emaid_crew.model.response.report

import com.google.gson.annotations.SerializedName

data class ResponseScheduleReport(

	@field:SerializedName("report_details")
	val reportDetails: ReportDetails? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class ReportDetails(

	@field:SerializedName("booking_hours")
	val bookingHours: Double? = null,

	@field:SerializedName("total_billed")
	val totalBilled: Double? = null,

	@field:SerializedName("booking_count")
	val bookingCount: Int? = null,

	@field:SerializedName("total_collected")
	val totalCollected: Double? = null,

	@field:SerializedName("cash_in_hand")
	val cashinhand: Double? = null
)
