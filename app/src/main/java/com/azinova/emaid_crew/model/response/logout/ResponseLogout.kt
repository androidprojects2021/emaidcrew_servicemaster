package com.azinova.emaid_crew.model.response.logout

import com.google.gson.annotations.SerializedName

data class ResponseLogout(
        @field:SerializedName("message")
        val message: String? = null,

        @field:SerializedName("status")
        val status: String? = null
)