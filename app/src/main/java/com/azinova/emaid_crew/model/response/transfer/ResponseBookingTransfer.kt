package com.azinova.emaid_crew.model.response.transfer

import com.google.gson.annotations.SerializedName

data class ResponseBookingTransfer(

	@field:SerializedName("transfered_bookings")
	val transferDetails: List<TransferDetailsItem>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("transfer_all")
	val transferAll: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class TransferDetailsItem(

	@field:SerializedName("booking_id")
	val bookingId: String? = null,

	@field:SerializedName("customer_id")
	val customerId: String? = null,

	@field:SerializedName("no_of_maids")
	val noOfMaids: Int? = null,

	@field:SerializedName("time_from")
	val timeing: String? = null
)
