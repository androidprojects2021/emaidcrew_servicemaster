package com.azinova.emaid_crew.model.response.edit_profile

import com.google.gson.annotations.SerializedName

data class ResponseEditProfile(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)
