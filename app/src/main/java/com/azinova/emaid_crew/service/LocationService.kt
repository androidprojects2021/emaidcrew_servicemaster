package com.azinova.emaid_crew.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Intent
import android.location.Location
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import com.azinova.emaid_crew.R
import com.azinova.emaid_crew.network.ApiClient
import com.azinova.emaid_crew.ui.activity.home.MainActivity
import com.azinova.emaid_crew.utils.SharedPref
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*


class LocationService : Service() {
    private val prefs = SharedPref
    private lateinit var parent: MainActivity


    private val NOTIFICATION_CHANNEL_ID = "my_notification_location"
    private val TAG = "LocationService"

    val sdf = SimpleDateFormat("yyyy-MM-dd")
    val currentdate = sdf.format(Date())

    override fun onCreate() {
        super.onCreate()
        isServiceStarted = true

        parent = MainActivity()


        val builder: NotificationCompat.Builder =
                NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                        .setOngoing(false)
                        .setSmallIcon(R.drawable.logo_main)
                        .setContentTitle("Location Tracking")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager: NotificationManager =
                    getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            val notificationChannel = NotificationChannel(
                    NOTIFICATION_CHANNEL_ID,
                    NOTIFICATION_CHANNEL_ID, NotificationManager.IMPORTANCE_LOW
            )
            notificationChannel.description = NOTIFICATION_CHANNEL_ID
            notificationChannel.setSound(null, null)
            notificationManager.createNotificationChannel(notificationChannel)
            startForeground(1, builder.build())
        }
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        val timer = Timer()
        LocationHelper().startListeningUserLocation(
                this, object : MyLocationListener {
            override fun onLocationChanged(location: Location?) {
                mLocation = location
                mLocation?.let {

                    prefs.Latitude = it.latitude.toString()
                    prefs.Longitude = it.longitude.toString()
                    Log.d(
                            TAG,
                            "onLocationChanged: Latitude ${it.latitude} , Longitude ${it.longitude}"
                    )

                    AppExecutors.instance?.networkIO()?.execute {
                        GlobalScope.launch {
                            try {
                                val response = ApiClient.getRetrofit().getLocationUpdate(
                                        prefs.user_id.toString(),
                                        prefs.user_type.toString(),
                                        currentdate,
                                        prefs.token!!,
                                        prefs.Latitude.toString(),
                                        prefs.Longitude.toString()
                                )
                                if (response.status.equals("success")) {
                                    Log.d("Success", "onLocationChanged: " + "Location Updated")
                                }

                               /* else if (response.status.equals("token_error")) {
                                    Loader.hideLoader()
                                    Toast.makeText(this@LocationService, response.message, Toast.LENGTH_SHORT)
                                            .show()


                                    clearLogin()
                                    parent.stopServiceTracker()
                                    BookingsRepository.database?.clearAllTables()

                                    val intent = Intent(this@LocationService, LoginActivity::class.java)
                                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                    startActivity(intent)


                                }
*/
                                else {
                                    Log.d("TAG", "onLocationChanged: " + "Location Update Failed")

                                }


                            } catch (e: Exception) {
                                Log.e("throw", "location Update: ")

                            }

                        }
                    }
                }
            }
        })
        return START_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onDestroy() {
        super.onDestroy()
        isServiceStarted = false

    }

    companion object {
        var mLocation: Location? = null
        var isServiceStarted = false
    }

    private fun clearLogin() {
        // prefs.clearSession(requireContext())
        prefs.user_details = ""
        prefs.user_id = 0
        prefs.user_name = ""
        prefs.user_type = ""
        prefs.token = ""
        prefs.image = ""
        prefs.phone = ""
        prefs.email = ""
        prefs.Latitude = ""
        prefs.Longitude = ""
    }
}