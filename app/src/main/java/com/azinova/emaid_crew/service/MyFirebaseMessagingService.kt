package com.azinova.emaid_crew.service

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.azinova.emaid_crew.R
import com.azinova.emaid_crew.ui.activity.home.MainActivity
import com.azinova.emaid_crew.utils.NotificationUtils
import com.azinova.emaid_crew.utils.SharedPref
import com.azinova.emaid_crew.utils.notification.BaseActivity
import com.azinova.emaid_crew.utils.notification.Myservices
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import net.khirr.library.foreground.Foreground
import java.text.SimpleDateFormat
import java.util.*


class MyFirebaseMessagingService : FirebaseMessagingService() {
    private var TAG = "MyFirebaseMessagingService"
    private val prefs = SharedPref


    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.d(TAG, "Token : " + token)
        Log.d("Firebase Token", "clickListen - token: " + token)

        prefs.firebasetoken = token
    }


    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        Log.d(TAG, "message receive : " + remoteMessage.from)

        //Verify if the message contains data
        if (remoteMessage.data.isNotEmpty()) {
            Log.d(TAG, "Message data : " + remoteMessage.data)
        }

        //Verify if the message contains notification
        if (remoteMessage.notification != null) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Log.d(TAG, "Message body : " + remoteMessage.notification!!.body)

                generateNotification(this, remoteMessage.getNotification()?.getBody()!!)

            }

            //current
            /* showNotificationsettings(this,
                     remoteMessage.notification!!.title,
                     remoteMessage.notification!!.body,
                     remoteMessage.data.toString()
             )*/

/*
             sendNotification(remoteMessage.notification!!.body)
*/
        }


/*

         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

             generateNotification(this, remoteMessage.getNotification()?.getBody()!!)

         }

*/

    }

    private fun showNotificationsettings(
            context: Context,
            title: String?,
            body: String?,
            remoteMessage: String
    ) {
        if (!NotificationUtils.isAppIsInBackground(context)) {   //   inForeground

            try {
                playsound(context)
            } catch (e: IllegalArgumentException) {
                e.printStackTrace()
            }

            // NotificationUtils.playNotificationSound(context) // loud
            Log.d("Emaid crew 1", "generateNotification: ")


            val myIntent = Intent(context, Myservices::class.java)
            myIntent.putExtra("message", body)
            myIntent.putExtra("heading", "Notification")
            myIntent.putExtra("isNotification7", "isNotification")
            context.startService(myIntent)

        } else if (NotificationUtils.isAppIsInBackground(context)) {              //  inBackground
            Log.d("Emaid crew 2", "generateNotification: ")

            var notification_id = 0
            val c = Calendar.getInstance()
            val df = SimpleDateFormat("kkmmss")
            val strTodayDate = df.format(c.time)

            notification_id = try {
                strTodayDate.toInt()
            } catch (e: java.lang.Exception) {
                0
            }


            NotificationUtils.playNotificationSound(context)

            val intent = Intent(context, MainActivity::class.java)

            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            //intent.putExtra("Notification", body)
            intent.putExtra("page", "notification")

            val pendingIntent = PendingIntent.getActivity(
                    context,
                    0,
                    intent,
                    PendingIntent.FLAG_ONE_SHOT
            )
            val notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

            val notificationBuilder = NotificationCompat.Builder(context, "Notification")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(body)
                .setContentText("")
                .setAutoCancel(true)
                .setSound(notificationSound)
                .setContentIntent(pendingIntent)

            val notificationManager: NotificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.notify(notification_id, notificationBuilder.build())
        }
    }


    private fun sendNotification(body: String?) {
        if (Foreground.isBackground()) {
            Log.e("Notification Foreground", "Go to background" + "+++++++=")

        } else if (Foreground.isForeground()) {
            Log.e("Notification Foreground", "Go to foreground")

        }
        val intent = Intent(this, MainActivity::class.java)

        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        intent.putExtra("Notification", body)

        val pendingIntent = PendingIntent.getActivity(
                this,
                0,
                intent,
                PendingIntent.FLAG_ONE_SHOT
        )
        val notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val notificationBuilder = NotificationCompat.Builder(this, "Notification")
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle("Push Notification FCM")
            .setContentText(body)
            .setAutoCancel(true)
            .setSound(notificationSound)
            .setContentIntent(pendingIntent)

        val notificationManager: NotificationManager =
            this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(0, notificationBuilder.build())
    }


    // -----------------------------------------

    override fun onDeletedMessages() {
        super.onDeletedMessages()
    }

    override fun onMessageSent(p0: String) {
        super.onMessageSent(p0)
    }

    override fun onSendError(p0: String, p1: Exception) {
        super.onSendError(p0, p1)
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private fun generateNotification(context: Context, message: String) {
        try {
            if (BaseActivity.appInFront) {                //appInFront
                println("*****cccc**")
                try {
                    playsound(context)
                } catch (e: IllegalArgumentException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }
                val myIntent = Intent(context, Myservices::class.java)
                myIntent.putExtra("message", message)
                myIntent.putExtra("heading", "Notification")
                myIntent.putExtra("isNotification7", "isNotification")
                context.startService(myIntent)
            } else {                                             //appInBack

                var notification_id = 0
                val c = Calendar.getInstance()
                val df = SimpleDateFormat("kkmmss")
                val strTodayDate = df.format(c.time)

                notification_id = try {
                    strTodayDate.toInt()
                } catch (e: java.lang.Exception) {
                    0
                }



                val intent = Intent(this, MainActivity::class.java)

                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                intent.putExtra("page", "notification")

                val pendingIntent = PendingIntent.getActivity(
                        this,
                        0,
                        intent,
                        PendingIntent.FLAG_ONE_SHOT
                )
                val notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

                val notificationBuilder = NotificationCompat.Builder(this, "Notification")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(message)
                    .setContentText("")
                    .setAutoCancel(true)
                    .setSound(notificationSound)
                    .setContentIntent(pendingIntent)

                val notificationManager: NotificationManager =
                    this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.notify(0, notificationBuilder.build())



              /*  val mBuilder: NotificationCompat.Builder =
                    NotificationCompat.Builder(context).setSmallIcon(
                        R.mipmap.ic_launcher
                    ).setContentTitle(message).setContentText("")
                mBuilder.setAutoCancel(true)
                val resultIntent = Intent(context, MainActivity::class.java)
                resultIntent.putExtra("page", "notification")

                // Add the bundle to the intent
                val stackBuilder = TaskStackBuilder.create(context)
                stackBuilder.addNextIntent(resultIntent)
                val resultPendingIntent = stackBuilder.getPendingIntent(
                    0,
                    PendingIntent.FLAG_UPDATE_CURRENT
                )
                mBuilder.setContentIntent(resultPendingIntent)
                val alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
                mBuilder.setSound(alarmSound)
                val mNotificationManager =
                    context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
                mNotificationManager.notify(notification_id, mBuilder.build())*/
            }
        } catch (e: java.lang.Exception) {


            var notification_id = 0
            val c = Calendar.getInstance()
            val df = SimpleDateFormat("kkmmss")
            val strTodayDate = df.format(c.time)

            notification_id = try {
                strTodayDate.toInt()
            } catch (es: java.lang.Exception) {
                0
            }


            val intent = Intent(this, MainActivity::class.java)

            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            intent.putExtra("page", "notification")

            val pendingIntent = PendingIntent.getActivity(
                    this,
                    0,
                    intent,
                    PendingIntent.FLAG_ONE_SHOT
            )
            val notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

            val notificationBuilder = NotificationCompat.Builder(this, "Notification")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(message)
                .setContentText("")
                .setAutoCancel(true)
                .setSound(notificationSound)
                .setContentIntent(pendingIntent)

            val notificationManager: NotificationManager =
                this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.notify(0, notificationBuilder.build())

           /* val mBuilder: NotificationCompat.Builder =
                NotificationCompat.Builder(context).setSmallIcon(
                    R.mipmap.ic_launcher
                ).setContentTitle(message).setContentText("")
            mBuilder.setAutoCancel(true)
            val resultIntent = Intent(context, MainActivity::class.java)
            resultIntent.putExtra("page", "notification")

            // Add the bundle to the intent
            val stackBuilder = TaskStackBuilder.create(context)
            *//* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                 stackBuilder.addParentStack(Synchronize::class.java)
             }*//*
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                stackBuilder.addNextIntent(resultIntent)
            }
            val resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
            mBuilder.setContentIntent(resultPendingIntent)
            val alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            mBuilder.setSound(alarmSound)
            val mNotificationManager =
                context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            mNotificationManager?.notify(notification_id, mBuilder.build())*/
        }
    }

    fun playsound(context: Context?) {
        val alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        RingtoneManager.getRingtone(context, alarmSound).play()
    }

}