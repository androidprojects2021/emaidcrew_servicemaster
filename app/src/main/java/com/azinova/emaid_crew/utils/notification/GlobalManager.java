package com.azinova.emaid_crew.utils.notification;


public class GlobalManager {

	static GlobalManager _manger;
	public String currentActivity;

	public GlobalManager()
	{

	}

	public static GlobalManager getdata()
	{
		if (_manger == null)
			_manger = new GlobalManager();
		return _manger;
	}

	public String getCurrentActivity()
	{
		return currentActivity;
	}

	public void setCurrentActivity(String currentActivity)
	{
		this.currentActivity = currentActivity;
	}

}
