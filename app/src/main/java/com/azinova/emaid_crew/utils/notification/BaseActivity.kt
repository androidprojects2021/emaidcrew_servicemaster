package com.azinova.emaid_crew.utils.notification

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity

open class BaseActivity : AppCompatActivity() {
    var myReceiver = MyReceiver()
    override fun onCreate(arg0: Bundle?) {
        // TODO Auto-generated method stub
        super.onCreate(arg0)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        appInFront = true
        Log.d("TAG", "onCreate: " + "appInFront  ->")
    }

    @SuppressLint("MissingSuperCall")
    override fun onResume() {
        super.onStart()
        ServiceStart()
        appInFront = true
    }

    override fun onPause() {
        super.onPause()
        appInFront = false

        Log.d("TAG", "onCreate: " + "appInFront   <-")
        GlobalManager.getdata().setCurrentActivity("")
        unregisterReceiver(myReceiver)
    }

    fun ServiceStart() {


        Log.e("TAG", "ServiceStart: ")
        val intentFilter = IntentFilter()
        intentFilter.addAction(Myservices.MY_ACTION)
        registerReceiver(myReceiver, intentFilter)
    }

    class MyReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, arg1: Intent) {

            if (arg1.extras!!.getString("isNotification") != null) {

                NotificationDioalogue.NotificationNew(context, arg1.extras!!.getString("heading").toString(), arg1.extras!!.getString("message").toString(), arg1.extras!!.getString("isNotification"))

                return
            }
            NotificationDioalogue.Notification(context, arg1.extras!!.getString("heading").toString(), arg1.extras!!.getString("message").toString())
        }
    }


    companion object {
        var appInFront = false
        var dialog1: Dialog? = null
    }
}