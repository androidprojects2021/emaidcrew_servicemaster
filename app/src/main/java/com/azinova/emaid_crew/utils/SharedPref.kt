package com.azinova.emaid_crew.utils

import android.content.Context
import android.content.SharedPreferences

object SharedPref {

    private lateinit var sharedPreferences: SharedPreferences

        private const val PREF_NAME = "prefName"
        private const val USER_DETAILS = "userDetails"
        private const val USER_ID = "userid"
        private const val USER_NAME = "username"
        private const val USER_IMAGE = "userimage"
        private const val USER_PHONE = "userphone"
        private const val USER_EMAIL = "useremail"
        private const val USER_TYPE = "usertype"
        private const val TOKEN = "token"
        private const val FIREBASE_TOKEN = "firebasetoken"
        private const val LATITUDE = "latitude"
        private const val LONGITUDE = "longitude"
        private const val VIEW_STATUS = "viewstatus"


        fun clearSession(context: Context) {
            val editor = context.applicationContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit()
            editor.clear()
            editor.apply()
        }

        fun with(context: Context) {
            sharedPreferences = context.applicationContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        }


    var user_details: String?
        get() = sharedPreferences.getString(USER_DETAILS, "")
        set(id) = sharedPreferences.edit().putString(USER_DETAILS,id).apply()


    var user_id:Int
        get() = sharedPreferences.getInt(USER_ID,0)
        set(id) = sharedPreferences.edit().putInt(USER_ID,id).apply()


    var user_name: String?
        get() = sharedPreferences.getString(USER_NAME, "")
        set(value) = sharedPreferences.edit().putString(USER_NAME,value).apply()

    var user_type: String?
        get() = sharedPreferences.getString(USER_TYPE, "")
        set(value) = sharedPreferences.edit().putString(USER_TYPE,value).apply()

    var token: String?
        get() = sharedPreferences.getString(TOKEN,"")
        set(value) = sharedPreferences.edit().putString(TOKEN,value).apply()

    var image: String?
        get() = sharedPreferences.getString(USER_IMAGE,"")
        set(value) = sharedPreferences.edit().putString(USER_IMAGE,value).apply()

    var phone: String?
        get() = sharedPreferences.getString(USER_PHONE,"")
        set(value) = sharedPreferences.edit().putString(USER_PHONE,value).apply()

    var email: String?
        get() = sharedPreferences.getString(USER_EMAIL,"")
        set(value) = sharedPreferences.edit().putString(USER_EMAIL,value).apply()

    var Latitude: String?
        get() = sharedPreferences.getString(LATITUDE, "")
        set(value) = sharedPreferences.edit().putString(LATITUDE,value).apply()

    var Longitude: String?
        get() = sharedPreferences.getString(LONGITUDE, "")
        set(value) = sharedPreferences.edit().putString(LONGITUDE,value).apply()

    var firebasetoken: String?
        get() = sharedPreferences.getString(FIREBASE_TOKEN,"")
        set(value) = sharedPreferences.edit().putString(FIREBASE_TOKEN,value).apply()



}