package com.azinova.emaid_crew.utils.notification

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.azinova.emaid_crew.R
import com.azinova.emaid_crew.commons.Loader
import com.azinova.emaid_crew.database.EmaidDatabase
import com.azinova.emaid_crew.database.repository.BookingsRepository
import com.azinova.emaid_crew.network.ApiClient
import com.azinova.emaid_crew.service.AppExecutors
import com.azinova.emaid_crew.ui.activity.home.MainActivity
import com.azinova.emaid_crew.ui.activity.login.LoginActivity
import com.azinova.emaid_crew.utils.SharedPref
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

object NotificationDioalogue {

    private lateinit var parent: MainActivity
    var database: EmaidDatabase? = null

    private val prefs = SharedPref

    @SuppressLint("SimpleDateFormat")
    val sdf = SimpleDateFormat("yyyy/MM/dd")
    val currentDate = sdf.format(Date())


    var dialog1: Dialog? = null


    fun Notification(_context: Context, _strHeading: String, _strMessage: String) {


        val builder = AlertDialog.Builder(_context)

        val _view: View =
            LayoutInflater.from(_context)
                .inflate(R.layout.customdialogue_error, null)
        builder.setView(_view)

        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)

        val textViewHeading: TextView = _view.findViewById(R.id.customeerror_heading)
        val textViewContent: TextView = _view.findViewById(R.id.customeerror_message)

        textViewHeading.text = _strHeading.trim { it <= ' ' }
        textViewContent.text = _strMessage.trim { it <= ' ' }

        val customeerror_btn: TextView = _view.findViewById(R.id.customeerror_btn)

        customeerror_btn.setOnClickListener {

            Loader.showLoader(_context)
            if (_context.isConnectedToNetwork()) {

                AppExecutors.instance?.networkIO()?.execute {
                    GlobalScope.launch {
                        try {

                            updateBookings(_context)


                        } catch (e: Exception) {
                            Log.e("throw", "Notification Update: ")

                        }

                    }
                }

                alertDialog.dismiss()
            } else {
                Loader.hideLoader()
                Toast.makeText(_context, "Please connect to internet", Toast.LENGTH_LONG).show()
            }
        }

        alertDialog.show()

    }

    suspend fun updateBookings(_context: Context) {

        try {

            val response = ApiClient.getRetrofit().getBookings(
                    prefs.user_id.toString(),
                    prefs.user_type.toString(),
                    currentDate,
                    prefs.token.toString()
            )

            if (response.status.equals("success")) {

                BookingsRepository.insertData(_context, response.schedule)

                Loader.hideLoader()
                val intent = Intent(_context, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                _context.startActivity(intent)


                Toast.makeText(_context, "Notification Updated", Toast.LENGTH_SHORT)
                        .show()
                Log.d("Success", "onNotification: " + "Notification Updated")
            } else if (response.status.equals("token_error")) {
                Loader.hideLoader()
                Toast.makeText(_context, response.message, Toast.LENGTH_SHORT)
                        .show()


                clearLogin(_context)
                // parent.stopServiceTracker()
                database?.clearAllTables()

                val intent = Intent(_context, LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                _context.startActivity(intent)

                Log.d("Fail", "onNotification: " + "Notification token ")

            } else {
                Loader.hideLoader()
                Toast.makeText(_context, response.message, Toast.LENGTH_SHORT)
                        .show()
                Log.d("Fail", "onNotification: " + "Notification Fail ")

            }

        }catch (e: Exception) {
            Loader.hideLoader()
            Log.e("throw", "fetDatFromServer: Notification")
        }

    }


    fun NotificationNew(
        _context: Context,
        _strHeading: String,
        _strMessage: String,
        notificationData: String?
    ) {


        val builder = AlertDialog.Builder(_context)

        val _view: View =
            LayoutInflater.from(_context)
                .inflate(R.layout.notification_dialog, null)
        builder.setView(_view)

        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)

        val textViewHeading: TextView = _view.findViewById(R.id.customeerror_heading)
        val textViewContent: TextView = _view.findViewById(R.id.customeerror_message)
        val shiftTextView: TextView = _view.findViewById(R.id.shift)
        val areaTextView: TextView = _view.findViewById(R.id.area)

        val string = _strMessage.split(":").toTypedArray()

        textViewHeading.text = _strHeading.trim { it <= ' ' }.toUpperCase()
        textViewContent.text = String.format("Maid     : %s", string.get(1).replace(", Customer", ""))
        textViewContent.text = String.format("Customer : %s", string.get(2).replace(", Shift", ""))
        shiftTextView.text = String.format("Maid     : %s", string.get(1).replace(", Customer", ""))
        areaTextView.text = String.format("Time : %s", string.get(3) + string.get(4) + " - " + string.get(5))

        val customeerror_btn: TextView = _view.findViewById(R.id.customeerror_btn)

        customeerror_btn.setOnClickListener {

            Loader.showLoader(_context)
            if (_context.isConnectedToNetwork()) {

                AppExecutors.instance?.networkIO()?.execute {
                    GlobalScope.launch {
                        try {

                            updateBookings(_context)


                        } catch (e: Exception) {
                            Log.e("throw", "Notification Update: ")

                        }

                    }
                }

                alertDialog.dismiss()
            } else {
                Loader.hideLoader()
                Toast.makeText(_context, "Please connect to internet", Toast.LENGTH_LONG).show()
            }
        }

        alertDialog.show()



    }

    private fun clearLogin(_context: Context) {
        prefs.clearSession(_context)
    }

    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }


    fun synchrinoouserror(_context: Activity, _strHeading: String, _strMessage: String) {


        val builder = AlertDialog.Builder(_context)

        val _view: View =
            LayoutInflater.from(_context)
                .inflate(R.layout.customdialogue_error, null)
        builder.setView(_view)

        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)

        val textViewHeading: TextView = _view.findViewById(R.id.customeerror_heading)
        val textViewContent: TextView = _view.findViewById(R.id.customeerror_message)

        textViewHeading.text = _strHeading.trim { it <= ' ' }
        textViewContent.text = _strMessage.trim { it <= ' ' }

        val customeerror_btn: TextView = _view.findViewById(R.id.customeerror_btn)

        customeerror_btn.setOnClickListener {

            Loader.showLoader(_context)
            if (_context.isConnectedToNetwork()) {

                AppExecutors.instance?.networkIO()?.execute {
                    GlobalScope.launch {
                        try {

                            updateBookings(_context)


                        } catch (e: Exception) {
                            Log.e("throw", "Notification Update: ")

                        }

                    }
                }

                alertDialog.dismiss()
            } else {
                Loader.hideLoader()
                Toast.makeText(_context, "Please connect to internet", Toast.LENGTH_LONG).show()
            }
        }

        alertDialog.show()

    }
}