package com.azinova.emaid_crew.database.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "bookings_table")
data class BookingsTable (

        @PrimaryKey
        @ColumnInfo(name = "booking_id")
        var booking_id: Int? = null,

        @ColumnInfo(name = "schedule_date")
        var schedule_date: String? = null,

        @ColumnInfo(name = "timeing")
        var timeing: String? = null,

        @ColumnInfo(name = "no_of_maids")
        var no_of_maids: Int? = null,

        @ColumnInfo(name = "service_status")
        var service_status: Int? = null,

        @ColumnInfo(name = "shift_start")
        var shift_start: String? = null,

        @ColumnInfo(name = "shift_end")
        var shift_end: String? = null,

        @ColumnInfo(name = "area")
        var area: String? = null,

        @ColumnInfo(name = "cleaning_material")
        var cleaning_material: String? = null,

        @ColumnInfo(name = "customer_id")
        var customer_id: Int? = null,

        @ColumnInfo(name = "customer_name")
        var customer_name: String? = null,


        @ColumnInfo(name = "customer_address")
        val customerAddress: String? = null,


        @ColumnInfo(name = "customer_mobile")
        val customerMobile: String? = null,

        @ColumnInfo(name = "customer_type")
        val customerType: String? = null,

        @ColumnInfo(name = "customer_code")
        val customerCode: String? = null,

        @ColumnInfo(name = "key_status")
        val keyStatus: String? = null,

        @ColumnInfo(name = "booking_note")
        val bookingNote: String? = null,

        @ColumnInfo(name ="customer_notes")
        val customernotes: String? = null,

        @ColumnInfo(name = "service_fee")
        val serviceFee: String? = null,

        @ColumnInfo(name = "zone")
        val zone: String? = null,

        @ColumnInfo(name = "mop")
        val mop: String? = null,

        @ColumnInfo(name ="extra_service")
        val extraService: String? = null,

        @ColumnInfo(name ="tools")
        val tools: String? = null,

        @ColumnInfo(name ="total")
        val total: String? = null,

        @ColumnInfo(name ="balance")
        val balance: Double? = null,

        @ColumnInfo(name ="outstanding_balance")
        val outstandingBalance: String? = null,

        @ColumnInfo(name ="servicetype")
        val servicetype: String? = null,

        @ColumnInfo(name ="payment_status")
        val paymentStatus: Int? = null,

        @ColumnInfo(name ="paid_amount")
        val paidAmount: Double? = null,

        @ColumnInfo(name ="customer_latitude")
        val customerLatitude: String? = null,

        @ColumnInfo(name ="customer_longitude")
        val customerLongitude: String? = null,

        @ColumnInfo(name ="starttime")
        val starttime: String? = null,

        @ColumnInfo(name ="endtime")
        val endtime: String? = null

        )
