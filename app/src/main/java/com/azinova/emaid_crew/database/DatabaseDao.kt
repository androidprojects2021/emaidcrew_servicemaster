package com.azinova.emaid_crew.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.azinova.emaid_crew.database.tables.BookingsTable
import com.azinova.emaid_crew.database.tables.MaidTable

@Dao
interface DatabaseDao {
    @Insert
    suspend fun insertAllBookings(bookins: BookingsTable)

    @Insert
    suspend fun insertAllMaidss(maiids: MaidTable)

    //new change
    @Query("INSERT INTO bookings_table(schedule_date) VALUES (:selecteddate)")
    fun insertDate(selecteddate: String)


    // -------------------------------------- Timer ----------------------

    /* @Insert
     suspend fun insertCounterTimeData(timeCountdownTable: TimeCountdownTable)*/

    // --------------------------------------  ----------------------


    @Query("SELECT DISTINCT schedule_date from bookings_table ORDER BY schedule_date ASC")
    suspend fun getDates(): List<String>

    //AllDataByDate
    @Query("SELECT * FROM bookings_table WHERE schedule_date =:selecteddate ORDER BY timeing ASC")
    fun getAllBookings(selecteddate: String): LiveData<List<BookingsTable>>?

    //ALL Timeing
    @Query("SELECT DISTINCT timeing FROM bookings_table WHERE schedule_date =:selecteddate ORDER BY timeing ASC")
    suspend fun getBookingTimeings(selecteddate: String): List<String>?

    //Selected Timeing
    @Query("SELECT DISTINCT timeing FROM bookings_table WHERE schedule_date =:selecteddate AND service_status=:filterPosition ORDER BY timeing ASC")           //  <--------
    suspend fun getBookingTimeingsByFilterPosition(
        selecteddate: String,
        filterPosition: String
    ): List<String>?

    //ALL Bookings
    @Query("SELECT * FROM bookings_table WHERE schedule_date =:selecteddate AND timeing =:timeing ORDER BY service_status ASC")
    suspend fun getSchedulesByTime(
        selecteddate: String?,
        timeing: String?

    ): List<BookingsTable>?

    //Selected Bookings
    @Query("SELECT * FROM bookings_table WHERE schedule_date =:selecteddate AND timeing =:timeing AND service_status=:filterPosition")
    suspend fun getSchedulesByTimeByFilterPosition(
        selecteddate: String?,
        timeing: String?,
        filterPosition: String
    ): List<BookingsTable>?

    @Query("SELECT * FROM bookings_table WHERE booking_id =:bookingId ")
    fun getCustomerDetails(bookingId: String):LiveData<List<BookingsTable>>?

    @Query("SELECT * FROM maid_table WHERE booking_id =:booking_id ")
    suspend fun getMaidList(booking_id: String?): List<MaidTable>?

    @Query("SELECT * FROM maid_table WHERE booking_id =:booking_id ")
    fun getMailListFromDB(booking_id: String?):  LiveData<List<MaidTable>>?





//-----------
    @Query("SELECT * FROM maid_table WHERE booking_id =:booking_id ")
   suspend fun NewMailListFromDB(booking_id: String?): List<MaidTable>?

    @Query("SELECT * FROM bookings_table WHERE booking_id =:bookingId ")
    suspend fun NewCustomerDetailsFromDB(bookingId: String?):List<BookingsTable>?
//---------------




    @Query("SELECT maid_image from maid_table WHERE maid_id =:maidid")
    suspend fun getMaidImage(maidid: String): List<String>




    // -------------------------------------- Timer ----------------------

   /* @Query("SELECT * FROM timecountdown_table WHERE booking_id =:booking_id")
    suspend fun getCounterTimeing(booking_id: String?): List<TimeCountdownTable>?*/
    // -------------------------------------- Timer ----------------------


    @Query("UPDATE bookings_table SET service_status=:serviceStatus, starttime=:starttime WHERE booking_id = :bookingId ")
    suspend fun updatestartbookingStatus(bookingId: String, serviceStatus: String, starttime: String?)

    @Query("UPDATE bookings_table SET service_status=:serviceStatus, paid_amount=:paid_amount, outstanding_balance=:outstandingAmount, mop=:mop, payment_status=:payment_status, balance=:balance , endtime=:endtime WHERE booking_id = :bookingId ")
    suspend fun updatefinishbookingStatus(
            bookingId: String, serviceStatus: String, paid_amount: String?,
            outstandingAmount: String?, mop: String?, payment_status: Int, balance: Double?,endtime: String?
    )

    @Query("UPDATE bookings_table SET service_status=:serviceStatus WHERE booking_id = :bookingId ")
    suspend fun updateCancelbookingStatus(bookingId: String, serviceStatus: String)

    @Query("UPDATE maid_table SET maid_attandence=:maidAttandence WHERE maid_id = :maidId ")
    suspend fun updateMaidStatusChange(maidId: String, maidAttandence: String)

    @Query("UPDATE bookings_table SET no_of_maids=:noOfMaids WHERE timeing = :timeing ")
    suspend fun transferupdates(timeing: String, noOfMaids: Int?)


    @Query("SELECT DISTINCT maid_id from maid_table")
    suspend fun getMaids(): List<String>


    @Query("DELETE FROM bookings_table")
    suspend fun deleteBookings()

    @Query("DELETE FROM maid_table")
    suspend fun deleteMaids()


    @Query("DELETE FROM bookings_table WHERE booking_id =:bookingId")
    suspend fun deleteTransferedBooking(bookingId: String)

    @Query("DELETE FROM maid_table WHERE booking_id =:bookingId")
    suspend fun deleteTransferedBookingmaidtable(bookingId: String)

    @Query("DELETE FROM bookings_table WHERE service_status =:service_type")
    suspend fun deleteTransferedallBooking(service_type: String)

    @Query("DELETE FROM maid_table WHERE service_status =:service_type")
    suspend fun deleteTransferedallBookingmaidtable(service_type: String)


    // -------------------------------------- Timer ----------------------

    /*  @Query("DELETE FROM timecountdown_table WHERE booking_id =:bookingId")
      suspend fun deleteCounterTimeData(bookingId: String)*/

    // -------------------------------------- Timer ----------------------

}