package com.azinova.emaid_crew.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.azinova.emaid_crew.database.tables.BookingsTable
import com.azinova.emaid_crew.database.tables.MaidTable

@Database(entities = arrayOf(BookingsTable::class,MaidTable::class), version = 1, exportSchema = false)  // ,TimeCountdownTable::class
abstract class EmaidDatabase : RoomDatabase() {

    abstract fun getDao(): DatabaseDao

    companion object {

        @Volatile
        private var INSTANCE: EmaidDatabase? = null

        fun getDataseClient(context: Context): EmaidDatabase {

            if (INSTANCE != null) return INSTANCE!!

            synchronized(this) {

                INSTANCE = Room
                    .databaseBuilder(context, EmaidDatabase::class.java, "APP_DATABASE")
                    .fallbackToDestructiveMigration()
                    .build()

                return INSTANCE!!

            }
        }
    }
}